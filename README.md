﻿# Course Site Generator #

This program helps user to create a website for various courses through this program. The program will let the user select how many pages their site should have and then what sections of content should appear on the site. In addition, we'll build a modular system for adding dynamically loaded content for things like the course office hours grid and schedule. The application will generate the necessary HTML and complementary JSON files for loading such dynamic data.

The application will also reflect student projects if there are any.

### This repository is to track the author's progress throughout the semester. ###

* Program to create proper course websites
* Version 1.1

### How do I get set up? ###

* It will be finished in about 3 months and then the setting up functions would be loaded here.

### Contribution guidelines ###

* Please provide any suggestions that you have to improve this program.

### Who do I talk to? ###

* Please contact Pakigya.Tuladhar@stonybrook.edu if you have any question

### WISHES ###

* Hope the project turns out to be great such that I can turn it into a VR generating website. Wish me LUCK!!!
    I hope this can be done.
    
### DEMO ###

*  The Demo will be here soon.