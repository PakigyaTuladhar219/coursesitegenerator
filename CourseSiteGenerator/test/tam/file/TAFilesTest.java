/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.file;

import djf.components.AppDataComponent;
import static djf.settings.AppStartupConstants.APP_PROPERTIES_FILE_NAME;
import static djf.settings.AppStartupConstants.APP_PROPERTIES_FILE_NAME_ENGLISH;
import static djf.settings.AppStartupConstants.PATH_DATA;
import static djf.settings.AppStartupConstants.PATH_WORK;
import static djf.settings.AppStartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import properties_manager.PropertiesManager;
import tam.data.TAData;

/**
 *
 * @author PLT
 */
public class TAFilesTest {
    
    public TAFilesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of exportData method, of class TAFiles.
     
    @Ignore
    @Test
    public void testExportData() throws Exception {
        System.out.println("exportData");
        AppDataComponent data = null;
        String filePath = "";
        TAFiles instance = null;
        instance.exportData(data, filePath);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Ignore
    @Test
    public void testExportTestWork() throws Exception {
        System.out.println("exportTestWork");
        String selectedFile = "";
        TAFiles instance = null;
        instance.exportTestWork(selectedFile);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    * */

    /**
     * Test of loadTestData method, of class TAFiles.
     */
    @Test
    public void testLoadTestData() throws Exception {
        System.out.println("loadTestData");
        
        
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
	props.loadProperties(APP_PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
        AppDataComponent data = new TAData();
        //AppDataComponent data = null;
        
        String filePathLoad = PATH_WORK + "SiteSaveTestData.json";
        String filePathSave = PATH_WORK + "SiteSaveTestResult.json";
        TAFiles instance = new TAFiles();
        
        instance.loadTestData(data, filePathLoad);
        instance.saveTestData(data, filePathSave);
        
        
	JsonObject jsonLoad = loadJSONFile(filePathLoad);
	JsonObject jsonSave = loadJSONFile(filePathSave);
        
        //assertEquals(jsonSave.toString(),jsonLoad.toString());
        if(jsonLoad ==jsonSave){
            System.out.println(jsonLoad);
            System.out.println(jsonSave);
        }
        else
        {
            System.out.println(jsonLoad);
            System.out.println(jsonSave);
        }
        assertEquals(jsonSave.toString(),jsonLoad.toString());
        
        
        filePathLoad = PATH_WORK + "TestData.json";
        filePathSave = PATH_WORK + "TestResult.json";
        data.resetData();
        
        instance.loadTestData(data, filePathLoad);
        instance.saveTestData(data, filePathSave);
        
        
	jsonLoad = loadJSONFile(filePathLoad);
	jsonSave = loadJSONFile(filePathSave);
        
        //assertEquals(jsonSave.toString(),jsonLoad.toString());
        if(jsonLoad ==jsonSave){
            System.out.println(jsonLoad);
            System.out.println(jsonSave);
        }
        else
        {
            System.out.println(jsonLoad);
            System.out.println(jsonSave);
        }
        assertEquals(jsonSave.toString(),jsonLoad.toString());
        
        
        
        filePathLoad = PATH_WORK + "jUnitTestData.json";
        filePathSave = PATH_WORK + "jUnitTestResult.json";
        data.resetData();
        
        instance.loadTestData(data, filePathLoad);
        instance.saveTestData(data, filePathSave);
        
        
	jsonLoad = loadJSONFile(filePathLoad);
	jsonSave = loadJSONFile(filePathSave);
        
        //assertEquals(jsonSave.toString(),jsonLoad.toString());
        if(jsonLoad ==jsonSave){
            System.out.println(jsonLoad);
            System.out.println(jsonSave);
        }
        else
        {
            System.out.println(jsonLoad);
            System.out.println(jsonSave);
        }
        assertEquals(jsonSave.toString(),jsonLoad.toString());
    }

    /**
     * 
        //assertArrayEquals(new ByteArrayInputStream(jsonSave.toString().getBytes()),new ByteArrayInputStream(jsonLoad.toString().getBytes()));
        //assertEquals(jsonSave.toString(),jsonLoad.toString());
        //assertEquals(new ByteArrayInputStream(jsonSave.toString().getBytes()),new ByteArrayInputStream(jsonLoad.toString().getBytes()));
     * Test of saveTestData method, of class TAFiles.
     */
    /*
    @Ignore
    @Test
    public void testSaveTestData() throws Exception {
        System.out.println("saveTestData");
        AppDataComponent data = null;
        String filePath = "";
        TAFiles instance = null;
        instance.saveTestData(data, filePath);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
*/

    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

}
