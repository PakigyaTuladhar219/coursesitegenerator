
import csg.workspace.courseWorkspace;
import csg.workspace.csgWorkspace;
import csg.workspace.projectWorkspace;
import csg.workspace.recitationWorkspace;
import csg.workspace.scheduleWorkspace;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import tam.TAManagerApp;
import tam.workspace.TAWorkspace;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author PLT
 */
public class TestSave {
    
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
    
    csgWorkspace csgworkspace;
     
        TAWorkspace TAworkspace;
        courseWorkspace courseworkspace;
        recitationWorkspace recitationworkspace;
        scheduleWorkspace scheduleworkspace;
        projectWorkspace projectworkspace;
    
    public TestSave(){
        
    }
    
    /**
     * Constructor, note that the app must already be constructed.
     */
    public TestSave(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        csgworkspace = (csgWorkspace)app.getWorkspaceComponent();
        TAworkspace = (TAWorkspace)app.getTAWorkspaceComponent();
        courseworkspace = (courseWorkspace)app.getCourseWorkspaceComponent();
        recitationworkspace = (recitationWorkspace)app.getRecitationWorkspaceComponent();
        scheduleworkspace = (scheduleWorkspace)app.getScheduleWorkspaceComponent();
        projectworkspace = (projectWorkspace) app.getProjectWorkspaceComponent();
        
    }
    
    public void fillInData()
    {   /*
        csgWorkspace csgworkspace = (csgWorkspace)app.getWorkspaceComponent();
        TAWorkspace TAworkspace = (TAWorkspace)app.getTAWorkspaceComponent();
        courseWorkspace courseworkspace = (courseWorkspace)app.getCourseWorkspaceComponent();
        recitationWorkspace recitationworkspace = (recitationWorkspace)app.getRecitationWorkspaceComponent();
        scheduleWorkspace scheduleworkspace = (scheduleWorkspace)app.getScheduleWorkspaceComponent();
        projectWorkspace projectworkspace = (projectWorkspace) app.getProjectWorkspaceComponent();
        */
        
    }
    
    public void fillCourseDetailsData()
    {
        //courseworkspace.getSubjectComboBox().addItem("CSE");
        //courseworkspace.getSubjectComboBox() = new ComboBox(getSubjectOptions());
        courseworkspace.getTitleTextField().setText("Pakigya Tuladhar");
    }
    
    
    public ObservableList<String> getSubjectOptions(){
        //ObservableList<String> options = new ObservableList<String>();
        ObservableList<String> options = FXCollections.observableArrayList();
        options.add("CSE");
        options.add("MAT");
        options.add("AMS");
        options.add("PHY");
        return options;
    }
    
    public void fillRecitationData()
    {
        
    }
    
    public void fillScheduleData()
    {
        
    }
    
    public void fillProjectData()
    {
        
    }
}
