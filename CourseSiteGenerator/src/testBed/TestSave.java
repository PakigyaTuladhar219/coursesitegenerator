package testBed;


import csg.workspace.courseWorkspace;
import csg.workspace.csgWorkspace;
import csg.workspace.projectWorkspace;
import csg.workspace.recitationWorkspace;
import csg.workspace.scheduleWorkspace;
import djf.settings.AppStartupConstants;
import java.io.IOException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.workspace.TAWorkspace;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author PLT
 */
public class TestSave {
    
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
    
    csgWorkspace csgworkspace;
     
        TAWorkspace TAworkspace;
        courseWorkspace cws;
        recitationWorkspace rws;
        scheduleWorkspace sws;
        projectWorkspace pws;
        
        TAData data;
    
    public TestSave(){
        
    }
    
    /**
     * Constructor, note that the app must already be constructed.
     */
    public TestSave(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        csgworkspace = (csgWorkspace)app.getWorkspaceComponent();
        TAworkspace = (TAWorkspace)app.getTAWorkspaceComponent();
        cws = (courseWorkspace)app.getCourseWorkspaceComponent();
        rws = (recitationWorkspace)app.getRecitationWorkspaceComponent();
        sws = (scheduleWorkspace)app.getScheduleWorkspaceComponent();
        pws = (projectWorkspace) app.getProjectWorkspaceComponent();
        
        data = (TAData) app.getDataComponent();
        
        //fillInData();
        //app.getFileComponent().saveAllData(data, "SiteSaveTest");
    }
    
    public void fillInData()
    {           
        fillCourseDetailsData();
        fillTeachingAssistantData();
        fillRecitationData();
        fillScheduleData();
        fillProjectData();
        try {
            app.getFileComponent().exportTestWork("SiteSaveTest.json");
        } catch (IOException ex) {
            Logger.getLogger(TestSave.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void fillCourseDetailsData()
    {
        cws.getSubjectComboBox().setItems(getSubjectOptions());
        cws.getNumberComboBox().setItems(getNumberOptions());
        cws.getSemesterComboBox().setItems(getSemesterOptions());
        cws.getYearComboBox().setItems(getYearOptions());
        cws.getSubjectComboBox().getSelectionModel().selectFirst();
        cws.getNumberComboBox().getSelectionModel().selectFirst();
        cws.getSemesterComboBox().getSelectionModel().selectFirst();
        cws.getYearComboBox().getSelectionModel().selectFirst();
        
        cws.getTitleTextField().setText("CSE 215");
        cws.getInstructorNameTextField().setText("Richard Mckenna");
        cws.getInstructorHomeTextField().setText("www.stonybrook.edu/~richard");
        cws.getPathLabel().setText(AppStartupConstants.PATH_EXPORT);
        cws.getTemplateChooseLabel().setText(AppStartupConstants.PATH_TO_HTML);
        cws.getStylesheetComboBox().setValue("style.css");
        
        data.addPage("Home", "index.html", "HomeBuilder.js", true);
        data.addPage("Syllabus", "syllabus.html", "SyllabusBuilder.js", true);
        data.addPage("Schedule", "schedule.html", "ScheduleBuilder.js", true);
        data.addPage("HWs", "hws.html", "HWsBuilder.js", false);
        data.addPage("Projects", "projects.html", "ProjectsBuilder.js", true);
    }
    
    public void fillTeachingAssistantData(){
        data.addTA("Pakigya Tuladhar", "Pakigya@hotmail.com", true);
        data.addTA("Sandhya Sharma", "Sandhya.Sharma@stonybrook.edu", false);
        data.addTA("Tsering Namgyal", "Tsering.Namgyal@stonybrook.edu", true);
    }
    
    public ObservableList<String> getSubjectOptions(){
        ObservableList<String> options = FXCollections.observableArrayList();
        options.add("CSE");
        options.add("MAT");
        options.add("AMS");
        options.add("PHY");
        return options;
    }
    
    public ObservableList<Integer> getNumberOptions(){
        ObservableList<Integer> options = FXCollections.observableArrayList();
        int i = 200;
        for (i=200; i<300; i++)
        {
            options.add(i);
        }
        return options;
    }
    
    public ObservableList<String> getSemesterOptions(){
        ObservableList<String> options = FXCollections.observableArrayList();
        options.add("Fall");
        options.add("Winter");
        options.add("Spring");
        options.add("Summer I");
        options.add("Summer I Extended");
        options.add("Summer I");
        options.add("Summer II Extended");
        return options;
    }
    public ObservableList<Integer> getYearOptions(){
        ObservableList<Integer> options = FXCollections.observableArrayList();
        int i = 2000;
        for (i=2000; i<2030; i++)
        {
            options.add(i);
        }
        return options;
    }
    
    public void fillRecitationData()
    {
        rws.getSectionTextField().setText("01");
        rws.getInstructorTextField().setText("Richard Mckenna");
        rws.getDayTimeTextField().setText("Monday: 5:30pm to 6:30pm");
        rws.getLocationTextField().setText("Javits 110");
        rws.getSupervisingTAComboBox1().setItems(data.getTeachingAssistants());
        rws.getSupervisingTAComboBox1().getSelectionModel().selectFirst();
        rws.getSupervisingTAComboBox2().setItems(data.getTeachingAssistants());
        rws.getSupervisingTAComboBox2().getSelectionModel().selectLast();
        
        
        //data.addRecitation(String initSection, String initInstructor, String initDayTime, String initLocation, String initTA1, String initTA2);
        data.addRecitation("01", "Richard Mckenna", "2:00 Monday", "Old CS 2110", "Robert Trujillo", "Micheal Mathew");
        data.addRecitation("02", "Ritwik Banerjee", "4:15 Tuesday", "Old CS 2110", "Wonje Kang", "Yu Zhao");
        data.addRecitation("03", "Richard Mckenna", "5:30 Tuesday", "Old CS 2110", "Hsiang-Ju Lai", "Calvin Cheng");
       
    }
    
    public void fillScheduleData()
    {
        sws.getStartDatePicker().setValue(LocalDate.of(2017,1, 1));
        sws.getEndDatePicker().setValue(LocalDate.of(2017, 5,19));
        
        data.addSchedule("Holiday", "04/22/2017", "5:00pm", "Lecture 20", "Reflection", "www.blackboard.stonybrook.edu", "need to know UML");
        data.addSchedule("Snow Days", "03/21/2017", "10:00am", "Snow Day", "No Class", "www.stonybrook.edu", "");
        data.addSchedule("Homework", "04/24/2017", "11:59pm", "Homework", "Populating Data", "www.blackboard.stonybrook.edu", "need to know Java");
        data.addSchedule("Holiday", "04/21/2017", "9:00am", "Weekend", "Homework Due", "www.blackboard.stonybrook.edu", "");
        
        sws.getTypeComboBox().setItems(getDayTypeOptions());
        sws.getTypeComboBox().getSelectionModel().selectFirst();
        sws.getaddDatePicker().setValue(LocalDate.of(2017, 4, 23));
        sws.getDayTimeTextField().setText("11:00am");
        sws.getTitleTextField().setText("Lecture 3");
        sws.getTopicTextField().setText("Event Programming");
        sws.getLinkTextField().setText("www.google.com");
        sws.getCriteriaTextField().setText("need to learn UML");
    }
    
    public ObservableList<String> getDayTypeOptions(){
        ObservableList<String> options = FXCollections.observableArrayList();
        options.add("Holiday");
        options.add("Lecture");
        options.add("HW");
        options.add("Notes");
        return options;
    }
    
    public void fillProjectData()
    {
        pws.getNameTextField().setText("Chepang");
        //pws.getBgColorPicker().setValue(Color.BLUE);
        pws.getBgColorPicker().setValue(Color.web("#669966"));
        pws.getTextColorPicker().setValue(Color.web("#ff9999"));
        pws.getLinkTextField().setText("www.pakigya.herokuapp.com");
        
        
        data.addTeam("Chepang", "#669966", "#6699FF", "www.google.com");
        data.addTeam("void", "#000000", "#FFFFFF", "www.hotmail.com");
        data.addTeam("Mersenne", "#FFFFFF", "#000000", "www.gmail.com");
        data.addTeam("MonkeyKing", "#FF00FF", "#000000", "www.monkeyKing.com");
        data.addTeam("Afsheen", "#00FFFF", "#000000", "www.yahoo.com");
        
        
        pws.getFirstNameTextField().setText("Pakigya");
        pws.getLastNameTextField().setText("Tuladhar");
        pws.getTeamComboBox().setItems(data.getTeams());
        pws.getTeamComboBox().getSelectionModel().selectFirst();
        pws.getRoleTextField().setText("Designer");
        data.addStudent("Pakigya", "Tuladhar", "Chepang", "Researcher");
        data.addStudent("Farhan", "Hyder", "MonkeyKing", "Sketcher");
        data.addStudent("Sandhya", "Sharma", "Chepang", "Designer");
        data.addStudent("Tsering", "Namgyal", "Chepang", "Investor");
    }
}
