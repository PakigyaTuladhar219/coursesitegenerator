/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author PLT
 */
public class Recitation<E extends Comparable<E>> implements Comparable<E>  {

    private final StringProperty section;
    private final StringProperty instructor;
    private final StringProperty dayTime;
    private final StringProperty location;
    private final StringProperty supervisingTA1;
    private final StringProperty supervisingTA2;
    
    public Recitation(String initSection, String initInstructor, String initDayTime, String initLocation, String initTA1, String initTA2)
    {
        section = new SimpleStringProperty(initSection);
        instructor = new SimpleStringProperty(initInstructor);
        dayTime = new SimpleStringProperty(initDayTime);
        location = new SimpleStringProperty(initLocation);
        supervisingTA1 = new SimpleStringProperty(initTA1);
        supervisingTA2 = new SimpleStringProperty(initTA2);
    }
    
    @Override
    public int compareTo(E o) {
        return getSection().compareTo(((Recitation)o).getSection());
    }

    /**
     * @return the section
     */
    public String getSection() {
        return section.get();
    }

    /**
     * @param initSection the section to set
     */
    public void setSection(String initSection) {
        section.set(initSection);
    }

    /**
     * @return the instructor
     */
    public String getInstructor() {
        return instructor.get();
    }

    /**
     * @param initInstructor the instructor to set
     */
    public void setInstructor(String initInstructor) {
        instructor.set(initInstructor);
    }

    /**
     * @return the dayTime
     */
    public String getDayTime() {
        return dayTime.get();
    }

    /**
     * @param initDayTime the dayTime to set
     */
    public void setDayTime(String initDayTime) {
        dayTime.set(initDayTime);
    }
    
    /**
     * @return the location
     */
    public String getLocation() {
        return location.get();
    }

    /**
     * @param initLocation
     * @param location the location to set
     */
    public void setLocation(String initLocation) {
        location.set(initLocation);
    }

    /**
     * @return the supervisingTA1
     */
    public String getSupervisingTA1() {
        return supervisingTA1.get();
    }

    /**
     * @param initSupervisingTA1 the supervisingTA1 to set
     */
    public void setSupervisingTA1(String initSupervisingTA1) {
        supervisingTA1.set(initSupervisingTA1);
    }

    /**
     * @return the supervisingTA2
     */
    public String getSupervisingTA2() {
        return supervisingTA2.get();
    }
   
    /**
     * @param initSupervisingTA2 the supervisingTA1 to set
     */
    public void setSupervisingTA2(String initSupervisingTA2) {
        supervisingTA2.set(initSupervisingTA2);
    }
        
    /**
     * EQUALS TO CHECK WHETHER A GIVEN TA IS EQUAL TO ANOTHER TA OR NOT
     * @param o
     * @return 
     */
    public boolean equals(Object o){
    	if (this == o) return true;
    	if (!(o instanceof Recitation)) return false;
		Recitation otherRec  = (Recitation) o;
        return (otherRec.getSection().equals(this.getSection()));    	
    }
    
    public boolean isEmpty(){
        return (getSection().isEmpty() && getInstructor().isEmpty() && getDayTime().isEmpty() && getLocation().isEmpty() && getSupervisingTA1().isEmpty() && getSupervisingTA1().isEmpty());
    }
}
