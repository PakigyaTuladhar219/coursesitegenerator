/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author PLT
 */
public class Team <E extends Comparable<E>> implements Comparable<E>{

    
    private final StringProperty name;
    private final StringProperty bgColor;
    private final StringProperty textColor;
    private final StringProperty link;
    
    public Team(String initName, String initBGcolor, String initTextColor, String initLink)
    {
        name = new SimpleStringProperty(initName);
        bgColor = new SimpleStringProperty(initBGcolor);
        textColor = new SimpleStringProperty(initTextColor);
        link = new SimpleStringProperty(initLink);
    }
    
    @Override
    public int compareTo(E o) {
        return getName().compareTo(((Team)o).getName());
    }

    /**
     * @return the name
     */
    public String getName() {
        return name.get();
    }

    /**
     * @param initName the name to set
     */
    public void setName(String initName) {
        name.set(initName);
    }

    /**
     * @return the bgcolor
     */
    public String getBGcolor() {
        return bgColor.get();
    }
    
    public int getRed() {
        return Integer.parseInt(bgColor.get().substring(1,3),16);
    }
    
    public int getGreen() {
        return Integer.parseInt(bgColor.get().substring(3,5),16);
    }
    
    public int getBlue() {
        return Integer.parseInt(bgColor.get().substring(5,7),16);
    }

    /**
     * @param initBGcolor the bgcolor to set
     */
    public void setBGcolor(String initBGcolor) {
        bgColor.set(initBGcolor);
    }

    /**
     * @return the textColor
     */
    public String getTextColor() {
        return textColor.get();
    }

    /**
     * @param initTextColor the textColor to set
     */
    public void setTextColor(String initTextColor) {
        textColor.set(initTextColor);
    }
    
    /**
     * @return the link
     */
    public String getLink() {
        return link.get();
    }

    /**
     * @param link the link to set
     */
    public void setLink(String initLink) {
        link.set(initLink);
    }

        
    /**
     * EQUALS TO CHECK WHETHER A GIVEN Team IS EQUAL TO ANOTHER Team OR NOT
     * @param o
     * @return 
     */
    public boolean equals(Object o){
    	if (this == o) return true;
    	if (!(o instanceof Team)) return false;
		Team otherTeam  = (Team) o;
        return (otherTeam.getBGcolor().equals(this.getBGcolor()));    	
    }
    
    public String toString(){
        return getName();
    }

    
}
