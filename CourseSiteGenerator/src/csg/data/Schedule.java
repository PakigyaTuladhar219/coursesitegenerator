/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import java.time.LocalDate;
import java.util.Date;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author PLT
 */
public class Schedule <E extends Comparable<E>> implements Comparable<E>{

 
    private final StringProperty type;
    private final StringProperty date;
    private final StringProperty time;
    private final StringProperty title;
    private final StringProperty topic;
    private final StringProperty link;
    private final StringProperty criteria;
    
    public Schedule(String initType, String initDate, String initTime, String initTitle, String initTopic, String initLink, String initCriteria)
    {
        type = new SimpleStringProperty(initType);
        date = new SimpleStringProperty(initDate);
        time = new SimpleStringProperty(initTime);
        title = new SimpleStringProperty(initTitle);
        topic = new SimpleStringProperty(initTopic);
        link = new SimpleStringProperty(initLink);
        criteria = new SimpleStringProperty(initCriteria);
    }
    
    @Override
    public int compareTo(E o) {
        return getDate().compareTo(((Schedule)o).getDate());
    }

    /**
     * @return the type
     */
    public String getType() {
        return type.get();
    }

    /**
     * @param initType the type to set
     */
    public void setType(String initType) {
        type.set(initType);
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date.get();
    }
    /**
     * @return the date
     */
    public LocalDate getDateFormat(){
        String date = getDate();
        System.out.println(date);
        int first = date.indexOf('/');
        String subDate = date.substring(first+1,date.length());
        int second = subDate.indexOf('/');
        System.out.println(first);
        System.out.println(second);
        int month = Integer.parseInt(date.substring(0, first));
        int day = Integer.parseInt(subDate.substring(0, second));
        int year = Integer.parseInt(date.substring(date.length()-4,date.length()));
        return LocalDate.of(year, month, day);
    }
    
    
    /**
     * @param initDate the date to set
     */
    public void setDate(String initDate) {
        date.set(initDate);
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time.get();
    }

    /**
     * @param initTime the time to set
     */
    public void setTime(String initTime) {
        time.set(initTime);
    }
    
    /**
     * @return the title
     */
    public String getTitle() {
        return title.get();
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String initTitle) {
        title.set(initTitle);
    }

    /**
     * @return the topic
     */
    public String getTopic() {
        return topic.get();
    }

    /**
     * @param initTopic the topic to set
     */
    public void setTopic(String initTopic) {
        topic.set(initTopic);
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link.get();
    }
   
    /**
     * @param initLink the topic to set
     */
    public void setlink(String initLink) {
        link.set(initLink);
    }
        
    /**
     * @return the criteria
     */
    public String getCriteria() {
        return criteria.get();
    }
   
    /**
     * @param initCriteria the topic to set
     */
    public void setCriteria(String initCriteria) {
        criteria.set(initCriteria);
    }
        
    /**
     * EQUALS TO CHECK WHETHER A GIVEN SCHEDULE IS EQUAL TO ANOTHER SCHEDULE OR NOT
     * @param o
     * @return 
     */
    public boolean equals(Object o){
    	if (this == o) return true;
    	if (!(o instanceof Schedule)) return false;
		Schedule otherSchedule  = (Schedule) o;
        return (otherSchedule.getDate().equals(this.getDate()));    	
    }
    
}
