/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author PLT
 */
public class Student <E extends Comparable<E>> implements Comparable<E>{

    private final StringProperty firstName;
    private final StringProperty lastName;
    private final StringProperty team;
    private final StringProperty role;
    
    public Student(String initFirstName, String initLastName, String initTeam, String initRole)
    {
        firstName = new SimpleStringProperty(initFirstName);
        lastName = new SimpleStringProperty(initLastName);
        team = new SimpleStringProperty(initTeam);
        role = new SimpleStringProperty(initRole);
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName.get();
    }

    /**
     * @param initFirstName the firstName to set
     */
    public void setFirstName(String initFirstName) {
        firstName.set(initFirstName);
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName.get();
    }

    /**
     * @param initLastName the lastName to set
     */
    public void setLastName(String initLastName) {
        lastName.set(initLastName);
    }

    /**
     * @return the team
     */
    public String getTeam() {
        return team.get();
    }

    /**
     * @param initTeam the team to set
     */
    public void setTeam(String initTeam) {
        team.set(initTeam);
    }
    
    /**
     * @return the role
     */
    public String getRole() {
        return role.get();
    }

    /**
     * @param initRole the role to set
     */
    public void setRole(String initRole) {
        role.set(initRole);
    }
        
    /**
     * EQUALS TO CHECK WHETHER A GIVEN TEAM IS EQUAL TO ANOTHER TEAM OR NOT
     * @param o
     * @return 
     */
    public boolean equals(Object o){
    	if (this == o) return true;
    	if (!(o instanceof Student)) return false;
		Student otherStudent  = (Student) o;
        return (otherStudent.getFirstName().equals(this.getFirstName()));    	
    }
    
    @Override
    public int compareTo(E o) {
        return getFirstName().compareTo(((Student)o).getFirstName());
    }
}
