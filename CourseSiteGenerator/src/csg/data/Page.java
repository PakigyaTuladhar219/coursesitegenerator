/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author PLT
 */
public class Page <E extends Comparable<E>> implements Comparable<E>{
    
    private final StringProperty nav;
    private final StringProperty fileName;
    private final StringProperty script;
    protected boolean use;
    
    public Page(String initNav, String initFileName, String initScript, boolean initUse)
    {
        nav = new SimpleStringProperty(initNav);
        fileName = new SimpleStringProperty(initFileName);
        script = new SimpleStringProperty(initScript);
        use = initUse;
    }
    
    @Override
    public int compareTo(E o) {
        return getNav().compareTo(((Page)o).getNav());
    }

    /**
     * @return the nav
     */
    public String getNav() {
        return nav.get();
    }

    /**
     * @param initNav the nav to set
     */
    public void setNav(String initNav) {
        nav.set(initNav);
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName.get();
    }

    /**
     * @param initFileName the fileName to set
     */
    public void setFileName(String initFileName) {
        fileName.set(initFileName);
    }

    /**
     * @return the script
     */
    public String getScript() {
        return script.get();
    }

    /**
     * @param initScript the script to set
     */
    public void setScript(String initScript) {
        script.set(initScript);
    }
    
    /**
     * @return the use
     */
    public boolean isUse() {
        return use;
    }

    /**
     * @param use the use to set
     */
    public void setUse(boolean use) {
        this.use = use;
    }
        
    /**
     * EQUALS TO CHECK WHETHER A GIVEN Page IS EQUAL TO ANOTHER Page OR NOT
     * @param o
     * @return 
     */
    public boolean equals(Object o){
    	if (this == o) return true;
    	if (!(o instanceof Page)) return false;
		Page otherPage  = (Page) o;
        return (otherPage.getNav().equals(this.getNav()));    	
    }
    
}
