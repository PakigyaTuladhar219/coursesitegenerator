package csg.controller;

import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javax.json.JsonObject;
import csg.transaction.jTPS;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.TAManagerProp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import csg.transaction.saveDataTransaction;
import csg.validators.EmailValidator;
import tam.workspace.TAWorkspace;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @coauthor Pakigya Tuladhar
 * @version 1.0
 */
public class courseController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
 
    /**
     * Constructor, note that the app must already be constructed.
     */
    public courseController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    public void clearSelection(){
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        /*PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
       
        TableView studentTable = workspace.getStudentTable();
        
        TextField firstNameTextField = workspace.getFirstNameTextField();
        TextField lastNameTextField = workspace.getLastNameTextField();
        TextField roleTextField = workspace.getRoleTextField();
        ComboBox teamComboBox = workspace.getTeamComboBox();
        Button addButton = workspace.getAddUpdateButton();

        // CLEAR THE TEXT FIELDS
        teamComboBox.getSelectionModel().clearSelection();
        firstNameTextField.setText("");
        lastNameTextField.setText("");
        roleTextField.setText("");
        
        studentTable.getSelectionModel().clearSelection();
        addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        
        
        if (data.getStudents().isEmpty())
        {
            studentTable.getSelectionModel().clearSelection();
        // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            firstNameTextField.requestFocus();
        }
        updateToolBar(true);
*/
    }
    
    void updateToolBar(boolean IsChangeMade)
    {
        app.getGUI().getFileController().markFileAsNotSaved();
         //app.getFileController().markAsEdited(app.getGUI());
        app.getGUI().updateToolbarControls(!IsChangeMade);
         //app.getFileController().markFileAsNotSaved();
    }
}