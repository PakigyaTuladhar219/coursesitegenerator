package csg.controller;

import csg.data.Recitation;
import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import csg.transaction.jTPS;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.TAManagerProp;
import tam.data.TAData;
import csg.workspace.recitationWorkspace;
import djf.ui.AppGUI;
import javafx.scene.control.ComboBox;

/**
 *
 * @author PLT
 */
public class recitationController {

    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;

    // HERE WE KEEP TRACK OF TRANSACTIONS USING THE TRANSACTION PROCESSING SYSTEM 
    static jTPS jtps = new jTPS();
    
    public static jTPS getJTPS(){
        return jtps;
    } 
    /**
     * Constructor, note that the app must already be constructed.
     */
    public recitationController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    /**
     * This helper method should be called every time an edit happens.
     */
    private void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }
    
    /**
     * This method checks whether it is add or update
     */
    public void handleAddUpdateRecitation()
    {        
        // We will need the Properties to compare whether it is update or addRecitation
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        recitationWorkspace workspace = (recitationWorkspace)app.getRecitationWorkspaceComponent();
        Button addButton =  workspace.getAddUpdateButton();
        String buttonText = addButton.getText();
        String compareText = props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()).toLowerCase();
        if (buttonText.toLowerCase().contains(compareText))
        {
            handleAddRecitation();
        }
        else
        {
            handleUpdateRecitation();
        }
    }
    
    /**
     * This method responds to when the user requests to add
     * a new Recitation via the UI. Note that it must first do some
     * validation to make sure that there is some data
     * provided.
     */

    public void handleAddRecitation() {
        // GET THE TABLE
        recitationWorkspace workspace = (recitationWorkspace)app.getRecitationWorkspaceComponent();
        String section = workspace.getSectionTextField().getText();
        String instructor = workspace.getInstructorTextField().getText();
        String dayTime = workspace.getDayTimeTextField().getText();
        String location = workspace.getLocationTextField().getText();
        String TA1;
        String TA2;
        if(!workspace.getSupervisingTAComboBox1().getSelectionModel().isEmpty())
        {
            TA1 = workspace.getSupervisingTAComboBox1().getSelectionModel().getSelectedItem().toString();
        }
        else
        {
            TA1 = "";
        }
        //if (workspace.getSupervisingTAComboBox2().getSelectionModel().getSelectedItem().toString()!= null){
        
        if(!workspace.getSupervisingTAComboBox2().getSelectionModel().isEmpty())
        {
            TA2 = workspace.getSupervisingTAComboBox2().getSelectionModel().getSelectedItem().toString();
        }
        else
        {
            TA2 = "";
        }
        
        TAData data = (TAData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        //if (!section.isEmpty() || !instructor.isEmpty() || !dayTime.isEmpty() || !location.isEmpty() || !TA1.isEmpty() || !TA2.isEmpty())
        if (section.isEmpty() && instructor.isEmpty() && dayTime.isEmpty() && location.isEmpty() && TA1.isEmpty() && TA2.isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else if (section.isEmpty() || instructor.isEmpty() || dayTime.isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else if (TA1.equals(TA2) && !TA1.isEmpty() && !TA2.isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else
        {
            data.addRecitation(section, instructor, dayTime, location, TA1, TA2);
            clearUpdate();
            updateToolBar(true);
            handleAddTransaction();
        }
    }
    
     /**
     * This function provides updates Recitation when update is clicked.
     * 
     */
    public void handleUpdateRecitation() {// GET THE TABLE
        recitationWorkspace workspace = (recitationWorkspace)app.getRecitationWorkspaceComponent();
        TableView recitationTable = workspace.getRecitationTable();
        String section = workspace.getSectionTextField().getText();
        String instructor = workspace.getInstructorTextField().getText();
        String dayTime = workspace.getDayTimeTextField().getText();
        String location = workspace.getLocationTextField().getText();
        String TA1;
        String TA2;
        if(!workspace.getSupervisingTAComboBox1().getSelectionModel().isEmpty())
        {
            TA1 = workspace.getSupervisingTAComboBox1().getSelectionModel().getSelectedItem().toString();
        }
        else
        {
            TA1 = "";
        }
        if(!workspace.getSupervisingTAComboBox2().getSelectionModel().isEmpty())
        {
            TA2 = workspace.getSupervisingTAComboBox2().getSelectionModel().getSelectedItem().toString();
        }
        else
        {
            TA2 = "";
        }
        
        TAData data = (TAData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        // IS A Recitation SELECTED IN THE TABLE?
        Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();
        
        // GET THE Recitation
        Recitation rec1 = (Recitation)selectedItem;
        Recitation rec2 = new Recitation(section, instructor, dayTime, location, TA1, TA2);

        if (section.isEmpty() && instructor.isEmpty() && dayTime.isEmpty() && location.isEmpty() && TA1.isEmpty() && TA2.isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else if (TA1.equals(TA2) && !TA1.isEmpty() && !TA2.isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else
        {
            //data.deleteRecitation(rec1);
            //handleDeleteRecitation();
            //handleAddRecitation();
            data.updateRecitation(rec1, rec2);
            clearUpdate();
            //workspace.getAddUpdateButton().setDisable(true);
            updateToolBar(true);
            handleAddTransaction();
        }
    }

    
    /**
     * This method responds to when the user deletes 
     * a  Recitation via the UI. 
     */
    public void handleDeleteRecitation(){      
        // GET THE TABLE
        recitationWorkspace workspace = (recitationWorkspace) app.getRecitationWorkspaceComponent();
        TableView recitationTable = workspace.getRecitationTable();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // IS A Recitation SELECTED IN THE TABLE?
        if (recitationTable.getSelectionModel().isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else
        {
            Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();
            // GET THE Recitation
            Recitation rec = (Recitation)selectedItem;        
            data.deleteRecitation(rec);
            clearSelection();
            updateToolBar(true);
            //workspace.getAddUpdateButton().setDisable(true);
               
        }
    }
    
    /**
     * This function provides updates the data of the AddBox when add Recitation is clicked.
     * 
     */
    public void handleShowUpdateRecitation() {
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // GET THE TABLE
        
        recitationWorkspace workspace = (recitationWorkspace)app.getRecitationWorkspaceComponent();

        TableView recitationTable = workspace.getRecitationTable();
        TextField sectionTextField = workspace.getSectionTextField();
        TextField instructorTextField = workspace.getInstructorTextField();
        TextField dayTimeTextField = workspace.getDayTimeTextField();
        TextField locationTextField = workspace.getLocationTextField();
        ComboBox TA1ComboBox = workspace.getSupervisingTAComboBox1();
        ComboBox TA2ComboBox = workspace.getSupervisingTAComboBox2();
        Button addButton = workspace.getAddUpdateButton();
        
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        // IS ARecitation SELECTED IN THERecitationBLE?
        Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();
        
        // GET THERecitation
        Recitation rec = (Recitation) selectedItem;
        String recitationSection = rec.getSection();
        String recitationInstructor = rec.getInstructor();
        String recitationDayTime = rec.getDayTime();
        String recitationLocation = rec.getLocation();
        String recitationTA1 = rec.getSupervisingTA1();
        String recitationTA2 = rec.getSupervisingTA2();
        //if (selectedItem != null){
        if (!data.getRecitations().isEmpty())
        {
            sectionTextField.setText(recitationSection);
            instructorTextField.setText(recitationInstructor);
            dayTimeTextField.setText(recitationDayTime);
            locationTextField.setText(recitationLocation);
            TA1ComboBox.getSelectionModel().clearAndSelect(data.getTeachingAssistants().indexOf(data.getTA(recitationTA1)));
            TA2ComboBox.getSelectionModel().clearAndSelect(data.getTeachingAssistants().indexOf(data.getTA(recitationTA2)));
            changeToUpdate();
        }
        else
        {
            sectionTextField.setText("");
            instructorTextField.setText("");
            dayTimeTextField.setText("");
            locationTextField.setText("");
            TA1ComboBox.getSelectionModel().clearSelection();
            TA2ComboBox.getSelectionModel().clearSelection();
            recitationTable.getSelectionModel().clearSelection();
            addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        }
    }
    
     
    /**
     * This function changes the text on add/update Recitation button to 
     * AddRecitation
     */
    public void clearUpdate(){
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        recitationWorkspace workspace = (recitationWorkspace)app.getRecitationWorkspaceComponent();
        
        TableView recitationTable = workspace.getRecitationTable();
        
        
        TextField sectionTextField = workspace.getSectionTextField();
        TextField instructorTextField = workspace.getInstructorTextField();
        TextField dayTimeTextField = workspace.getDayTimeTextField();
        TextField locationTextField = workspace.getLocationTextField();
        ComboBox TA1ComboBox = workspace.getSupervisingTAComboBox1();
        ComboBox TA2ComboBox = workspace.getSupervisingTAComboBox2();
        
        Button addButton = workspace.getAddUpdateButton();

        // CLEAR THE TEXT FIELDS
        sectionTextField.setText("");
        instructorTextField.setText("");
        dayTimeTextField.setText("");
        locationTextField.setText("");
        TA1ComboBox.getSelectionModel().clearSelection();
        TA2ComboBox.getSelectionModel().clearSelection();
        //recitationTable.getSelectionModel().clearSelection();
        
        addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        
        // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
        sectionTextField.requestFocus();
        updateToolBar(true);
    }
    
    /**
     * This clears the table selection as well as the input fields when a recitation is deleted
     */
    public void clearSelection(){
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        recitationWorkspace workspace = (recitationWorkspace)app.getRecitationWorkspaceComponent();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        TableView recitationTable = workspace.getRecitationTable();
        
        TextField sectionTextField = workspace.getSectionTextField();
        TextField instructorTextField = workspace.getInstructorTextField();
        TextField dayTimeTextField = workspace.getDayTimeTextField();
        TextField locationTextField = workspace.getLocationTextField();
        ComboBox TA1ComboBox = workspace.getSupervisingTAComboBox1();
        ComboBox TA2ComboBox = workspace.getSupervisingTAComboBox2();
        
        Button addButton = workspace.getAddUpdateButton();

        // CLEAR THE TEXT FIELDS
        sectionTextField.setText("");
        instructorTextField.setText("");
        dayTimeTextField.setText("");
        locationTextField.setText("");
        TA1ComboBox.getSelectionModel().clearSelection();
        TA2ComboBox.getSelectionModel().clearSelection();
        
        
        if (data.getRecitations().isEmpty())
        {
            recitationTable.getSelectionModel().clearSelection();
        // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            sectionTextField.requestFocus();
        }
        
        addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        
        updateToolBar(true);
    }
    
    
    /**
     * This function changes the text of the add/update button to 
     * Update Recitation
     */
    
    public void changeToUpdate()
    {
        // GET THE WORKSPACE
        recitationWorkspace workspace = (recitationWorkspace)app.getRecitationWorkspaceComponent();
        
        // WE'LL NEED THIS TO ACCESS XML DATA
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Button addButton = workspace.getAddUpdateButton();
        addButton.setText(props.getProperty(TAManagerProp.UPDATE_BUTTON_TEXT.toString()));
    }   
    
    
    public void handleAddTransaction()
    {
        /*
        saveDataTransaction transaction = new saveDataTransaction(app);
        
        jtps.addTransaction(transaction);
        handleShowUpdateTA();
        //clearSelection();
        //clearUpdate();
    */
    }
    
    public void handleDoTransaction()
    {
        jtps.doTransaction();
        clearSelection();
    }
    
    public void handleUndoTransaction()
    {
        jtps.undoTransaction();
        clearSelection();
    }
    void updateToolBar(boolean IsChangeMade)
    {
        app.getGUI().getFileController().markFileAsNotSaved();
        app.getGUI().updateToolbarControls(!IsChangeMade);
    }
    
    
}
