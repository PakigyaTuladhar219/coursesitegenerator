package csg.controller;

import csg.data.Student;
import csg.data.Team;
import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import csg.transaction.jTPS;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.TAManagerProp;
import tam.data.TAData;
import csg.workspace.projectWorkspace;
import djf.ui.AppGUI;
import javafx.scene.control.ColorPicker;
//import java.firstName.LocalDate;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.paint.Color;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @coauthor Pakigya Tuladhar
 * @version 1.0
 */
public class projectController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;

    // HERE WE KEEP TRACK OF TRANSACTIONS USING THE TRANSACTION PROCESSING SYSTEM 
    static jTPS jtps = new jTPS();
    
    public static jTPS getJTPS(){
        return jtps;
    } 
    /**
     * Constructor, note that the app must already be constructed.
     */
    public projectController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    
    /**
     * This helper method should be called every firstName an edit happens.
     */
    private void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }
    
    /**
     * This method checks whether it is add or update
     */
    public void handleAddUpdateStudent()
    {        
        // We will need the Properties to compare whether it is update or addStudent
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        Button addButton =  workspace.getAddUpdateButton();
        String buttonText = addButton.getText();
        String compareText = props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()).toLowerCase();
        if (buttonText.toLowerCase().contains(compareText))
        {
            handleAddStudent();
        }
        else
        {
            handleUpdateStudent();
        }
    }
    
    /**
     * This method responds to when the user requests to add
     * a new Student via the UI. Note that it must first do some
     * validation to make sure that there is some data
     * provided.
     */

    public void handleAddStudent() {
        // GET THE TABLE
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        String firstName = workspace.getFirstNameTextField().getText();
        String lastName = workspace.getLastNameTextField().getText();
        String role = workspace.getRoleTextField().getText();
        String team;
        if(!workspace.getTeamComboBox().getSelectionModel().isEmpty())
        {
            team = workspace.getTeamComboBox().getSelectionModel().getSelectedItem().toString();
        }
        else
        {
            team = "";
        }
        
        TAData data = (TAData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        if (firstName.isEmpty() || lastName.isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else if (data.containsStudent(firstName,lastName))
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else
        {
            data.addStudent(firstName, lastName, team, role);
            clearUpdate();
            updateToolBar(true);
            handleAddTransaction();
        }
    }
    
     /**
     * This function provides updates Student when update is clicked.
     * 
     */
    public void handleUpdateStudent() {// GET THE TABLE
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        TableView studentTable = workspace.getStudentTable();
        String firstName = workspace.getFirstNameTextField().getText();
        String lastName = workspace.getLastNameTextField().getText();
        String role = workspace.getRoleTextField().getText();
        String team;
        if(!workspace.getTeamComboBox().getSelectionModel().isEmpty())
        {
            team = workspace.getTeamComboBox().getSelectionModel().getSelectedItem().toString();
        }
        else
        {
            team = "";
        }
        
        TAData data = (TAData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        // IS A Student SELECTED IN THE TABLE?
        Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
        
        // GET THE Student
        Student std1 = (Student)selectedItem;
        Student std2 = new Student(firstName, lastName, team, role);
 
        if (firstName.isEmpty() || lastName.isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else
        {
            data.deleteStudent(std1);
            if (data.containsStudent(std2))
            {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
                data.addStudent(std1);
            }
            else
            {
                // EVERYTHING IS FINE, UPDATE Student
                data.addStudent(std2);
                //data.updateStudent(std1, std2);
                clearUpdate();
                //workspace.getAddUpdateButton().setDisable(true);
                updateToolBar(true);
                handleAddTransaction();
            }
        }
    }

    
    /**
     * This method responds to when the user deletes 
     * a  Student via the UI. 
     */
    public void handleDeleteStudent(){      
        // GET THE TABLE
        projectWorkspace workspace = (projectWorkspace) app.getProjectWorkspaceComponent();
        TableView studentTable = workspace.getStudentTable();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // IS A Student SELECTED IN THE TABLE?
        if (studentTable.getSelectionModel().isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else
        {
            Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
            // GET THE Student
            Student std = (Student)selectedItem;        
            data.deleteStudent(std);
            clearSelection();
            updateToolBar(true);
            //workspace.getAddUpdateButton().setDisable(true);
               
        }
    }
    
    /**
     * This function provides updates the data of the AddBox when add Student is clicked.
     * 
     */
    public void handleShowUpdateStudent() {
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // GET THE TABLE
        
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();

        TableView studentTable = workspace.getStudentTable();
        TextField firstNameTextField = workspace.getFirstNameTextField();
        TextField lastNameTextField = workspace.getLastNameTextField();
        TextField roleTextField = workspace.getRoleTextField();
        ComboBox teamComboBox = workspace.getTeamComboBox();
        Button addButton = workspace.getAddUpdateButton();
        
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        // IS AStudent SELECTED IN THEStudentBLE?
        Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
        
        // GET THEStudent
        Student std = (Student) selectedItem;
        
        
        if (!data.getStudents().isEmpty())
        {
            
            String studentTeam = std.getTeam();
            String studentFirstName = std.getFirstName();
            String studentLastName = std.getLastName();
            String studentRole = std.getRole();
        
            teamComboBox.getSelectionModel().clearAndSelect(data.getTeamIndex(studentTeam));
            firstNameTextField.setText(studentFirstName);
            lastNameTextField.setText(studentLastName);
            roleTextField.setText(studentRole);
            changeToUpdate();
        }
        else
        {
            teamComboBox.getSelectionModel().clearSelection();
            firstNameTextField.setText("");
            lastNameTextField.setText("");
            roleTextField.setText("");
            
            studentTable.getSelectionModel().clearSelection();
            addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        }
    }
    
     
    /**
     * This function changes the text on add/update Student button to 
     * Add Student
     */
    public void clearUpdate(){
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        
        TableView studentTable = workspace.getStudentTable();
        TextField firstNameTextField = workspace.getFirstNameTextField();
        TextField lastNameTextField = workspace.getLastNameTextField();
        TextField roleTextField = workspace.getRoleTextField();
        ComboBox teamComboBox = workspace.getTeamComboBox();
        Button addButton = workspace.getAddUpdateButton();

        // CLEAR THE TEXT FIELDS
        teamComboBox.getSelectionModel().clearSelection();
        firstNameTextField.setText("");
        lastNameTextField.setText("");
        roleTextField.setText("");

        studentTable.getSelectionModel().clearSelection();
        addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        
        // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
        firstNameTextField.requestFocus();
        updateToolBar(true);
    }
    
    /**
     * This clears the table selection as well as the input fields when a student is deleted
     */
    public void clearSelection(){
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
       
        TableView studentTable = workspace.getStudentTable();
        
        TextField firstNameTextField = workspace.getFirstNameTextField();
        TextField lastNameTextField = workspace.getLastNameTextField();
        TextField roleTextField = workspace.getRoleTextField();
        ComboBox teamComboBox = workspace.getTeamComboBox();
        Button addButton = workspace.getAddUpdateButton();

        // CLEAR THE TEXT FIELDS
        teamComboBox.getSelectionModel().clearSelection();
        firstNameTextField.setText("");
        lastNameTextField.setText("");
        roleTextField.setText("");
        
        studentTable.getSelectionModel().clearSelection();
        addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        
        
        if (data.getStudents().isEmpty())
        {
            studentTable.getSelectionModel().clearSelection();
        // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            firstNameTextField.requestFocus();
        }
        updateToolBar(true);
    }
        
    /**
     * This function changes the text of the add/update button to 
     * Update Student
     */
    
    public void changeToUpdate()
    {
        // GET THE WORKSPACE
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        
        // WE'LL NEED THIS TO ACCESS XML DATA
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Button addButton = workspace.getAddUpdateButton();
        addButton.setText(props.getProperty(TAManagerProp.UPDATE_BUTTON_TEXT.toString()));
    }   
    
    
    
    
    
    
    
    // FOR TEAMS 
    
    /**
     * This method checks whether it is add or update
     */
    public void handleAddUpdateTeam()
    {        
        // We will need the Properties to compare whether it is update or addTeam
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        Button addButton =  workspace.getAddUpdateTeamButton();
        String buttonText = addButton.getText();
        String compareText = props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()).toLowerCase();
        if (buttonText.toLowerCase().contains(compareText))
        {
            handleAddTeam();
        }
        else
        {
            handleUpdateTeam();
        }
    }
    
    /**
     * This method responds to when the user requests to add
     * a new Team via the UI. Note that it must first do some
     * validation to make sure that there is some data
     * provided.
     */
    public String getColorCode(ColorPicker cp){
        return ("#" + cp.getValue().toString().substring(2,8));
    }
    public void handleAddTeam() {
        // GET THE TABLE
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        String name = workspace.getNameTextField().getText();
        String link = workspace.getLinkTextField().getText();
        String bgColor = "#" + workspace.getBgColorPicker().getValue().toString().substring(2,8);
        String textColor = "#" + workspace.getTextColorPicker().getValue().toString().substring(2,8);
        
        TAData data = (TAData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        if (name.isEmpty() || link.isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else if (data.containsTeam(name))
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else if (data.containsBgColor(bgColor))
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else if (bgColor.equals(textColor))
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else
        {
            data.addTeam(name, bgColor, textColor, link);
            clearTeamUpdate();
            updateToolBar(true);
            handleAddTransaction();
        }
    }
    
     /**
     * This function provides updates Team when update is clicked.
     * 
     */
    public void handleUpdateTeam() {
        // GET THE TABLE
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        TableView teamTable = workspace.getTeamTable();
        String name = workspace.getNameTextField().getText();
        String link = workspace.getLinkTextField().getText();
        String bgColor = "#" + workspace.getBgColorPicker().getValue().toString().substring(2,8);
        String textColor = "#" + workspace.getTextColorPicker().getValue().toString().substring(2,8);
        
        TAData data = (TAData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // IS A Team SELECTED IN THE TABLE?
        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
        
        // GET THE Team
        Team team1 = (Team)selectedItem;
        team1 = data.getTeam(team1.getName());
        Team team2 = new Team(name, bgColor, textColor, link);

        if (name.isEmpty() || link.isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else 
        {
            data.tempDeleteTeam(team1);
            if (data.containsTeam(name))
            {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));
                data.addTeam(team1);
            }
            else if (data.containsBgColor(bgColor))
            {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));
                data.addTeam(team1);
            }
            else
            {
                // EVERYTHING IS FINE, UPDATE Team
                data.addTeam(team1);
                data.updateTeam(team1, team2);
                clearTeamUpdate();
                //workspace.getAddUpdateTeamButton().setDisable(true);
                updateToolBar(true);
                handleAddTransaction();
            }
        }
    }

    
    /**
     * This method responds to when the user deletes 
     * a  Team via the UI. 
     */
    public void handleDeleteTeam(){      
        // GET THE TABLE
        projectWorkspace workspace = (projectWorkspace) app.getProjectWorkspaceComponent();
        TableView teamTable = workspace.getTeamTable();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // IS A TEAM SELECTED IN THE TABLE?
        if (teamTable.getSelectionModel().isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else
        {
            Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
            // GET THE Team
            Team team = (Team)selectedItem;        
            data.deleteTeam(team);
            clearTeamSelection();
            updateToolBar(true);
            //workspace.getAddUpdateTeamButton().setDisable(true);
               
        }
    }
    
    /**
     * This function provides updates the data of the AddBox when add Team is clicked.
     * 
     */
    public void handleShowUpdateTeam() {
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // GET THE TABLE
        
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();

        TableView teamTable = workspace.getTeamTable();
        TextField nameTextField = workspace.getNameTextField();
        TextField linkTextField = workspace.getLinkTextField();
        ColorPicker bgColorPicker = workspace.getBgColorPicker();
        ColorPicker textColorPicker = workspace.getTextColorPicker();
        Button addButton = workspace.getAddUpdateTeamButton();
        
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        // IS ATeam SELECTED IN THETeamBLE?
        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
        
        // GET THETeam
        Team team = (Team) selectedItem;
        
        
        if (!data.getTeams().isEmpty())
        {
            String teamName = team.getName();
            String teamLink = team.getLink();
        
            nameTextField.setText(teamName);
            bgColorPicker.setValue(Color.web(team.getBGcolor()));
            textColorPicker.setValue(Color.web(team.getTextColor()));
            linkTextField.setText(teamLink);
            changeToTeamUpdate();
        }
        else
        {
            
            nameTextField.setText("");
            linkTextField.setText("");
            textColorPicker.setValue(Color.web("FFFFFF"));
            textColorPicker.setValue(Color.web("FFFFFF"));
            
            teamTable.getSelectionModel().clearSelection();
            addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        }
    }
    
     
    /**
     * This function changes the text on add/update Team button to 
     * Add Team
     */
    public void clearTeamUpdate(){
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        
        TableView teamTable = workspace.getTeamTable();
        TextField nameTextField = workspace.getNameTextField();
        TextField linkTextField = workspace.getLinkTextField();
        ColorPicker bgColorPicker = workspace.getBgColorPicker();
        ColorPicker textColorPicker = workspace.getTextColorPicker();
        Button addButton = workspace.getAddUpdateTeamButton();
        
        // CLEAR THE TEXT FIELDS
        
        nameTextField.setText("");
        linkTextField.setText("");
        bgColorPicker.setValue(Color.web("FFFFFF"));
        textColorPicker.setValue(Color.web("FFFFFF"));

        teamTable.getSelectionModel().clearSelection();
        addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        
        // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
        nameTextField.requestFocus();
        updateToolBar(true);
    }
    
    /**
     * This clears the table selection as well as the input fields when a team is deleted
     */
    public void clearTeamSelection(){
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
       
        TableView teamTable = workspace.getTeamTable();
        
        TextField nameTextField = workspace.getNameTextField();
        TextField linkTextField = workspace.getLinkTextField();
        
        ColorPicker bgColorPicker = workspace.getBgColorPicker();
        ColorPicker textColorPicker = workspace.getTextColorPicker();
        Button addButton = workspace.getAddUpdateTeamButton();

        // CLEAR THE TEXT FIELDS
        nameTextField.setText("");
        linkTextField.setText("");
        bgColorPicker.setValue(Color.web("FFFFFF"));
        textColorPicker.setValue(Color.web("FFFFFF"));
        
        teamTable.getSelectionModel().clearSelection();
        addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        
        
        if (data.getTeams().isEmpty())
        {
            teamTable.getSelectionModel().clearSelection();
        // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
        }
        updateToolBar(true);
    }
        
    /**
     * This function changes the text of the add/update button to 
     * Update Team
     */
    
    public void changeToTeamUpdate()
    {
        // GET THE WORKSPACE
        projectWorkspace workspace = (projectWorkspace)app.getProjectWorkspaceComponent();
        
        // WE'LL NEED THIS TO ACCESS XML DATA
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Button addButton = workspace.getAddUpdateTeamButton();
        addButton.setText(props.getProperty(TAManagerProp.UPDATE_BUTTON_TEXT.toString()));
    }   
    
    
    
    
    
    public void handleAddTransaction()
    {
        /*
        saveDataTransaction transaction = new saveDataTransaction(app);
        
        jtps.addTransaction(transaction);
        handleShowUpdateTA();
        //clearSelection();
        //clearUpdate();
    */
    }
    
    public void handleDoTransaction()
    {
        jtps.doTransaction();
        clearSelection();
    }
    
    public void handleUndoTransaction()
    {
        jtps.undoTransaction();
        clearSelection();
    }
    void updateToolBar(boolean IsChangeMade)
    {
        app.getGUI().getFileController().markFileAsNotSaved();
        app.getGUI().updateToolbarControls(!IsChangeMade);
    }

}