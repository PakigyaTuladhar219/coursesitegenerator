package csg.controller;

import static tam.TAManagerProp.*;
import csg.data.Schedule;
import djf.ui.AppMessageDialogSingleton;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import csg.transaction.jTPS;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.TAManagerProp;
import tam.data.TAData;
import csg.workspace.scheduleWorkspace;
import djf.ui.AppGUI;
import java.time.LocalDate;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @coauthor Pakigya Tuladhar
 * @version 1.0
 */
public class scheduleController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;

    // HERE WE KEEP TRACK OF TRANSACTIONS USING THE TRANSACTION PROCESSING SYSTEM 
    static jTPS jtps = new jTPS();
    
    public static jTPS getJTPS(){
        return jtps;
    } 
    /**
     * Constructor, note that the app must already be constructed.
     */
    public scheduleController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }

    /**
     * This helper method should be called every time an edit happens.
     */
    private void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }
    
    /**
     * This method checks whether it is add or update
     */
    public void handleAddUpdateSchedule()
    {        
        // We will need the Properties to compare whether it is update or addSchedule
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        scheduleWorkspace workspace = (scheduleWorkspace)app.getScheduleWorkspaceComponent();
        Button addButton =  workspace.getAddUpdateButton();
        String buttonText = addButton.getText();
        String compareText = props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()).toLowerCase();
        if (buttonText.toLowerCase().contains(compareText))
        {
            handleAddSchedule();
        }
        else
        {
            handleUpdateSchedule();
        }
    }
    
    /**
     * This method responds to when the user requests to add
     * a new Schedule via the UI. Note that it must first do some
     * validation to make sure that there is some data
     * provided.
     */

    public void handleAddSchedule() {
        // GET THE TABLE
        scheduleWorkspace workspace = (scheduleWorkspace)app.getScheduleWorkspaceComponent();
        String date = workspace.getaddDatePicker().getValue().getMonthValue() + "/" + workspace.getaddDatePicker().getValue().getDayOfMonth() + "/" + workspace.getaddDatePicker().getValue().getYear();
        String time = workspace.getDayTimeTextField().getText();
        String title = workspace.getTitleTextField().getText();
        String topic = workspace.getTopicTextField().getText();
        String link = workspace.getLinkTextField().getText();
        String criteria = workspace.getCriteriaTextField().getText();
        String type;
        if(!workspace.getTypeComboBox().getSelectionModel().isEmpty())
        {
            type = workspace.getTypeComboBox().getSelectionModel().getSelectedItem().toString();
        }
        else
        {
            type = "";
        }
        
        TAData data = (TAData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        if (date.isEmpty() || type.isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else
        {
            data.addSchedule(type, date, time, title, topic, link, criteria);
            clearUpdate();
            updateToolBar(true);
            handleAddTransaction();
        }
    }
    
     /**
     * This function provides updates Schedule when update is clicked.
     * 
     */
    public void handleUpdateSchedule() {// GET THE TABLE
        scheduleWorkspace workspace = (scheduleWorkspace)app.getScheduleWorkspaceComponent();
        TableView scheduleTable = workspace.getScheduleTable();
        String date = workspace.getaddDatePicker().getValue().getMonthValue() + "/" + workspace.getaddDatePicker().getValue().getDayOfMonth() + "/" + workspace.getaddDatePicker().getValue().getYear();
        String time = workspace.getDayTimeTextField().getText();
        String title = workspace.getTitleTextField().getText();
        String topic = workspace.getTopicTextField().getText();
        String link = workspace.getLinkTextField().getText();
        String criteria = workspace.getCriteriaTextField().getText();
        String type;
        if(!workspace.getTypeComboBox().getSelectionModel().isEmpty())
        {
            type = workspace.getTypeComboBox().getSelectionModel().getSelectedItem().toString();
        }
        else
        {
            type = "";
        }
        
        TAData data = (TAData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        // IS A Schedule SELECTED IN THE TABLE?
        Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
        
        // GET THE Schedule
        Schedule sche1 = (Schedule)selectedItem;
        Schedule sche2 = new Schedule(type, date, time, title, topic, link, criteria);

        if (date.isEmpty() || type.isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else
        {
            data.updateSchedule(sche1, sche2);
            clearUpdate();
            //workspace.getAddUpdateButton().setDisable(true);
            updateToolBar(true);
            handleAddTransaction();
        }
    }

    
    /**
     * This method responds to when the user deletes 
     * a  Schedule via the UI. 
     */
    public void handleDeleteSchedule(){      
        // GET THE TABLE
        scheduleWorkspace workspace = (scheduleWorkspace) app.getScheduleWorkspaceComponent();
        TableView scheduleTable = workspace.getScheduleTable();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // IS A Schedule SELECTED IN THE TABLE?
        if (scheduleTable.getSelectionModel().isEmpty())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATE_TITLE), props.getProperty(INVALID_DATE_MESSAGE));  
        }
        else
        {
            Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
            // GET THE Schedule
            Schedule sche = (Schedule)selectedItem;        
            data.deleteSchedule(sche);
            clearSelection();
            updateToolBar(true);
            //workspace.getAddUpdateButton().setDisable(true);
               
        }
    }
    
    /**
     * This function provides updates the data of the AddBox when add Schedule is clicked.
     * 
     */
    public void handleShowUpdateSchedule() {
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // GET THE TABLE
        
        scheduleWorkspace workspace = (scheduleWorkspace)app.getScheduleWorkspaceComponent();

        TableView scheduleTable = workspace.getScheduleTable();
        TextField timeTextField = workspace.getDayTimeTextField();
        TextField titleTextField = workspace.getTitleTextField();
        TextField topicTextField = workspace.getTopicTextField();
        TextField linkTextField = workspace.getLinkTextField();
        TextField criteriaTextField = workspace.getCriteriaTextField();
        ComboBox typeComboBox = workspace.getTypeComboBox();
        DatePicker addDatePicker = workspace.getaddDatePicker();
        Button addButton = workspace.getAddUpdateButton();
        
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        // IS ASchedule SELECTED IN THEScheduleBLE?
        Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
        
        // GET THESchedule
        Schedule sche = (Schedule) selectedItem;
        
        
        if (!data.getSchedules().isEmpty())
        {
            
            String scheduleType = sche.getType();
            LocalDate scheduleDate = sche.getDateFormat();
            String scheduleDayTime = sche.getTime();
            String scheduleTitle = sche.getTitle();
            String scheduleTopic = sche.getTopic();
            String scheduleLink = sche.getLink();
            String scheduleCriteria = sche.getCriteria();
        
            typeComboBox.getSelectionModel().clearAndSelect(data.getTypeIndex(scheduleType));
            addDatePicker.setValue(scheduleDate);
            timeTextField.setText(scheduleDayTime);
            titleTextField.setText(scheduleTitle);
            topicTextField.setText(scheduleTopic);
            linkTextField.setText(scheduleLink);
            criteriaTextField.setText(scheduleCriteria);
            changeToUpdate();
        }
        else
        {
            typeComboBox.getSelectionModel().clearSelection();
            addDatePicker.setValue(LocalDate.now());
            timeTextField.setText("");
            titleTextField.setText("");
            topicTextField.setText("");
            linkTextField.setText("");
            criteriaTextField.setText("");
            
            scheduleTable.getSelectionModel().clearSelection();
            addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        }
    }
    
     
    /**
     * This function changes the text on add/update Schedule button to 
     * Add Schedule
     */
    public void clearUpdate(){
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        scheduleWorkspace workspace = (scheduleWorkspace)app.getScheduleWorkspaceComponent();
        
        TableView scheduleTable = workspace.getScheduleTable();
        TextField timeTextField = workspace.getDayTimeTextField();
        TextField titleTextField = workspace.getTitleTextField();
        TextField topicTextField = workspace.getTopicTextField();
        TextField linkTextField = workspace.getLinkTextField();
        TextField criteriaTextField = workspace.getCriteriaTextField();
        ComboBox typeComboBox = workspace.getTypeComboBox();
        DatePicker addDatePicker = workspace.getaddDatePicker();
        Button addButton = workspace.getAddUpdateButton();

        // CLEAR THE TEXT FIELDS
        typeComboBox.getSelectionModel().clearSelection();
        addDatePicker.setValue(LocalDate.now());
        timeTextField.setText("");
        titleTextField.setText("");
        topicTextField.setText("");
        linkTextField.setText("");
        criteriaTextField.setText("");

        scheduleTable.getSelectionModel().clearSelection();
        addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        
        // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
        timeTextField.requestFocus();
        updateToolBar(true);
    }
    
    /**
     * This clears the table selection as well as the input fields when a schedule is deleted
     */
    public void clearSelection(){
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        scheduleWorkspace workspace = (scheduleWorkspace)app.getScheduleWorkspaceComponent();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
       
        TableView scheduleTable = workspace.getScheduleTable();
        
        TextField timeTextField = workspace.getDayTimeTextField();
        TextField titleTextField = workspace.getTitleTextField();
        TextField topicTextField = workspace.getTopicTextField();
        TextField linkTextField = workspace.getLinkTextField();
        TextField criteriaTextField = workspace.getCriteriaTextField();
        ComboBox typeComboBox = workspace.getTypeComboBox();
        DatePicker addDatePicker = workspace.getaddDatePicker();
        Button addButton = workspace.getAddUpdateButton();

        // CLEAR THE TEXT FIELDS
        typeComboBox.getSelectionModel().clearSelection();
        addDatePicker.setValue(LocalDate.now());
        timeTextField.setText("");
        titleTextField.setText("");
        topicTextField.setText("");
        linkTextField.setText("");
        criteriaTextField.setText("");
        
        scheduleTable.getSelectionModel().clearSelection();
        addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        
        
        if (data.getSchedules().isEmpty())
        {
            scheduleTable.getSelectionModel().clearSelection();
        // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            timeTextField.requestFocus();
        }
        updateToolBar(true);
    }
    
    
    public void handleStartDateChange() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.     
    }

    public void handleEndDateChange() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * This function changes the text of the add/update button to 
     * Update Schedule
     */
    
    public void changeToUpdate()
    {
        // GET THE WORKSPACE
        scheduleWorkspace workspace = (scheduleWorkspace)app.getScheduleWorkspaceComponent();
        
        // WE'LL NEED THIS TO ACCESS XML DATA
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Button addButton = workspace.getAddUpdateButton();
        addButton.setText(props.getProperty(TAManagerProp.UPDATE_BUTTON_TEXT.toString()));
    }   
    
    
    public void handleAddTransaction()
    {
        /*
        saveDataTransaction transaction = new saveDataTransaction(app);
        
        jtps.addTransaction(transaction);
        handleShowUpdateTA();
        //clearSelection();
        //clearUpdate();
    */
    }
    
    public void handleDoTransaction()
    {
        jtps.doTransaction();
        clearSelection();
    }
    
    public void handleUndoTransaction()
    {
        jtps.undoTransaction();
        clearSelection();
    }
    void updateToolBar(boolean IsChangeMade)
    {
        app.getGUI().getFileController().markFileAsNotSaved();
        app.getGUI().updateToolbarControls(!IsChangeMade);
    }


}