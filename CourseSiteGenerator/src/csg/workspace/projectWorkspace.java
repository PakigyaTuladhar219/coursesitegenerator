/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.controller.projectController;
import csg.data.Student;
import csg.data.Team;
import djf.components.AppDataComponent;
import djf.components.AppProjectWorkspaceComponent;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import tam.ProjectProp;
import tam.TAManagerApp;
import tam.data.TAData;

/**
 *
 * @author PLT
 */
public class projectWorkspace extends AppProjectWorkspaceComponent {

    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    TAManagerApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    projectController controller;

    // Check if the first transaction is done or not
    boolean isFirstTransactionDone=false;

      /**
     * The constructor initializes the user interface, except for
     * the full office hours grid, since it doesn't yet know what
     * the hours will be until a file is loaded or a new one is created.
     */        
        
    protected TableView studentTable;
    protected TableColumn firstNameColumn;
    protected TableColumn lastNameColumn;
    protected TableColumn teamColumn;
    protected TableColumn roleColumn;
    
    
    protected TableView teamTable;
    protected TableColumn nameColumn;
    protected TableColumn colorColumn;
    protected TableColumn colorTextColumn;
    protected TableColumn linkColumn;
    
    
    protected Label projectsLabel;

    protected ColorPicker bgColorPicker;
    protected ColorPicker textColorPicker;
    
    protected Label teamsLabel;
    protected Label add_editTeamLabel;
    protected Label nameLabel;
    protected Label colorLabel;
    protected Label textColorLabel;
    protected Label linkLabel;
    
    protected Label studentsLabel;
    protected Label add_editLabel;
    protected Label firstNameLabel;
    protected Label lastNameLabel;
    protected Label teamLabel;
    protected Label roleLabel;
    
    protected TextField nameTextField;
    protected TextField linkTextField;
    
    protected TextField firstNameTextField;
    protected TextField lastNameTextField;
    protected ComboBox teamComboBox;
    protected TextField roleTextField;
    
    protected Button removeButton;
    protected Button addUpdateButton;
    protected Button clearButton;
    
    protected Button removeTeamButton;
    protected Button addUpdateTeamButton;
    protected Button clearTeamButton;
    
    protected GridPane addEditGridPane;
    
    
    protected VBox partVbox1 = new VBox();
    protected VBox partVbox2 = new VBox();
    protected VBox partVbox3 = new VBox();
    protected VBox allVbox = new VBox();

    public projectWorkspace(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
    projectsLabel = new Label(props.getProperty(ProjectProp.PROJECTS_LABEL_TEXT.toString()));
    teamsLabel = new Label(props.getProperty(ProjectProp.TEAMS_LABEL_TEXT.toString()));
    studentsLabel = new Label(props.getProperty(ProjectProp.STUDENTS_HEADER_TEXT.toString()));
    
    bgColorPicker = new ColorPicker();
    textColorPicker = new ColorPicker();

    bgColorPicker.setPrefWidth(150);
    textColorPicker.setPrefWidth(150);
    bgColorPicker.setPrefHeight(150);
    textColorPicker.setPrefHeight(150);

    
    teamTable = new TableView();
    nameColumn = new TableColumn(props.getProperty(ProjectProp.NAME_HEADER_TEXT.toString()));
    colorColumn = new TableColumn(props.getProperty(ProjectProp.COLOR_HEADER_TEXT.toString()));
    colorTextColumn = new TableColumn(props.getProperty(ProjectProp.TEXT_COLOR_HEADER_TEXT.toString()));
    linkColumn = new TableColumn(props.getProperty(ProjectProp.LINK_HEADER_TEXT.toString()));
    
    studentTable = new TableView();
    firstNameColumn = new TableColumn(props.getProperty(ProjectProp.FIRST_NAME_HEADER_TEXT.toString()));
    lastNameColumn = new TableColumn(props.getProperty(ProjectProp.LAST_NAME_HEADER_TEXT.toString()));
    teamColumn = new TableColumn(props.getProperty(ProjectProp.TEAM_HEADER_TEXT.toString()));
    roleColumn = new TableColumn(props.getProperty(ProjectProp.ROLE_HEADER_TEXT.toString()));
    
    
    
    add_editTeamLabel = new Label(props.getProperty(ProjectProp.ADD_EDIT_LABEL_TEXT.toString()));
    nameLabel = new Label(props.getProperty(ProjectProp.NAME_LABEL_TEXT.toString()));
    colorLabel = new Label(props.getProperty(ProjectProp.COLOR_LABEL_TEXT.toString()));
    textColorLabel = new Label(props.getProperty(ProjectProp.TEXT_COLOR_LABEL_TEXT.toString()));
    linkLabel = new Label(props.getProperty(ProjectProp.LINK_LABEL_TEXT.toString()));
    
    add_editLabel = new Label(props.getProperty(ProjectProp.ADD_EDIT_LABEL_TEXT.toString()));
    firstNameLabel = new Label(props.getProperty(ProjectProp.FIRST_NAME_LABEL_TEXT.toString()));
    lastNameLabel = new Label(props.getProperty(ProjectProp.LAST_NAME_LABEL_TEXT.toString()));
    teamLabel = new Label(props.getProperty(ProjectProp.TEAM_LABEL_TEXT.toString()));
    roleLabel = new Label(props.getProperty(ProjectProp.ROLE_LABEL_TEXT.toString()));
    
    nameTextField = new TextField();
    roleTextField = new TextField();
    firstNameTextField = new TextField();
    lastNameTextField = new TextField();
    linkTextField = new TextField();
    teamComboBox = new ComboBox();
    
    removeButton = new Button("-");
    addUpdateButton = new Button(props.getProperty(ProjectProp.ADD_BUTTON_TEXT.toString()));
    clearButton = new Button(props.getProperty(ProjectProp.CLEAR_TEXT.toString()));
    
    removeTeamButton = new Button("-");
    addUpdateTeamButton = new Button(props.getProperty(ProjectProp.ADD_BUTTON_TEXT.toString()));
    clearTeamButton = new Button(props.getProperty(ProjectProp.CLEAR_TEXT.toString()));
    
    
    HBox header1 = new HBox();
    header1.getChildren().add(teamsLabel);
    header1.getChildren().add(removeTeamButton);
    partVbox2.getChildren().add(header1);
    
    
    
        nameColumn.prefWidthProperty().bind(teamTable.widthProperty().multiply(0.20)); 
        colorColumn.prefWidthProperty().bind(teamTable.widthProperty().multiply(0.25)); 
        colorTextColumn.prefWidthProperty().bind(teamTable.widthProperty().multiply(0.25)); 
        linkColumn.prefWidthProperty().bind(teamTable.widthProperty().multiply(0.30)); 
        
        TAData data = (TAData) app.getDataComponent();
        
        ObservableList<Team> tableData = data.getTeams();
        
        teamTable.setItems(tableData);
        teamTable.setEditable(true);
        
        
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<Team, String>("name")
        ); 
        
        colorColumn.setCellValueFactory(
                new PropertyValueFactory<Team, String>("textColor")
                //new PropertyValueFactory<Team, String>("bgColor")
        ); 
        
        colorTextColumn.setCellValueFactory(
                new PropertyValueFactory<Team, String>("textColor")
        ); 
        
        linkColumn.setCellValueFactory(
                new PropertyValueFactory<Team, String>("link")
        ); 
        
        teamTable.getColumns().add(nameColumn);
        teamTable.getColumns().add(colorColumn);
        teamTable.getColumns().add(colorTextColumn);
        teamTable.getColumns().add(linkColumn);
        
        teamTable.prefWidthProperty().bind(partVbox2.widthProperty().multiply(1)); 
        
    
    //GridPane teamGrid = new GridPane();
    int i =0;
        
    partVbox2.getChildren().add(teamTable);
    partVbox2.getChildren().add(add_editLabel);

    GridPane inputTeamSection  = new GridPane();
    i =0;

    inputTeamSection.add(nameLabel,0,i);
    inputTeamSection.add(nameTextField,1,i++);
    inputTeamSection.add(colorLabel,0,i);
    inputTeamSection.add(bgColorPicker,1,i);
    inputTeamSection.add(textColorLabel,2,i);
    inputTeamSection.add(textColorPicker,3,i++);
    inputTeamSection.add(linkLabel,0,i);
    inputTeamSection.add(linkTextField,1,i++,3,1); //column span and row span
    inputTeamSection.add(addUpdateTeamButton,0,i);
    inputTeamSection.add(clearTeamButton,1,i++);
    
    partVbox2.getChildren().add(inputTeamSection);
    
    
    
    
    
    HBox header = new HBox();
    header.getChildren().add(studentsLabel);
    header.getChildren().add(removeButton);
    partVbox3.getChildren().add(header);
    
      firstNameColumn.prefWidthProperty().bind(studentTable.widthProperty().multiply(0.20)); 
        lastNameColumn.prefWidthProperty().bind(studentTable.widthProperty().multiply(0.25)); 
        teamColumn.prefWidthProperty().bind(studentTable.widthProperty().multiply(0.25)); 
        roleColumn.prefWidthProperty().bind(studentTable.widthProperty().multiply(0.30)); 
        
        
        ObservableList<Student> tableStudentData = data.getStudents();
        
        studentTable.setItems(tableStudentData);
        studentTable.setEditable(true);
        
        
        firstNameColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("firstName")
        ); 
        
        lastNameColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("lastName")
        ); 
        
        
        teamColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("team")
        ); 
        
        roleColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("role")
        ); 
        
        studentTable.getColumns().add(firstNameColumn);
        studentTable.getColumns().add(lastNameColumn);
        studentTable.getColumns().add(teamColumn);
        studentTable.getColumns().add(roleColumn);
        
        studentTable.prefWidthProperty().bind(partVbox3.widthProperty().multiply(1)); 
        
    partVbox3.getChildren().add(studentTable);
   
    
    partVbox3.getChildren().add(add_editLabel);

    GridPane inputSection  = new GridPane();
    i =0;

    inputSection.add(firstNameLabel,0,i);
    inputSection.add(firstNameTextField,1,i++);
    inputSection.add(lastNameLabel,0,i);
    inputSection.add(lastNameTextField,1,i++);
    inputSection.add(teamLabel,0,i);
    inputSection.add(teamComboBox,1,i++);
    inputSection.add(roleLabel,0,i);
    inputSection.add(roleTextField,1,i++);
    inputSection.add(addUpdateButton,0,i);
    inputSection.add(clearButton,1,i++);
    
    
    teamComboBox.setItems(data.getTeams());
    
    partVbox3.getChildren().add(inputSection);
    
    VBox allVbox = new VBox();
    partVbox1.getChildren().add(projectsLabel);
    allVbox.getChildren().add(partVbox1);
    allVbox.getChildren().add(partVbox2);
    allVbox.getChildren().add(partVbox3);
    
    allVbox.prefWidthProperty().bind(app.getGUI().getAppPane().widthProperty().multiply(1));
    
        workspace =  new Pane(allVbox); 
        controller = new projectController(app);
        //ADDING THE FIRST TRANSACTION WHEN THE APP LOADS
        
        if (data.isLoaded)
        {
            controller.handleAddTransaction();
            changeColorPickerColors();
        }
        
        // FOR TEAMS
        
        addUpdateTeamButton.setOnAction(e -> {
            updateInitialTransaction();
            controller.handleAddUpdateTeam();
            changeColorPickerColors();
        });
        
        clearTeamButton.setOnAction(e -> {
            updateInitialTransaction();
           controller.clearTeamUpdate(); 
            changeColorPickerColors();
        });
       
       teamTable.setOnMouseClicked(e ->{
            updateInitialTransaction();
            controller.handleShowUpdateTeam();
            changeColorPickerColors();
       });
       
       // FOR DELETING TA
       teamTable.setOnKeyPressed(e -> {
           if (e.getCode() == KeyCode.DELETE )
           {
                updateInitialTransaction();
                controller.handleDeleteTeam();
                changeColorPickerColors();
           }  
       });
       
       removeTeamButton.setOnAction(e -> {
            updateInitialTransaction();
            controller.handleDeleteTeam();
            changeColorPickerColors();
        });
        
       // FOR STUDENTS
       
       addUpdateButton.setOnAction(e -> {
            updateInitialTransaction();
            controller.handleAddUpdateStudent();
        });
        
        clearButton.setOnAction(e -> {
            updateInitialTransaction();
           controller.clearUpdate(); 
        });
       
       studentTable.setOnMouseClicked(e ->{
            updateInitialTransaction();
            controller.handleShowUpdateStudent();
       });
       
       // FOR DELETING TA
       studentTable.setOnKeyPressed(e -> {
           if (e.getCode() == KeyCode.DELETE )
           {
                updateInitialTransaction();
                controller.handleDeleteStudent();
           }  
       });
       
       removeButton.setOnAction(e -> {
            updateInitialTransaction();
            controller.handleDeleteStudent();
        });
       
       
       bgColorPicker.setOnAction(e -> {
            //updateInitialTransaction();
            changeColorPickerColors();
       });
       
       textColorPicker.setOnAction(e -> {
            //updateInitialTransaction();
            changeColorPickerColors();
       });
       
            changeColorPickerColors();
    }
    
    private void changeColorPickerColors(){
        bgColorPicker.setStyle("-fx-background-color: "+ controller.getColorCode(bgColorPicker));
        textColorPicker.setStyle("-fx-background-color: "+ controller.getColorCode(textColorPicker));
    }
    
    private void updateInitialTransaction(){
        if(!isFirstTransactionDone){ controller.handleAddTransaction(); isFirstTransactionDone=true;}
    }
    
    @Override
    public void resetWorkspace() {
        controller.clearSelection();
        controller.clearTeamSelection();
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        
    }

    /**
     * @return the studentTable
     */
    public TableView getStudentTable() {
        return studentTable;
    }

    /**
     * @return the firstNameColumn
     */
    public TableColumn getFirstNameColumn() {
        return firstNameColumn;
    }

    /**
     * @return the lastNameColumn
     */
    public TableColumn getLastNameColumn() {
        return lastNameColumn;
    }

    /**
     * @return the teamColumn
     */
    public TableColumn getTeamColumn() {
        return teamColumn;
    }

    /**
     * @return the roleColumn
     */
    public TableColumn getRoleColumn() {
        return roleColumn;
    }

    /**
     * @return the teamTable
     */
    public TableView getTeamTable() {
        return teamTable;
    }

    /**
     * @return the nameColumn
     */
    public TableColumn getNameColumn() {
        return nameColumn;
    }

    /**
     * @return the colorColumn
     */
    public TableColumn getColorColumn() {
        return colorColumn;
    }

    /**
     * @return the colorTextColumn
     */
    public TableColumn getColorTextColumn() {
        return colorTextColumn;
    }

    /**
     * @return the linkColumn
     */
    public TableColumn getLinkColumn() {
        return linkColumn;
    }

    /**
     * @return the projectsLabel
     */
    public Label getProjectsLabel() {
        return projectsLabel;
    }

    /**
     * @return the bgColorPicker
     */
    public ColorPicker getBgColorPicker() {
        return bgColorPicker;
    }

    /**
     * @return the textColorPicker
     */
    public ColorPicker getTextColorPicker() {
        return textColorPicker;
    }

    /**
     * @return the teamsLabel
     */
    public Label getTeamsLabel() {
        return teamsLabel;
    }

    /**
     * @return the add_editTeamLabel
     */
    public Label getAdd_editTeamLabel() {
        return add_editTeamLabel;
    }

    /**
     * @return the nameLabel
     */
    public Label getNameLabel() {
        return nameLabel;
    }

    /**
     * @return the colorLabel
     */
    public Label getColorLabel() {
        return colorLabel;
    }

    /**
     * @return the textColorLabel
     */
    public Label getTextColorLabel() {
        return textColorLabel;
    }

    /**
     * @return the linkLabel
     */
    public Label getLinkLabel() {
        return linkLabel;
    }

    /**
     * @return the studentsLabel
     */
    public Label getStudentsLabel() {
        return studentsLabel;
    }

    /**
     * @return the add_editLabel
     */
    public Label getAdd_editLabel() {
        return add_editLabel;
    }

    /**
     * @return the firstNameLabel
     */
    public Label getFirstNameLabel() {
        return firstNameLabel;
    }

    /**
     * @return the lastNameLabel
     */
    public Label getLastNameLabel() {
        return lastNameLabel;
    }

    /**
     * @return the teamLabel
     */
    public Label getTeamLabel() {
        return teamLabel;
    }

    /**
     * @return the roleLabel
     */
    public Label getRoleLabel() {
        return roleLabel;
    }

    /**
     * @return the nameTextField
     */
    public TextField getNameTextField() {
        return nameTextField;
    }

    /**
     * @return the linkTextField
     */
    public TextField getLinkTextField() {
        return linkTextField;
    }

    /**
     * @return the firstNameTextField
     */
    public TextField getFirstNameTextField() {
        return firstNameTextField;
    }

    /**
     * @return the lastNameTextField
     */
    public TextField getLastNameTextField() {
        return lastNameTextField;
    }

    /**
     * @return the teamComboBox
     */
    public ComboBox getTeamComboBox() {
        return teamComboBox;
    }

    /**
     * @return the roleTextField
     */
    public TextField getRoleTextField() {
        return roleTextField;
    }

    /**
     * @return the removeButton
     */
    public Button getRemoveButton() {
        return removeButton;
    }

    /**
     * @return the addUpdateButton
     */
    public Button getAddUpdateButton() {
        return addUpdateButton;
    }

    /**
     * @return the clearButton
     */
    public Button getClearButton() {
        return clearButton;
    }

    /**
     * @return the removeTeamButton
     */
    public Button getRemoveTeamButton() {
        return removeTeamButton;
    }

    /**
     * @return the addUpdateTeamButton
     */
    public Button getAddUpdateTeamButton() {
        return addUpdateTeamButton;
    }

    /**
     * @return the clearTeamButton
     */
    public Button getClearTeamButton() {
        return clearTeamButton;
    }

    /**
     * @return the addEditGridPane
     */
    public GridPane getAddEditGridPane() {
        return addEditGridPane;
    }

    /**
     * @return the partVbox1
     */
    public VBox getPartVbox1() {
        return partVbox1;
    }

    /**
     * @return the partVbox2
     */
    public VBox getPartVbox2() {
        return partVbox2;
    }
    /**
     * @return the partVbox3
     */
    public VBox getPartVbox3() {
        return partVbox3;
    }

    /**
     * @return the allVbox
     */
    public VBox getAllVbox() {
        return allVbox;
    }
    
    /**
     * @return the controller
     */
    public projectController getController() {
        return controller;
    }
}
