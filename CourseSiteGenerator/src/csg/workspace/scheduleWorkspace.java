/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.controller.scheduleController;
import csg.data.Schedule;
import djf.components.AppDataComponent;
import djf.components.AppScheduleWorkspaceComponent;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import tam.ScheduleProp;
import tam.TAManagerApp;
import tam.data.TAData;

/**
 *
 * @author PLT
 */
public class scheduleWorkspace extends AppScheduleWorkspaceComponent{
    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    TAManagerApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    scheduleController controller;
    
    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    
    // Check if the first transaction is done or not
    boolean isFirstTransactionDone=false; 
    
    protected TableView scheduleTable;
    protected TableColumn typeColumn;
    protected TableColumn dateColumn;
    protected TableColumn titleColumn;
    protected TableColumn topicColumn;
    
    
    protected Label scheduleLabel;
    protected Label calenderLabel;
    protected Label startDayLabel;
    protected DatePicker startDatePicker;
    protected Label endDayLabel;
    protected DatePicker endDatePicker;
    protected DatePicker addDatePicker;
    
    
    protected Label scheduleItemsLabel;
    protected Label add_editLabel;
    protected Label typeLabel;
    protected Label dateLabel;
    protected Label timeLabel;
    protected Label titleLabel;
    protected Label topicLabel;
    protected Label linkLabel;
    protected Label criteriaLabel;
    
    protected TextField topicTextField;
    protected TextField dayTimeTextField;
    protected TextField titleTextField;
    protected TextField linkTextField;
    protected TextField criteriaTextField;
    protected ComboBox typeComboBox;
    
    protected Button removeButton;
    protected Button addUpdateButton;
    protected Button clearButton;
    
    protected GridPane addEditGridPane;
    
    
    
    protected VBox partVbox1 = new VBox();
    protected VBox partVbox2 = new VBox();
    protected VBox allVbox = new VBox();

    public scheduleWorkspace(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
    scheduleLabel = new Label(props.getProperty(ScheduleProp.SCHEDULE_LABEL_TEXT.toString()));
    calenderLabel = new Label(props.getProperty(ScheduleProp.CALENDAR_BOUNDARY_LABEL_TEXT.toString()));
    scheduleItemsLabel = new Label(props.getProperty(ScheduleProp.SCHEDULE_ITEM_LABEL_TEXT.toString()));
    
    startDayLabel = new Label(props.getProperty(ScheduleProp.START_TIME_LABEL_TEXT.toString()));
    startDatePicker = new DatePicker();
    endDayLabel = new Label(props.getProperty(ScheduleProp.END_TIME_LABEL_TEXT.toString()));
    endDatePicker = new DatePicker();
    addDatePicker = new DatePicker();

    scheduleTable = new TableView();
    typeColumn = new TableColumn(props.getProperty(ScheduleProp.TYPE_HEADER_TEXT.toString()));
    dateColumn = new TableColumn(props.getProperty(ScheduleProp.DATE_HEADER_TEXT.toString()));
    titleColumn = new TableColumn(props.getProperty(ScheduleProp.TITLE_HEADER_TEXT.toString()));
    topicColumn = new TableColumn(props.getProperty(ScheduleProp.TOPIC_HEADER_TEXT.toString()));
    
    add_editLabel = new Label(props.getProperty(ScheduleProp.ADD_EDIT_LABEL_TEXT.toString()));
    typeLabel = new Label(props.getProperty(ScheduleProp.TYPE_LABEL_TEXT.toString()));
    dateLabel = new Label(props.getProperty(ScheduleProp.DATE_LABEL_TEXT.toString()));
    timeLabel = new Label(props.getProperty(ScheduleProp.TIME_LABEL_TEXT.toString()));
    titleLabel = new Label(props.getProperty(ScheduleProp.TITLE_LABEL_TEXT.toString()));
    topicLabel = new Label(props.getProperty(ScheduleProp.TOPIC_LABEL_TEXT.toString()));
    linkLabel = new Label(props.getProperty(ScheduleProp.LINK_LABEL_TEXT.toString()));
    criteriaLabel = new Label(props.getProperty(ScheduleProp.CRITERIA_LABEL_TEXT.toString()));
    
    topicTextField = new TextField();
    criteriaTextField = new TextField();
    dayTimeTextField = new TextField();
    titleTextField = new TextField();
    linkTextField = new TextField();
    typeComboBox = new ComboBox();
    
    //remove = new Button(props.getProperty(ScheduleProp.ADD_UPDATE_BUTTON_TEXT.toString()));
    removeButton = new Button("-");
    addUpdateButton = new Button(props.getProperty(ScheduleProp.ADD_UPDATE_BUTTON_TEXT.toString()));
    clearButton = new Button(props.getProperty(ScheduleProp.CLEAR_TEXT.toString()));
    
    
    partVbox1.getChildren().add(calenderLabel);
    GridPane calenderGrid = new GridPane();
    int i =0;
    calenderGrid.add(startDayLabel,i++,0);
    calenderGrid.add(startDatePicker,i++,0);
    calenderGrid.add(endDayLabel,i++,0);
    calenderGrid.add(endDatePicker,i++,0);
    partVbox1.getChildren().add(calenderGrid);
    
    
    HBox header = new HBox();
    header.getChildren().add(scheduleItemsLabel);
    header.getChildren().add(removeButton);
    partVbox2.getChildren().add(header);
    
      typeColumn.prefWidthProperty().bind(scheduleTable.widthProperty().multiply(0.20)); 
        dateColumn.prefWidthProperty().bind(scheduleTable.widthProperty().multiply(0.25)); 
        titleColumn.prefWidthProperty().bind(scheduleTable.widthProperty().multiply(0.25)); 
        topicColumn.prefWidthProperty().bind(scheduleTable.widthProperty().multiply(0.30)); 
        
        
        scheduleTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TAData data = (TAData) app.getDataComponent();
        
        ObservableList<Schedule> tableData = data.getSchedules();
        
        scheduleTable.setItems(tableData);
        scheduleTable.setEditable(true);
        
        
        typeColumn.setCellValueFactory(
                new PropertyValueFactory<Schedule, String>("type")
        ); 
        
        dateColumn.setCellValueFactory(
                new PropertyValueFactory<Schedule, String>("date")
        ); 
        
        
        titleColumn.setCellValueFactory(
                new PropertyValueFactory<Schedule, String>("title")
        ); 
        
        topicColumn.setCellValueFactory(
                new PropertyValueFactory<Schedule, String>("topic")
        ); 
        
        
        scheduleTable.getColumns().add(typeColumn);
        scheduleTable.getColumns().add(dateColumn);
        scheduleTable.getColumns().add(titleColumn);
        scheduleTable.getColumns().add(topicColumn);
        
        scheduleTable.prefWidthProperty().bind(partVbox2.widthProperty().multiply(1)); 
        
    partVbox2.getChildren().add(scheduleTable);
   
    
    partVbox2.getChildren().add(add_editLabel);
    GridPane inputSection  = new GridPane();
    i =0;
    //inputSection.add(header, 0, 0);
    
    //inputSection.add(add_editLabel,0,i);
    inputSection.add(typeLabel,0,i);
    inputSection.add(typeComboBox,1,i++);
    inputSection.add(dateLabel,0,i);


    inputSection.add(addDatePicker,1,i++);

    inputSection.add(timeLabel,0,i);
    inputSection.add(dayTimeTextField,1,i++);
    inputSection.add(titleLabel,0,i);
    inputSection.add(titleTextField,1,i++);
    inputSection.add(topicLabel,0,i);
    inputSection.add(topicTextField,1,i++);
    inputSection.add(linkLabel,0,i);
    inputSection.add(linkTextField,1,i++);
    inputSection.add(criteriaLabel,0,i);

    //add here
    //add here
    inputSection.add(criteriaTextField,1,i++);
    inputSection.add(addUpdateButton,0,i);
    inputSection.add(clearButton,1,i++);
    
    
    partVbox2.getChildren().add(inputSection);
    
    allVbox.getChildren().add(scheduleLabel);
    allVbox.getChildren().add(partVbox1);
    allVbox.getChildren().add(partVbox2);
    
    allVbox.prefWidthProperty().bind(app.getGUI().getAppPane().widthProperty().multiply(1));
    
    
    typeComboBox.setItems(data.getTypeOptions());
    
        workspace =  new Pane(getAllVbox()); 
      
        controller = new scheduleController(app);
        //ADDING THE FIRST TRANSACTION WHEN THE APP LOADS
        
        if (data.isLoaded)
        {
            controller.handleAddTransaction();
        }
              
        addUpdateButton.setOnAction(e -> {
            updateInitialTransaction();
            controller.handleAddUpdateSchedule();
        });
        
        clearButton.setOnAction(e -> {
            updateInitialTransaction();
           controller.clearUpdate(); 
        });
       
       scheduleTable.setOnMouseClicked(e ->{
            updateInitialTransaction();
            controller.handleShowUpdateSchedule();
       });
       
       // FOR DELETING TA
       scheduleTable.setOnKeyPressed(e -> {
           if (e.getCode() == KeyCode.DELETE )
           {
                updateInitialTransaction();
                controller.handleDeleteSchedule();
           }  
       });
       
       removeButton.setOnAction(e -> {
            updateInitialTransaction();
            controller.handleDeleteSchedule();
        });
       
       
       
       addDatePicker.setValue(LocalDate.now());
       startDatePicker.setValue(LocalDate.now());
       while(startDatePicker.getValue().getDayOfWeek().compareTo(DayOfWeek.MONDAY)!=0)
       {
           startDatePicker.setValue(startDatePicker.getValue().plusDays(-1));
       }
       
       endDatePicker.setValue(LocalDate.now());
       while(endDatePicker.getValue().getDayOfWeek().compareTo(DayOfWeek.FRIDAY)!=0)
       {
           endDatePicker.setValue(endDatePicker.getValue().plusDays(1));
       }
       data.initDates(startDatePicker.getValue().getYear(), startDatePicker.getValue().getMonthValue(),startDatePicker.getValue().getDayOfMonth(),endDatePicker.getValue().getYear(), endDatePicker.getValue().getMonthValue(),endDatePicker.getValue().getDayOfMonth());
        
       final Callback<DatePicker, DateCell> dayCellFactoryStart = 
            new Callback<DatePicker, DateCell>() {
                @Override
                public DateCell call(final DatePicker datePicker) {
                    return new DateCell() {
                        @Override
                        public void updateItem(LocalDate item, boolean empty) {
                            super.updateItem(item, empty);
                           
                            // DISABLING DAYS THAT ARE BEFORE THE END DATE
                            if (item.isAfter(
                                    endDatePicker.getValue())
                                ) {
                                    setDisable(true);
                                    setStyle("-fx-background-color: #ffc0cb;");
                            }
                            
                            // SHOWING THE WEEKS TO THE USER
                            long p = ChronoUnit.WEEKS.between(item, endDatePicker.getValue());
                            setTooltip(new Tooltip("The semester is about " + (p + 1) + " weeks"));
                            
                            // CHECKING IF THE DAY IS MONDAY OR NOT
                            if(item.getDayOfWeek().compareTo(DayOfWeek.MONDAY)!=0)
                            {
                                    setDisable(true);
                                    setStyle("-fx-background-color: #ffc0cb;");
                            }
                    }
                };
            }
        };
        startDatePicker.setDayCellFactory(dayCellFactoryStart);
       
        final Callback<DatePicker, DateCell> dayCellFactoryEnd = 
            new Callback<DatePicker, DateCell>() {
                @Override
                public DateCell call(final DatePicker datePicker) {
                    return new DateCell() {
                        @Override
                        public void updateItem(LocalDate item, boolean empty) {
                            super.updateItem(item, empty);
                           
                            // DISABLING DAYS THAT ARE BEFORE THE START DATE
                            if (item.isBefore(
                                    startDatePicker.getValue())
                                ) {
                                    setDisable(true);
                                    setStyle("-fx-background-color: #ffc0cb;");
                            }
                            
                            // SHOWING THE WEEKS TO THE USER
                            long p = ChronoUnit.WEEKS.between(startDatePicker.getValue(), item);
                            setTooltip(new Tooltip("The semester is about " + (p + 1) + " weeks"));
                            
                            // CHECKING IF THE DAY IS FRIDAY OR NOT
                            if(item.getDayOfWeek().compareTo(DayOfWeek.FRIDAY)!=0)
                            {
                                    setDisable(true);
                                    setStyle("-fx-background-color: #ffc0cb;");
                            }
                    }
                };
            }
        };
        endDatePicker.setDayCellFactory(dayCellFactoryEnd);

       
        startDatePicker.setOnAction(e ->{
             updateInitialTransaction();
             data.initDates(startDatePicker.getValue().getYear(), startDatePicker.getValue().getMonthValue(),startDatePicker.getValue().getDayOfMonth(),endDatePicker.getValue().getYear(), endDatePicker.getValue().getMonthValue(),endDatePicker.getValue().getDayOfMonth());
        
             //controller.handleStartDateChange();
        });

        endDatePicker.setOnAction(e ->{
             updateInitialTransaction();
             data.initDates(startDatePicker.getValue().getYear(), startDatePicker.getValue().getMonthValue(),startDatePicker.getValue().getDayOfMonth(),endDatePicker.getValue().getYear(), endDatePicker.getValue().getMonthValue(),endDatePicker.getValue().getDayOfMonth());
             //controller.handleEndDateChange();
        });
    }
    
    
    private void updateInitialTransaction(){
        if(!isFirstTransactionDone){ controller.handleAddTransaction(); isFirstTransactionDone=true;}
    }
    
    @Override
    public void resetWorkspace() {    
        controller.clearSelection();
       addDatePicker.setValue(LocalDate.now());
       startDatePicker.setValue(LocalDate.now());
       while(startDatePicker.getValue().getDayOfWeek().compareTo(DayOfWeek.MONDAY)!=0)
       {
           startDatePicker.setValue(startDatePicker.getValue().plusDays(-1));
       }
       
       endDatePicker.setValue(LocalDate.now());
       while(endDatePicker.getValue().getDayOfWeek().compareTo(DayOfWeek.FRIDAY)!=0)
       {
           endDatePicker.setValue(endDatePicker.getValue().plusDays(1));
       }
       
        //data.initDates(startDatePicker.getValue().getYear(), startDatePicker.getValue().getMonthValue(),startDatePicker.getValue().getDayOfMonth(),endDatePicker.getValue().getYear(), endDatePicker.getValue().getMonthValue(),endDatePicker.getValue().getDayOfMonth());
        
        //data.setStartYear()
        //data.getStartMonth(), data.getStartDay()));
        //endDatePicker.setValue(LocalDate.of(data.getEndYear(),data.getEndMonth(), data.getEndDay()));
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        TAData data = (TAData) dataComponent;
        startDatePicker.setValue(LocalDate.of(data.getStartYear(),data.getStartMonth(), data.getStartDay()));
        endDatePicker.setValue(LocalDate.of(data.getEndYear(),data.getEndMonth(), data.getEndDay()));
    }

    /**
     * @return the scheduleTable
     */
    public TableView getScheduleTable() {
        return scheduleTable;
    }

    /**
     * @return the typeColumn
     */
    public TableColumn getTypeColumn() {
        return typeColumn;
    }

    /**
     * @return the dateColumn
     */
    public TableColumn getDateColumn() {
        return dateColumn;
    }

    /**
     * @return the titleColumn
     */
    public TableColumn getTitleColumn() {
        return titleColumn;
    }

    /**
     * @return the topicColumn
     */
    public TableColumn getTopicColumn() {
        return topicColumn;
    }

    /**
     * @return the scheduleLabel
     */
    public Label getScheduleLabel() {
        return scheduleLabel;
    }

    /**
     * @return the calenderLabel
     */
    public Label getCalenderLabel() {
        return calenderLabel;
    }

    /**
     * @return the startDayLabel
     */
    public Label getStartDayLabel() {
        return startDayLabel;
    }

    /**
     * @return the startDatePicker
     */
    public DatePicker getStartDatePicker() {
        return startDatePicker;
    }

    /**
     * @return the endDayLabel
     */
    public Label getEndDayLabel() {
        return endDayLabel;
    }

    /**
     * @return the endDatePicker
     */
    public DatePicker getEndDatePicker() {
        return endDatePicker;
    }

    /**
     * @return the scheduleItemsLabel
     */
    public Label getScheduleItemsLabel() {
        return scheduleItemsLabel;
    }

    /**
     * @return the add_editLabel
     */
    public Label getAdd_editLabel() {
        return add_editLabel;
    }

    /**
     * @return the typeLabel
     */
    public Label getTypeLabel() {
        return typeLabel;
    }

    /**
     * @return the dateLabel
     */
    public Label getDateLabel() {
        return dateLabel;
    }

    /**
     * @return the timeLabel
     */
    public Label getTimeLabel() {
        return timeLabel;
    }

    /**
     * @return the titleLabel
     */
    public Label getTitleLabel() {
        return titleLabel;
    }

    /**
     * @return the topicLabel
     */
    public Label getTopicLabel() {
        return topicLabel;
    }

    /**
     * @return the linkLabel
     */
    public Label getLinkLabel() {
        return linkLabel;
    }

    /**
     * @return the criteriaLabel
     */
    public Label getCriteriaLabel() {
        return criteriaLabel;
    }

    /**
     * @return the topicTextField
     */
    public TextField getTopicTextField() {
        return topicTextField;
    }

    /**
     * @return the dayTimeTextField
     */
    public TextField getDayTimeTextField() {
        return dayTimeTextField;
    }

    /**
     * @return the titleTextField
     */
    public TextField getTitleTextField() {
        return titleTextField;
    }

    /**
     * @return the linkTextField
     */
    public TextField getLinkTextField() {
        return linkTextField;
    }

    /**
     * @return the criteriaTextField
     */
    public TextField getCriteriaTextField() {
        return criteriaTextField;
    }

    /**
     * @return the typeComboBox
     */
    public ComboBox getTypeComboBox() {
        return typeComboBox;
    }

    /**
     * @return the addDatePicker
     */
    public DatePicker getaddDatePicker() {
        return addDatePicker;
    }

    /**
     * @return the removeButton
     */
    public Button getRemoveButton() {
        return removeButton;
    }

    /**
     * @return the addUpdateButton
     */
    public Button getAddUpdateButton() {
        return addUpdateButton;
    }

    /**
     * @return the clearButton
     */
    public Button getClearButton() {
        return clearButton;
    }

    /**
     * @return the addEditGridPane
     */
    public GridPane getAddEditGridPane() {
        return addEditGridPane;
    }

    /**
     * @return the partVbox1
     */
    public VBox getPartVbox1() {
        return partVbox1;
    }

    /**
     * @return the partVbox2
     */
    public VBox getPartVbox2() {
        return partVbox2;
    }

    /**
     * @return the allVbox
     */
    public VBox getAllVbox() {
        return allVbox;
    }
    
    /**
     * @return the controller
     */
    public scheduleController getController() {
        return controller;
    }

    private Object startDatePicker() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
