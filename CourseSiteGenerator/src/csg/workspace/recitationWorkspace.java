/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.controller.recitationController;
import csg.data.Recitation;
import djf.components.AppDataComponent;
import djf.components.AppRecitationWorkspaceComponent;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import tam.RecitationProp;
import tam.TAManagerApp;
import tam.TAManagerProp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.workspace.TAController;

/**
 *
 * @author PLT
 */
public class recitationWorkspace extends AppRecitationWorkspaceComponent{
    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    TAManagerApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    recitationController controller;
    
    // Check if the first transaction is done or not
    boolean isFirstTransactionDone=false;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    
    // Check if the first transaction is done or not
    //boolean isFirstTransactionDone=false;
    protected Label recitationsLabel;
    
    /*
    Label sectionHeaderLabel;
    Label instructorHeaderLabel;
    Label dayTimeHeaderLabel;
    Label locationHeaderLabel;
    Label taLabel;
    */
    
    protected TableView recitationTable;
    protected TableColumn sectionColumn;
    protected TableColumn instructorColumn;
    protected TableColumn dayTimeColumn;
    protected TableColumn locationColumn;
    protected TableColumn TAColumn1; 
    protected TableColumn TAColumn2; 
    
    protected Label add_editLabel;
    protected Label sectionLabel;
    protected Label instructorLabel;
    protected Label dayTimeLabel;
    protected Label locationLabel;
    protected Label supervisingTALabel1;
    protected Label supervisingTALabel2;
    
    protected TextField sectionTextField;
    protected TextField instructorTextField;
    protected TextField dayTimeTextField;
    protected TextField locationTextField;
    protected ComboBox supervisingTAComboBox1;
    protected ComboBox supervisingTAComboBox2;
    
    protected Button removeButton;
    protected Button addUpdateButton;
    protected Button clearButton;
    
    protected GridPane addEditGridPane;
    
    protected VBox partVbox1 = new VBox();
    protected VBox partVbox2 = new VBox();
    protected VBox allVbox = new VBox();

    public recitationWorkspace(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
    recitationsLabel = new Label(props.getProperty(RecitationProp.RECITATIONS_LABEL_TEXT.toString()));

    recitationTable = new TableView();
    sectionColumn = new TableColumn(props.getProperty(RecitationProp.SECTION_HEADER_TEXT.toString()));
    instructorColumn = new TableColumn(props.getProperty(RecitationProp.INSTRUCTOR_HEADER_TEXT.toString()));
    dayTimeColumn = new TableColumn(props.getProperty(RecitationProp.DAY_TIME_HEADER_TEXT.toString()));
    locationColumn = new TableColumn(props.getProperty(RecitationProp.LOCATION_HEADER_TEXT.toString()));
    TAColumn1 = new TableColumn(props.getProperty(RecitationProp.TA_HEADER_TEXT.toString()));
    TAColumn2 = new TableColumn(props.getProperty(RecitationProp.TA_HEADER_TEXT.toString()));
    
    add_editLabel = new Label(props.getProperty(RecitationProp.ADD_EDIT_LABEL_TEXT.toString()));
    sectionLabel = new Label(props.getProperty(RecitationProp.SECTION_LABEL_TEXT.toString()));
    instructorLabel = new Label(props.getProperty(RecitationProp.INSTRUCTOR_LABEL_TEXT.toString()));
    dayTimeLabel = new Label(props.getProperty(RecitationProp.DAY_TIME_LABEL_TEXT.toString()));
    locationLabel = new Label(props.getProperty(RecitationProp.LOCATION_LABEL_TEXT.toString()));
    supervisingTALabel1 = new Label(props.getProperty(RecitationProp.SUPERVISING_TA__LABEL_TEXT.toString()));
    supervisingTALabel2 = new Label(props.getProperty(RecitationProp.SUPERVISING_TA__LABEL_TEXT.toString()));
    
    sectionTextField = new TextField();
    instructorTextField = new TextField();
    dayTimeTextField = new TextField();
    locationTextField = new TextField();
    supervisingTAComboBox1 = new ComboBox();
    supervisingTAComboBox2 = new ComboBox();
    
    //remove = new Button(props.getProperty(RecitationProp.ADD_UPDATE_BUTTON_TEXT.toString()));
    removeButton = new Button("-");
    addUpdateButton = new Button(props.getProperty(RecitationProp.ADD_BUTTON_TEXT.toString()));
    clearButton = new Button(props.getProperty(RecitationProp.CLEAR_TEXT.toString()));
    
 
    
    HBox header = new HBox();
    header.getChildren().add(recitationsLabel);
    header.getChildren().add(removeButton);
    partVbox1.getChildren().add(header);
    
      sectionColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(0.1)); 
        instructorColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(0.25)); 
        dayTimeColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(0.2)); 
        locationColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(0.15)); 
        TAColumn1.prefWidthProperty().bind(recitationTable.widthProperty().multiply(0.15)); 
        TAColumn2.prefWidthProperty().bind(recitationTable.widthProperty().multiply(0.15)); 
        
        
        recitationTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TAData data = (TAData) app.getDataComponent();
        
        ObservableList<Recitation> tableData = data.getRecitations();
        recitationTable.setItems(tableData);
        recitationTable.setEditable(true);
        
        
        sectionColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation, String>("section")
        ); 
        
        instructorColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation, String>("instructor")
        ); 
               
        dayTimeColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation, String>("dayTime")
        );
        
        locationColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation, String>("location")
        ); 
        
        TAColumn1.setCellValueFactory(
                new PropertyValueFactory<Recitation, String>("supervisingTA1")
        ); 
        
        TAColumn2.setCellValueFactory(
                new PropertyValueFactory<Recitation, String>("supervisingTA2")
        ); 
        
        recitationTable.getColumns().add(sectionColumn);
        recitationTable.getColumns().add(instructorColumn);
        recitationTable.getColumns().add(dayTimeColumn);
        recitationTable.getColumns().add(locationColumn);
        recitationTable.getColumns().add(TAColumn1);
        recitationTable.getColumns().add(TAColumn2);
        
        
        
        recitationTable.prefWidthProperty().bind(partVbox1.widthProperty().multiply(1)); 
        
    partVbox1.getChildren().add(recitationTable);
   
    
    partVbox2.getChildren().add(add_editLabel);
    GridPane inputSection  = new GridPane();
    int i =0;
    //inputSection.add(header, 0, 0);
    
    //inputSection.add(add_editLabel,0,i);
    inputSection.add(sectionLabel,0,i);
    inputSection.add(sectionTextField,1,i++);
    inputSection.add(instructorLabel,0,i);
    inputSection.add(instructorTextField,1,i++);
    inputSection.add(dayTimeLabel,0,i);
    inputSection.add(dayTimeTextField,1,i++);
    inputSection.add(locationLabel,0,i);
    inputSection.add(locationTextField,1,i++);
    inputSection.add(supervisingTALabel1,0,i);
    inputSection.add(supervisingTAComboBox1,1,i++);
    inputSection.add(supervisingTALabel2,0,i);
    inputSection.add(supervisingTAComboBox2,1,i++);
    inputSection.add(addUpdateButton,0,i);
    inputSection.add(clearButton,1,i++);
    
    
    partVbox2.getChildren().add(inputSection);
    
    VBox allVbox = new VBox();
    allVbox.getChildren().add(partVbox1);
    allVbox.getChildren().add(partVbox2);
    
    supervisingTAComboBox1.setItems(data.getTeachingAssistants());
    supervisingTAComboBox2.setItems(data.getTeachingAssistants());
    
    allVbox.prefWidthProperty().bind(app.getGUI().getAppPane().widthProperty().multiply(1));
    
        workspace =  new Pane(allVbox); 
        
    //everything
    
    // HERE WE DO THE EVENT HANDLING
    
    // NOW LET'S SETUP THE EVENT HANDLING
        controller = new recitationController(app);
        
            //addUpdateButton.setDisable(true);
            
            //addButton.setDisable(false);
            
        
        //ADDING THE FIRST TRANSACTION WHEN THE APP LOADS
        
        if (data.isLoaded)
        {
            controller.handleAddTransaction();
        }
              
        addUpdateButton.setOnAction(e -> {
            updateInitialTransaction();
            controller.handleAddUpdateRecitation();
            recitationTable.refresh();
        });
        
        clearButton.setOnAction(e -> {
            updateInitialTransaction();
           controller.clearUpdate(); 
        });
       
       recitationTable.setOnMouseClicked(e ->{
            updateInitialTransaction();
            controller.handleShowUpdateRecitation();
            recitationTable.refresh();
       });
       
       // FOR DELETING TA
       recitationTable.setOnKeyPressed(e -> {
           if (e.getCode() == KeyCode.DELETE )
           {
                updateInitialTransaction();
                controller.handleDeleteRecitation();
                recitationTable.refresh();
           }  
       });
       
       removeButton.setOnAction(e -> {
            updateInitialTransaction();
            controller.handleDeleteRecitation();
            recitationTable.refresh();
        });
    
    }
    
    private void updateInitialTransaction(){
        if(!isFirstTransactionDone){ controller.handleAddTransaction(); isFirstTransactionDone=true;}
    }
    
    @Override
    public void resetWorkspace() {    
        controller.clearSelection();
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the recitationsLabel
     */
    public Label getRecitationsLabel() {
        return recitationsLabel;
    }

    /**
     * @return the recitationTable
     */
    public TableView getRecitationTable() {
        return recitationTable;
    }

    /**
     * @return the sectionColumn
     */
    public TableColumn getSectionColumn() {
        return sectionColumn;
    }

    /**
     * @return the instructorColumn
     */
    public TableColumn getInstructorColumn() {
        return instructorColumn;
    }

    /**
     * @return the dayTimeColumn
     */
    public TableColumn getDayTimeColumn() {
        return dayTimeColumn;
    }

    /**
     * @return the locationColumn
     */
    public TableColumn getLocationColumn() {
        return locationColumn;
    }

    /**
     * @return the TAColumn1
     */
    public TableColumn getTAColumn1() {
        return TAColumn1;
    }

    /**
     * @return the TAColumn2
     */
    public TableColumn getTAColumn2() {
        return TAColumn2;
    }

    /**
     * @return the add_editLabel
     */
    public Label getAdd_editLabel() {
        return add_editLabel;
    }

    /**
     * @return the sectionLabel
     */
    public Label getSectionLabel() {
        return sectionLabel;
    }

    /**
     * @return the instructorLabel
     */
    public Label getInstructorLabel() {
        return instructorLabel;
    }

    /**
     * @return the dayTimeLabel
     */
    public Label getDayTimeLabel() {
        return dayTimeLabel;
    }

    /**
     * @return the locationLabel
     */
    public Label getLocationLabel() {
        return locationLabel;
    }

    /**
     * @return the supervisingTALabel1
     */
    public Label getSupervisingTALabel1() {
        return supervisingTALabel1;
    }

    /**
     * @return the supervisingTALabel2
     */
    public Label getSupervisingTALabel2() {
        return supervisingTALabel2;
    }

    /**
     * @return the sectionTextField
     */
    public TextField getSectionTextField() {
        return sectionTextField;
    }

    /**
     * @return the instructorTextField
     */
    public TextField getInstructorTextField() {
        return instructorTextField;
    }

    /**
     * @return the dayTimeTextField
     */
    public TextField getDayTimeTextField() {
        return dayTimeTextField;
    }

    /**
     * @return the locationTextField
     */
    public TextField getLocationTextField() {
        return locationTextField;
    }

    /**
     * @return the supervisingTAComboBox1
     */
    public ComboBox getSupervisingTAComboBox1() {
        return supervisingTAComboBox1;
    }

    /**
     * @return the supervisingTAComboBox2
     */
    public ComboBox getSupervisingTAComboBox2() {
        return supervisingTAComboBox2;
    }

    /**
     * @return the removeButton
     */
    public Button getRemoveButton() {
        return removeButton;
    }

    /**
     * @return the addUpdateButton
     */
    public Button getAddUpdateButton() {
        return addUpdateButton;
    }

    /**
     * @return the clearButton
     */
    public Button getClearButton() {
        return clearButton;
    }

    /**
     * @return the addEditGridPane
     */
    public GridPane getAddEditGridPane() {
        return addEditGridPane;
    }

    /**
     * @return the partVbox1
     */
    public VBox getPartVbox1() {
        return partVbox1;
    }

    /**
     * @return the partVbox2
     */
    public VBox getPartVbox2() {
        return partVbox2;
    }

    /**
     * @return the allVbox
     */
    public VBox getAllVbox() {
        return allVbox;
    }
    
    /**
     * @return the controller
     */
    public recitationController getController() {
        return controller;
    }
}
