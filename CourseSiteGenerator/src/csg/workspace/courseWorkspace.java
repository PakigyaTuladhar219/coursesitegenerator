/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.controller.courseController;
import csg.data.Page;
import djf.components.AppCourseWorkspaceComponent;
import djf.components.AppDataComponent;
import static djf.settings.AppPropertyType.EXPORT_WORK_TITLE;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_BRANDS;
import static djf.settings.AppStartupConstants.PATH_TO_HTML;
import java.io.File;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import tam.CourseProp;
import tam.TAManagerApp;
import tam.data.TAData;

/**
 *
 * @author PLT
 */
public class courseWorkspace extends AppCourseWorkspaceComponent {
    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    TAManagerApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    courseController controller;
    
    

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    
    // Check if the first transaction is done or not
    //boolean isFirstTransactionDone=false;
    TAData data;
    
    // FOR THE PANE 
    protected Label courseInfoLabel;
    
    protected Label subjectLabel;
    protected Label numberLabel;
    protected Label semesterLabel;
    protected Label yearLabel;
    protected Label titleLabel;
    protected Label instructorNameLabel;
    protected Label instructorHomeLabel;
    protected Label exportDirLabel;
    protected Label pathLabel;
    
    protected ComboBox subjectComboBox;
    protected ComboBox numberComboBox;
    protected ComboBox semesterComboBox;
    protected ComboBox yearComboBox;
    protected ComboBox stylesheetComboBox;
    
    protected TextField titleTextField;
    protected TextField instructorNameTextField;
    protected TextField instructorHomeTextField;
    //TextField 
    
    protected Button changeDirButton;
    
    //button
    
    protected Label siteTemplateLabel;
    protected Label templateInfoLabel;
    protected Label templateChooseLabel;
    //button
    
    protected Label sitePagesLabel;
    //grid pane
    
    protected Label pageStyleLabel;
    protected Label bannerSchoolLabel;
    protected Label leftFooterLabel;
    protected Label rightFooterLabel;
    protected Label stylesheetLabel;
    protected Label stylesheetNoteLabel;
    
    protected Button selectTemplateDirButton;
    protected Button changeBannerButton;
    protected Button changeLeftFooterButton;
    protected Button changeRightFooterButton;
    //Button changeButton;
    
    protected GridPane courseDetailsGridPane;
    protected GridPane courseDetailsGridPane2;
   
    protected TableView pageTable;
    protected TableColumn useColumn;
    protected TableColumn navColumn;
    protected TableColumn fileNameColumn;
    protected TableColumn scriptColumn;
    
    protected SplitPane sPane;
             
    
    
    
    protected VBox partVbox1 = new VBox();
    protected VBox partVbox2 = new VBox();
    protected VBox partVbox3 = new VBox();
    protected VBox allVbox = new VBox();
    //officeHoursGridPane.add(cellPane, col, row);
    
    ImageView changeBannerImageView;
    ImageView leftFooterImageView;
    ImageView rightFooterImageView;
    
    String changeBannerName;
    String leftFooterName;
    String rightFooterName;
   
     int indexBannerChange = 0;
        int indexRightFooter = 0;
        int indexLeftFooter = 0;

    /**
     * The constructor initializes the user interface, except for
     * the full office hours grid, since it doesn't yet know what
     * the hours will be until a file is loaded or a new one is created.
     */
    public courseWorkspace(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        courseInfoLabel = new Label(props.getProperty(CourseProp.COURSE_INFO_LABEL_TEXT.toString()));
    
        subjectLabel = new Label(props.getProperty(CourseProp.SUBJECT_LABEL_TEXT.toString()));
        numberLabel = new Label(props.getProperty(CourseProp.NUMBER_LABEL_TEXT.toString()));
        semesterLabel = new Label(props.getProperty(CourseProp.SEMESTER_LABEL_TEXT.toString()));
        yearLabel = new Label(props.getProperty(CourseProp.YEAR_LABEL_TEXT.toString()));
        titleLabel = new Label(props.getProperty(CourseProp.TITLE_LABEL_TEXT.toString()));
        instructorNameLabel = new Label(props.getProperty(CourseProp.INSTRUCTOR_NAME_LABEL_TEXT.toString()));
        instructorHomeLabel = new Label(props.getProperty(CourseProp.INSTRUCTOR_HOME_LABEL_TEXT.toString()));
        exportDirLabel = new Label(props.getProperty(CourseProp.EXPORT_DIR_LABEL_TEXT.toString()));
        pathLabel = new Label(props.getProperty(CourseProp.EXPORT_DIR_LABEL_TEXT.toString()));

        subjectComboBox = new ComboBox();
        numberComboBox = new ComboBox();
        semesterComboBox = new ComboBox();
        yearComboBox = new ComboBox();
        stylesheetComboBox = new ComboBox();

        titleTextField = new TextField();
        instructorNameTextField = new TextField();
        instructorHomeTextField = new TextField();
            //TextField 

        changeDirButton = new Button(props.getProperty(CourseProp.CHANGE_BUTTON_TEXT.toString()));
        selectTemplateDirButton  = new Button(props.getProperty(CourseProp.SELECT_DIR_BUTTON_TEXT.toString()));
            //button

        siteTemplateLabel = new Label(props.getProperty(CourseProp.SITE_TEMPLATE_LABEL_TEXT.toString()));
        templateInfoLabel = new Label(props.getProperty(CourseProp.INFO_LABEL_TEXT.toString()));
        templateChooseLabel = new Label(CourseProp.EXPORT_DIR_LABEL_TEXT.toString());
            //button

        sitePagesLabel = new Label(props.getProperty(CourseProp.SITE_PAGES_LABEL_TEXT.toString()));
            //grid pane

        pageStyleLabel = new Label(props.getProperty(CourseProp.PAGE_STYLE_LABEL_TEXT.toString()));
        bannerSchoolLabel = new Label(props.getProperty(CourseProp.BANNER_SCHOOL_LABEL_TEXT.toString()));
        leftFooterLabel = new Label(props.getProperty(CourseProp.LEFT_FOOTER_LABEL_TEXT.toString()));
        rightFooterLabel = new Label(props.getProperty(CourseProp.RIGHT_FOOTER_LABEL_TEXT.toString()));
        stylesheetLabel = new Label(props.getProperty(CourseProp.STYLESHEET_LABEL_TEXT.toString()));
        stylesheetNoteLabel = new Label(props.getProperty(CourseProp.NOTE_STYLESHEET_LABEL_TEXT.toString()));

        changeBannerButton = new Button(props.getProperty(CourseProp.CHANGE_BUTTON_TEXT.toString()));
        changeLeftFooterButton = new Button(props.getProperty(CourseProp.CHANGE_BUTTON_TEXT.toString()));
        changeRightFooterButton = new Button(props.getProperty(CourseProp.CHANGE_BUTTON_TEXT.toString()));

        
        courseDetailsGridPane = new GridPane();
        
        
        // NOW PUT THE CELL IN THE WORKSPACE GRID
        
        partVbox1.getChildren().add(courseInfoLabel);
        int i = 0;
        
        courseDetailsGridPane.add(courseInfoLabel, 0, i++);
        courseDetailsGridPane.add(subjectLabel, 0, i);
        courseDetailsGridPane.add(subjectComboBox, 1, i);
        courseDetailsGridPane.add(numberLabel, 2, i);
        courseDetailsGridPane.add(numberComboBox, 3, i++);
        courseDetailsGridPane.add(semesterLabel, 0, i);
        courseDetailsGridPane.add(semesterComboBox, 1, i);        
        courseDetailsGridPane.add(yearLabel, 2, i);
        courseDetailsGridPane.add(yearComboBox, 3, i++);
        courseDetailsGridPane.add(titleLabel, 0, i);
        courseDetailsGridPane.add(titleTextField, 1, i++);
        courseDetailsGridPane.add(instructorNameLabel, 0, i);
        courseDetailsGridPane.add(instructorNameTextField, 1, i++);
        courseDetailsGridPane.add(instructorHomeLabel, 0, i);
        courseDetailsGridPane.add(instructorHomeTextField, 1, i++);
        courseDetailsGridPane.add(exportDirLabel, 0, i);
        courseDetailsGridPane.add(pathLabel, 1, i);
        courseDetailsGridPane.add(changeDirButton, 2, i++);
        
        partVbox1.getChildren().add(courseDetailsGridPane);
        
        //courseDetailsVBox.getChildren().add(courseDetailsGridPane);
        VBox partVbox2 = new VBox();
        
        partVbox2.getChildren().add(siteTemplateLabel);
        partVbox2.getChildren().add(templateInfoLabel);
        partVbox2.getChildren().add(templateChooseLabel);
        partVbox2.getChildren().add(selectTemplateDirButton);
        partVbox2.getChildren().add(sitePagesLabel);
        
    
        pageTable = new TableView();
        useColumn = new TableColumn(props.getProperty(CourseProp.USE_HEADER_TEXT.toString()));
        navColumn = new TableColumn(props.getProperty(CourseProp.NAVBAR_HEADER_TEXT.toString()));
        fileNameColumn = new TableColumn(props.getProperty(CourseProp.FILE_NAME_HEADER_TEXT.toString()));
        scriptColumn = new TableColumn(props.getProperty(CourseProp.SCRIPT_HEADER_TEXT.toString()));
        
        useColumn.prefWidthProperty().bind(pageTable.widthProperty().multiply(0.1)); // changing the width of email Tab
        navColumn.prefWidthProperty().bind(pageTable.widthProperty().multiply(0.3)); // changing the width of email Tab
        fileNameColumn.prefWidthProperty().bind(pageTable.widthProperty().multiply(0.3)); // changing the width of email Tab
        scriptColumn.prefWidthProperty().bind(pageTable.widthProperty().multiply(0.3)); // changing the width of email Tab

        
        pageTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        data = (TAData) app.getDataComponent();
        
        ObservableList<Page> tableData = data.getPages();
        pageTable.setItems(tableData);
        pageTable.setEditable(true);
        
        navColumn.setCellValueFactory(
                new PropertyValueFactory<Page, String>("nav")
        ); 
                
        fileNameColumn.setCellValueFactory(
                new PropertyValueFactory<Page, String>("fileName")
        );

        scriptColumn.setCellValueFactory(
                new PropertyValueFactory<Page, String>("script")
        );
        
        useColumn.setCellValueFactory(
                new PropertyValueFactory<Page, Boolean>("use")
        );

        useColumn.setCellValueFactory(new Callback<CellDataFeatures<Page, Boolean>, ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(CellDataFeatures<Page, Boolean> param) {
                Page page = param.getValue();
                SimpleBooleanProperty booleanProp = new SimpleBooleanProperty(page.isUse());
                booleanProp.addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    page.setUse(newValue);
                    }
                });
                return booleanProp;
            }
        });
        useColumn.setCellFactory(new Callback<TableColumn<Page, Boolean>, TableCell<Page, Boolean>>() {
            @Override
            public TableCell<Page, Boolean> call(TableColumn<Page,Boolean> p){
                CheckBoxTableCell<Page, Boolean> cell = new CheckBoxTableCell<Page,Boolean>();
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });
        pageTable.getColumns().add(useColumn);
        pageTable.getColumns().add(navColumn);
        pageTable.getColumns().add(fileNameColumn);
        pageTable.getColumns().add(scriptColumn);
        
                //prefWidthProperty().bind(taTable.widthProperty().multiply(0.4)); // changing the width of email Tab
       
        
        partVbox2.getChildren().add(pageTable);
        
        
        
        partVbox3.getChildren().add(pageStyleLabel);
        
        courseDetailsGridPane2 = new GridPane();
        i=0;
        courseDetailsGridPane2.add(bannerSchoolLabel, 0, i);
        
        //courseDetailsGridPane2.add(changeBannerImageView, 1, i);
        //courseDetailsGridPane2.add(new Rectangle(200,20), 1, i);
        indexBannerChange = i;
        courseDetailsGridPane2.add(changeBannerButton, 2, i++);
        courseDetailsGridPane2.add(leftFooterLabel, 0, i);
        
        //courseDetailsGridPane2.add(leftFooterImageView, 1, i);
        //courseDetailsGridPane2.add(new ImageView(), 1, i);
         
        indexLeftFooter = i;
        courseDetailsGridPane2.add(changeLeftFooterButton, 2, i++);
        courseDetailsGridPane2.add(rightFooterLabel, 0, i);
        
        //courseDetailsGridPane2.add(rightFooterImageView, 1, i);
        //courseDetailsGridPane2.add(new ImageView(), 1, i);
        indexRightFooter = i;
        courseDetailsGridPane2.add(changeRightFooterButton, 2, i++);
        courseDetailsGridPane2.add(stylesheetLabel, 0, i);
        courseDetailsGridPane2.add(stylesheetComboBox , 1, i++);
        
        //courseDetailsGridPane2.add(stylesheetNoteLabel, 0, i++);
        //courseDetailsGridPane2.add(stylesheetNoteLabel, 0, i++);
    
        
        partVbox3.getChildren().add(courseDetailsGridPane2);
        partVbox3.getChildren().add(stylesheetNoteLabel);
        
        allVbox.getChildren().add(partVbox1);
        allVbox.getChildren().add(partVbox2);
        allVbox.getChildren().add(partVbox3);
     
        
        subjectComboBox.setItems(data.getSubjectOptions());
        numberComboBox.setItems(data.getNumberOptions());
        semesterComboBox.setItems(data.getSemesterOptions());
        yearComboBox.setItems(data.getYearOptions());
        stylesheetComboBox.setItems(data.getStylesheetOptions());
               
            
        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        //sPane = new SplitPane(leftPane, new ScrollPane(rightPane), sidePane);
        
        //DIVIDE THE PANE EVENLY
        //sPane.setDividerPositions(0.4f, 0.9f, 0.95f);
         
        
        //workspace = new BorderPane();
        
        // AND PUT EVERYTHING IN THE WORKSPACE
        //((BorderPane) workspace).setCenter(sPane);
        
        //workspace =  new Pane(courseDetailsGridPane);
        changeBannerImageView = new ImageView();
        leftFooterImageView = new ImageView(); 
        rightFooterImageView = new ImageView(); 
        
        allVbox.prefWidthProperty().bind(app.getGUI().getAppPane().widthProperty().multiply(1));
        workspace =  new Pane(getAllVbox());
        
        changeDirButton.setOnAction(e -> {
            DirectoryChooser dc = new DirectoryChooser();
            dc.setInitialDirectory(new File(PATH_TO_HTML));
            File selectedFile = dc.showDialog(app.getGUI().getWindow());
            if (selectedFile != null) {
                String dirPath = selectedFile.getAbsolutePath();
                //String dirPath = selectedFile.getAbsolutePath();
                data.setExportDir(FILE_PROTOCOL + dirPath);
                pathLabel.setText(dirPath);
            }
        });
        
        selectTemplateDirButton.setOnAction(e -> {
            DirectoryChooser dc = new DirectoryChooser();
            dc.setInitialDirectory(new File(PATH_TO_HTML));
            File selectedFile = dc.showDialog(app.getGUI().getWindow());
            if (selectedFile != null) {
                //String dirPath = selectedFile.getName();
                //String dirPath = selectedFile.getName();
                String dirPath = selectedFile.getAbsolutePath();
                data.setSite(FILE_PROTOCOL + dirPath);
                templateChooseLabel.setText(dirPath);
            }
        });
        
        
        
        changeBannerButton.setOnAction(e -> {
            FileChooser fc = new FileChooser();
            fc.setInitialDirectory(new File(PATH_BRANDS));
            File selectedPath = fc.showOpenDialog(app.getGUI().getWindow());
            
            //changeBannerName = selectedPath.getName();
            //String imgPath = FILE_PROTOCOL +  selectedPath.getAbsolutePath();
                String imgPath = FILE_PROTOCOL + PATH_BRANDS + selectedPath.getName();
                //String dirPath = selectedFile.getAbsolutePath();
            System.out.println(imgPath);
            data.setBanner(imgPath);
            Image image = new Image(imgPath,350,50,false, false);
            
            //data.setBanner(imgPath);data.setLeftFooter(imgPath);data.setRightFooter(imgPath);
            changeBannerImageView.setImage(image);
            
            courseDetailsGridPane2.add(changeBannerImageView, 1, indexBannerChange);  
        });
        
        changeLeftFooterButton.setOnAction(e -> {
            FileChooser fc = new FileChooser();
            fc.setInitialDirectory(new File(PATH_BRANDS));
            File selectedPath = fc.showOpenDialog(app.getGUI().getWindow());
            
            //leftFooterName = selectedPath.getName();
            //changeBannerName = selectedPath.getName();
            //String imgPath = FILE_PROTOCOL +  selectedPath.getAbsolutePath();
                String imgPath = FILE_PROTOCOL  + PATH_BRANDS + selectedPath.getName();
                //String dirPath = selectedFile.getAbsolutePath();
            System.out.println(imgPath);
            data.setLeftFooter(imgPath);
            Image image = new Image(imgPath,350,50,false, false);
                   
            leftFooterImageView.setImage(image);
            
            courseDetailsGridPane2.add(leftFooterImageView, 1, indexLeftFooter); 
        });
            
        changeRightFooterButton.setOnAction(e -> {
            FileChooser fc = new FileChooser();
            fc.setInitialDirectory(new File(PATH_BRANDS));
            File selectedPath = fc.showOpenDialog(app.getGUI().getWindow());
            
            //rightFooterName = selectedPath.getName();
            //changeBannerName = selectedPath.getName();
            //String imgPath = FILE_PROTOCOL +  selectedPath.getAbsolutePath();
               String imgPath = FILE_PROTOCOL  + PATH_BRANDS + selectedPath.getName();
                //String dirPath = selectedFile.getAbsolutePath();
            System.out.println(imgPath);
            data.setRightFooter(imgPath);
            Image image = new Image(imgPath,350,50,false, false);
                 
            rightFooterImageView.setImage(image);
            
            courseDetailsGridPane2.add(rightFooterImageView, 1, indexRightFooter); 
        });
        
        subjectComboBox.setOnAction(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });
        numberComboBox.setOnAction(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });
        semesterComboBox.setOnAction(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });
        yearComboBox.setOnAction(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });
        
        titleTextField.setOnKeyTyped(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });
        instructorNameTextField.setOnKeyTyped(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });
        instructorHomeTextField.setOnKeyTyped(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });
        
        titleTextField.setOnAction(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });
        instructorNameTextField.setOnAction(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });
        instructorHomeTextField.setOnAction(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });
        /*pathLabel.setOnAction(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });
        templateChooseLabel.setOnAction(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });*/
        stylesheetComboBox.setOnAction(e -> {
            data.initCourseDetails(subjectComboBox.getSelectionModel().getSelectedItem().toString(), numberComboBox.getSelectionModel().getSelectedItem().toString(),semesterComboBox.getSelectionModel().getSelectedItem().toString(),yearComboBox.getSelectionModel().getSelectedItem().toString(), titleTextField.getText(),instructorNameTextField.getText(),instructorHomeTextField.getText(), pathLabel.getText(), templateChooseLabel.getText(), stylesheetComboBox.getSelectionModel().getSelectedItem().toString());
      
        });
        
        //reloadWorkspace(data);
    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT}    
    }
    
    @Override
    public void resetWorkspace() {
        // CLEAR OUT
        //controller.clearSelection();
        data.initClearCourseDetails();
        data.getPages().clear();
        
        subjectComboBox.setValue(data.getSubject());
        numberComboBox.setValue(data.getNumber());
        semesterComboBox.setValue(data.getSemester());
        yearComboBox.setValue(data.getYear());
        
        titleTextField.setText(data.getCourseTitle());
        instructorNameTextField.setText(data.getInstructorName());
        instructorHomeTextField.setText(data.getInstructorHome());
        /*
        pathLabel.setText(data.getExportDir().substring(5));
        templateChooseLabel.setText(data.getSite().substring(5));*/
        stylesheetComboBox.setValue(data.getStylesheet());
        
        /*
        String imgPath = data.getBanner();
        Image image = new Image(imgPath,350,50,false, false);          
        changeBannerImageView.setImage(image);

        imgPath = data.getLeftFooter();
        image = new Image(imgPath,350,50,false, false);
        leftFooterImageView.setImage(image);
 
        imgPath = data.getRightFooter();
        image = new Image(imgPath,350,50,false, false);        
        rightFooterImageView.setImage(image);

        courseDetailsGridPane2.add(changeBannerImageView, 1, indexBannerChange);  
        courseDetailsGridPane2.add(leftFooterImageView, 1, indexLeftFooter);
        courseDetailsGridPane2.add(rightFooterImageView, 1, indexRightFooter); 
        */
    //reloadWorkspace(data);
    
        
    }
    
    @Override
    public void addPagesForNewOnly()
    {
        
        data.addPage("Home", "index.html", "HomeBuilder.js", true);
        data.addPage("Syllabus", "syllabus.html", "SyllabusBuilder.js", true);
        data.addPage("Schedule", "schedule.html", "ScheduleBuilder.js", true);
        data.addPage("HWs", "hws.html", "HWsBuilder.js", true);
        data.addPage("Projects", "projects.html", "ProjectsBuilder.js", true);
        data.initCourseDetails("", "", "", "", "", "", "", "", "", "");
    }
    

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        TAData data = (TAData) dataComponent;
        //System.out.println(da);
        subjectComboBox.setValue(data.getSubject());
        numberComboBox.setValue(data.getNumber());
        semesterComboBox.setValue(data.getSemester());
        yearComboBox.setValue(data.getYear());
        
        titleTextField.setText(data.getCourseTitle());
        instructorNameTextField.setText(data.getInstructorName());
        instructorHomeTextField.setText(data.getInstructorHome());
        pathLabel.setText(data.getExportDir().substring(5));
        templateChooseLabel.setText(data.getSite().substring(5));
        stylesheetComboBox.setValue(data.getStylesheetOptions());
        stylesheetComboBox.setValue(data.getStylesheet());
        
        
        
        
        String imgPath = data.getBanner();
        Image image = new Image(imgPath,350,50,false, false);          
        changeBannerImageView.setImage(image);

        imgPath = data.getLeftFooter();
        image = new Image(imgPath,350,50,false, false);
        leftFooterImageView.setImage(image);
 
        imgPath = data.getRightFooter();
        image = new Image(imgPath,350,50,false, false);        
        rightFooterImageView.setImage(image);

        courseDetailsGridPane2.add(changeBannerImageView, 1, indexBannerChange);  
        courseDetailsGridPane2.add(leftFooterImageView, 1, indexLeftFooter);
        courseDetailsGridPane2.add(rightFooterImageView, 1, indexRightFooter); 
    }

    /**
     * @return the courseInfoLabel
     */
    public Label getCourseInfoLabel() {
        return courseInfoLabel;
    }

    /**
     * @return the subjectLabel
     */
    public Label getSubjectLabel() {
        return subjectLabel;
    }

    /**
     * @return the numberLabel
     */
    public Label getNumberLabel() {
        return numberLabel;
    }

    /**
     * @return the semesterLabel
     */
    public Label getSemesterLabel() {
        return semesterLabel;
    }

    /**
     * @return the yearLabel
     */
    public Label getYearLabel() {
        return yearLabel;
    }

    /**
     * @return the titleLabel
     */
    public Label getTitleLabel() {
        return titleLabel;
    }

    /**
     * @return the instructorNameLabel
     */
    public Label getInstructorNameLabel() {
        return instructorNameLabel;
    }

    /**
     * @return the instructorHomeLabel
     */
    public Label getInstructorHomeLabel() {
        return instructorHomeLabel;
    }

    /**
     * @return the exportDirLabel
     */
    public Label getExportDirLabel() {
        return exportDirLabel;
    }

    /**
     * @return the pathLabel
     */
    public Label getPathLabel() {
        return pathLabel;
    }

    /**
     * @return the subjectComboBox
     */
    public ComboBox getSubjectComboBox() {
        return subjectComboBox;
    }

    /**
     * @return the numberComboBox
     */
    public ComboBox getNumberComboBox() {
        return numberComboBox;
    }

    /**
     * @return the semesterComboBox
     */
    public ComboBox getSemesterComboBox() {
        return semesterComboBox;
    }

    /**
     * @return the yearComboBox
     */
    public ComboBox getYearComboBox() {
        return yearComboBox;
    }

    /**
     * @return the stylesheetComboBox
     */
    public ComboBox getStylesheetComboBox() {
        return stylesheetComboBox;
    }

    /**
     * @return the titleTextField
     */
    public TextField getTitleTextField() {
        return titleTextField;
    }

    /**
     * @return the instructorNameTextField
     */
    public TextField getInstructorNameTextField() {
        return instructorNameTextField;
    }

    /**
     * @return the instructorHomeTextField
     */
    public TextField getInstructorHomeTextField() {
        return instructorHomeTextField;
    }

    /**
     * @return the changeDirButton
     */
    public Button getChangeDirButton() {
        return changeDirButton;
    }

    /**
     * @return the siteTemplateLabel
     */
    public Label getSiteTemplateLabel() {
        return siteTemplateLabel;
    }

    /**
     * @return the templateInfoLabel
     */
    public Label getTemplateInfoLabel() {
        return templateInfoLabel;
    }

    /**
     * @return the sitePagesLabel
     */
    public Label getSitePagesLabel() {
        return sitePagesLabel;
    }

    /**
     * @return the pageStyleLabel
     */
    public Label getPageStyleLabel() {
        return pageStyleLabel;
    }

    /**
     * @return the bannerSchoolLabel
     */
    public Label getBannerSchoolLabel() {
        return bannerSchoolLabel;
    }

    /**
     * @return the leftFooterLabel
     */
    public Label getLeftFooterLabel() {
        return leftFooterLabel;
    }

    /**
     * @return the rightFooterLabel
     */
    public Label getRightFooterLabel() {
        return rightFooterLabel;
    }

    /**
     * @return the stylesheetLabel
     */
    public Label getStylesheetLabel() {
        return stylesheetLabel;
    }

    /**
     * @return the stylesheetNoteLabel
     */
    public Label getStylesheetNoteLabel() {
        return stylesheetNoteLabel;
    }

    /**
     * @return the selectTemplateDirButton
     */
    public Button getSelectTemplateDirButton() {
        return selectTemplateDirButton;
    }

    /**
     * @return the changeBannerButton
     */
    public Button getChangeBannerButton() {
        return changeBannerButton;
    }

    /**
     * @return the changeLeftFooterButton
     */
    public Button getChangeLeftFooterButton() {
        return changeLeftFooterButton;
    }

    /**
     * @return the changeRightFooterButton
     */
    public Button getChangeRightFooterButton() {
        return changeRightFooterButton;
    }

    /**
     * @return the courseDetailsGridPane
     */
    public GridPane getCourseDetailsGridPane() {
        return courseDetailsGridPane;
    }

    /**
     * @return the pageTable
     */
    public TableView getPageTable() {
        return pageTable;
    }

    /**
     * @return the useColumn
     */
    public TableColumn getUseColumn() {
        return useColumn;
    }

    /**
     * @return the navColumn
     */
    public TableColumn getNavColumn() {
        return navColumn;
    }

    /**
     * @return the fileNameColumn
     */
    public TableColumn getFileNameColumn() {
        return fileNameColumn;
    }

    /**
     * @return the scriptColumn
     */
    public TableColumn getScriptColumn() {
        return scriptColumn;
    }

    /**
     * @return the sPane
     */
    public SplitPane getsPane() {
        return sPane;
    }

    /**
     * @return the partVbox1
     */
    public VBox getPartVbox1() {
        return partVbox1;
    }

    /**
     * @return the partVbox2
     */
    public VBox getPartVbox2() {
        return partVbox2;
    }

    /**
     * @return the partVbox3
     */
    public VBox getPartVbox3() {
        return partVbox3;
    }

    /**
     * @return the allVbox
     */
    public VBox getAllVbox() {
        return allVbox;
    }

    public void getExportDirLabel(String PATH_EXPORT) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the templateChooseLabel
     */
    public Label getTemplateChooseLabel() {
        return templateChooseLabel;
    }

    /**
     * @return the controller
     */
    public courseController getController() {
        return controller;
    }
    
    
    /**
     * @return the changeBannerImageView
     */
    public ImageView getChangeBannerImageView() {
        return changeBannerImageView;
    }

    /**
     * @param changeBannerImageView the changeBannerImageView to set
     */
    public void setChangeBannerImageView(ImageView changeBannerImageView) {
        this.changeBannerImageView = changeBannerImageView;
    }

    /**
     * @return the leftFooterImageView
     */
    public ImageView getLeftFooterImageView() {
        return leftFooterImageView;
    }

    /**
     * @param leftFooterImageView the leftFooterImageView to set
     */
    public void setLeftFooterImageView(ImageView leftFooterImageView) {
        this.leftFooterImageView = leftFooterImageView;
    }

    /**
     * @return the rightFooterImageView
     */
    public ImageView getRightFooterImageView() {
        return rightFooterImageView;
    }

    /**
     * @param rightFooterImageView the rightFooterImageView to set
     */
    public void setRightFooterImageView(ImageView rightFooterImageView) {
        this.rightFooterImageView = rightFooterImageView;
    }

    /**
     * @return the changeBannerName
     */
    public String getChangeBannerName() {
        return changeBannerName;
    }

    /**
     * @param changeBannerName the changeBannerName to set
     */
    public void setChangeBannerName(String changeBannerName) {
        this.changeBannerName = changeBannerName;
    }

    /**
     * @return the leftFooterName
     */
    public String getLeftFooterName() {
        return leftFooterName;
    }

    /**
     * @param leftFooterName the leftFooterName to set
     */
    public void setLeftFooterName(String leftFooterName) {
        this.leftFooterName = leftFooterName;
    }

    /**
     * @return the rightFooterName
     */
    public String getRightFooterName() {
        return rightFooterName;
    }

    /**
     * @param rightFooterName the rightFooterName to set
     */
    public void setRightFooterName(String rightFooterName) {
        this.rightFooterName = rightFooterName;
    }
}
