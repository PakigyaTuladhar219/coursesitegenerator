/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import tam.TAManagerApp;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.shape.Rectangle;
import properties_manager.PropertiesManager;
import tam.csgProp;
import tam.workspace.TAController;
import javafx.scene.control.ScrollPane;

/**
 * This class serves as the workspace component for the TA Manager
 * application. It provides all the user interface controls in 
 * the workspace area.
 * 
 * @author Richard McKenna
 * @coauthor Pakigya Tuladhar
 */
public class csgWorkspace extends AppWorkspaceComponent {
    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    TAManagerApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    TAController controller;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    

    /**
     * The constructor initializes the user interface, except for
     * the full office hours grid, since it doesn't yet know what
     * the hours will be until a file is loaded or a new one is created.
     */
    public csgWorkspace(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        
        TabPane tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        
        Tab tabCourseDetails = new Tab();
        tabCourseDetails.setText(props.getProperty(csgProp.COURSE_DETAILS_TAB_TEXT.toString()));
        tabCourseDetails.setContent(new ScrollPane(app.getCourseWorkspaceComponent().getWorkspace()));
        tabPane.getTabs().add(tabCourseDetails); 
        
        Tab tabTA = new Tab();
        tabTA.setText(props.getProperty(csgProp.TA_DATA_TAB_TEXT.toString()));
        //tabTA.setContent(new ScrollPane(app.getTAWorkspaceComponent().getWorkspace()));
        tabTA.setContent(app.getTAWorkspaceComponent().getWorkspace());
        tabPane.getTabs().add(tabTA);
        
        Tab tabRecitation = new Tab();
        tabRecitation.setText(props.getProperty(csgProp.RECITATION_TAB_TEXT.toString()));
        tabRecitation.setContent(new ScrollPane(app.getRecitationWorkspaceComponent().getWorkspace()));
        tabPane.getTabs().add(tabRecitation);
        
        Tab tabSchedule = new Tab();
        tabSchedule.setText(props.getProperty(csgProp.SCHEDULE_TAB_TEXT.toString()));
        tabSchedule.setContent(new ScrollPane(app.getScheduleWorkspaceComponent().getWorkspace()));
        tabPane.getTabs().add(tabSchedule);
        
        Tab tabProject = new Tab();
        tabProject.setText(props.getProperty(csgProp.PROJECT_DATA_TAB_TEXT.toString()));
        tabProject.setContent(new ScrollPane(app.getProjectWorkspaceComponent().getWorkspace()));
        tabPane.getTabs().add(tabProject);
        
        tabWorkspace = tabPane;
       // app.getGUI().getAppPane().setCenter(tabPane);
        //workspace = tabPane;
        // AND PUT EVERYTHING IN THE WORKSPACE
        //((BorderPane) workspace).setCenter(tabPane);

    }

    @Override
    public void resetWorkspace() {
        System.out.println("reseting workspace");
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        System.out.println("reloading workspace");
    }
}