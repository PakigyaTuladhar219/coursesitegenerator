package tam.data;

import csg.data.Page;
import csg.data.Recitation;
import csg.data.Schedule;
import csg.data.Student;
import csg.data.Team;
import csg.workspace.courseWorkspace;
import csg.workspace.projectWorkspace;
import csg.workspace.recitationWorkspace;
import csg.workspace.scheduleWorkspace;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import static djf.settings.AppStartupConstants.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.TAManagerProp;
import static tam.style.TAStyle.Hover;
import tam.workspace.TAWorkspace;

/**
 * This is the data component for TAManagerApp. It has all the data needed
 * to be set by the user via the User Interface and file I/O can set and get
 * all the data from this object
 * 
 * @author Richard McKenna
 * @coauthor Pakigya Tuladhar
 */
public class TAData implements AppDataComponent, Cloneable {
    
    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    TAManagerApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<TeachingAssistant> teachingAssistants;
    
     ObservableList<Page> pages;    
     ObservableList<Recitation> recitations;
     ObservableList<Schedule> schedules;
     ObservableList<Team> teams;
     ObservableList<Student> students;
     
      
    // THIS WILL STORE ALL THE OFFICE HOURS GRID DATA, WHICH YOU
    // SHOULD NOTE ARE StringProperty OBJECTS THAT ARE CONNECTED
    // TO UI LABELS, WHICH MEANS IF WE CHANGE VALUES IN THESE
    // PROPERTIES IT CHANGES WHAT APPEARS IN THOSE LABELS
    HashMap<String, StringProperty> officeHours;
    
    // THESE ARE THE LANGUAGE-DEPENDENT VALUES FOR
    // THE OFFICE HOURS GRID HEADERS. NOTE THAT WE
    // LOAD THESE ONCE AND THEN HANG ON TO THEM TO
    // INITIALIZE OUR OFFICE HOURS GRID
    ArrayList<String> gridHeaders;

    // THESE ARE THE TIME BOUNDS FOR THE OFFICE HOURS GRID. NOTE
    // THAT THESE VALUES CAN BE DIFFERENT FOR DIFFERENT FILES, BUT
    // THAT OUR APPLICATION USES THE DEFAULT TIME VALUES AND PROVIDES
    // NO MEANS FOR CHANGING THESE VALUES
    int startHour;
    int endHour;
    
    // DEFAULT VALUES FOR START AND END HOURS IN MILITARY HOURS
    public static final int MIN_START_HOUR = 0;
    public static final int MAX_END_HOUR = 23;
    
    //CHECK IF THE DATA IS LOADED
    public boolean isLoaded = false;

    /**
     * This constructor will setup the required data structures for
     * use, but will have to wait on the office hours grid, since
     * it receives the StringProperty objects from the Workspace.
     *
     */
    public TAData() {

        // CONSTRUCT THE LIST OF TAs FOR THE TABLE
        pages = FXCollections.observableArrayList();
        teachingAssistants = FXCollections.observableArrayList();
        recitations = FXCollections.observableArrayList();
        schedules = FXCollections.observableArrayList();
        teams = FXCollections.observableArrayList();
        students = FXCollections.observableArrayList();

        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        
        //THIS WILL STORE OUR OFFICE HOURS
        officeHours = new HashMap();
        
        // THESE ARE THE LANGUAGE-DEPENDENT OFFICE HOURS GRID HEADERS
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        ArrayList<String> timeHeaders = props.getPropertyOptionsList(TAManagerProp.OFFICE_HOURS_TABLE_HEADERS);
        ArrayList<String> dowHeaders = props.getPropertyOptionsList(TAManagerProp.DAYS_OF_WEEK);
        gridHeaders = new ArrayList();
        gridHeaders.addAll(timeHeaders);
        gridHeaders.addAll(dowHeaders);
        
    }
    
    
    public TAData(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // CONSTRUCT THE LIST OF TAs FOR THE TABLE
        pages = FXCollections.observableArrayList();
        teachingAssistants = FXCollections.observableArrayList();
        recitations = FXCollections.observableArrayList();
        schedules = FXCollections.observableArrayList();
        teams = FXCollections.observableArrayList();
        students = FXCollections.observableArrayList();

        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        
        //THIS WILL STORE OUR OFFICE HOURS
        officeHours = new HashMap();
        
        // THESE ARE THE LANGUAGE-DEPENDENT OFFICE HOURS GRID HEADERS
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> timeHeaders = props.getPropertyOptionsList(TAManagerProp.OFFICE_HOURS_TABLE_HEADERS);
        ArrayList<String> dowHeaders = props.getPropertyOptionsList(TAManagerProp.DAYS_OF_WEEK);
        gridHeaders = new ArrayList();
        gridHeaders.addAll(timeHeaders);
        gridHeaders.addAll(dowHeaders);
        
        
        
    }
    /**
     * Called each time new work is created or loaded, it resets all data
     * and data structures such that they can be used for new values.
     */
    @Override
    public void resetData() {
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        teachingAssistants.clear();
        pages.clear();
        recitations.clear();
        schedules.clear();
        teams.clear();
        students.clear();
        officeHours.clear();
        resetPageTable();
        initClearCourseDetails();
        
    }
    
    public void resetPageTable()
    {
        /*
        addPage("Home", "index.html", "HomeBuilder.js", true);
        addPage("Syllabus", "syllabus.html", "SyllabusBuilder.js", true);
        addPage("Schedule", "schedule.html", "ScheduleBuilder.js", true);
        addPage("HWs", "hws.html", "HWsBuilder.js", true);
        addPage("Projects", "projects.html", "ProjectsBuilder.js", true);
        */
        app.getCourseWorkspaceComponent().resetWorkspace();
        app.getTAWorkspaceComponent().resetWorkspace();
        app.getRecitationWorkspaceComponent().resetWorkspace();
        app.getScheduleWorkspaceComponent().resetWorkspace();
        app.getProjectWorkspaceComponent().resetWorkspace();
        app.getCourseWorkspaceComponent().resetWorkspace();
    }
    
    public void resetTime(){
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        teachingAssistants.clear();
        //officeHours.clear();
    }
    
    // ACCESSOR METHODS

    public int getStartHour() {
        return startHour;
    }

    public int getEndHour() {
        return endHour;
    }
    
    public void setStartHour(int start) {
        startHour = start;
    }

    public void setEndHour(int end) {
        endHour = end;
    }
    
    public ObservableList<String> getGridTimeOptions(){
        ObservableList<String> options = FXCollections.observableArrayList();
        options.add("12:00 am");
        int hour=1;
        String minutesText = "00";
        
        /*if (!onHour) {
            minutesText = "30";
        }*/
        int hourToAdd;
        for (hour=1; hour<24;hour++)
        {
            hourToAdd = hour;
            if (hour == 12) {
                //hourToAdd -= 12;
                options.add(hourToAdd + ":" + minutesText + " pm");
            }
            else if (hour > 12) {
                hourToAdd -= 12;
                options.add(hourToAdd + ":" + minutesText + " pm");
            }
            else
            {
                options.add(hourToAdd + ":" + minutesText + " am");
            }
        }
        return options;
    }
    
    public ArrayList<String> getGridHeaders() {
        return gridHeaders;
    }

    public ObservableList getTeachingAssistants() {
        return teachingAssistants;
    }
    
    public String getCellKey(int col, int row) {
        return col + "_" + row;
    }

    public StringProperty getCellTextProperty(int col, int row) {
        String cellKey = getCellKey(col, row);
        return officeHours.get(cellKey);
    }

    public HashMap<String, StringProperty> getOfficeHours() {
        return officeHours;
    }
    
    public int getNumRows() {
        return ((endHour - startHour) * 2) + 1;
    }

    public String getTimeString(int militaryHour, boolean onHour) {
        String minutesText = "00";
        if (!onHour) {
            minutesText = "30";
        }

        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    
    public String getCellKey(String day, String time) {
        ///FIX THIS FOR PROPER TIME GRID
        ///*
        int col = gridHeaders.indexOf(day);
        int row = 1;
        int hour = Integer.parseInt(time.substring(0, time.indexOf("_")));
        int milHour = hour;
        
        if (time.contains("am") && time.contains("12"))
        {
            row += startHour;
        }
        else if (time.contains("am")){
            row += (milHour - startHour);
        }
        else
        {
            row += (milHour - startHour)+12;
        }    
            if (time.contains("_30"))
                row += 1;
            return getCellKey(col, row);
        //*/
        /*
        int col = gridHeaders.indexOf(day);
        int row = 1;
        int hour = Integer.parseInt(time.substring(0, time.indexOf("_")));
        int milHour = hour;
        if (hour < startHour) {
            milHour += 12;
        }
        row += (milHour - startHour) * 2;
        if (time.contains("_30")) {
            row += 1;
        }
        return getCellKey(col, row);
        */
    }
    
    public TeachingAssistant getTA(String testName) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName)) {
                return ta;
            }
        }
        return null;
    }
    
    /**
     * This method is for giving this data manager the string property
     * for a given cell.
     */
    public void setCellProperty(int col, int row, StringProperty prop) {
        String cellKey = getCellKey(col, row);
        officeHours.put(cellKey, prop);
    }    
    
    /**
     * This method is for setting the string property for a given cell.
     */
    public void setGridProperty(ArrayList<ArrayList<StringProperty>> grid,
                                int column, int row, StringProperty prop) {
        grid.get(row).set(column, prop);
    }
    
    
    /*
    * THis method updates the time for office hours 
     */
    public void updateTime(int newStartHour, int newEndHour) {
        HashMap<String, StringProperty> newOfficeHours;
        newOfficeHours = new HashMap();
        TAWorkspace workspaceComponent = (TAWorkspace) app.getTAWorkspaceComponent();
        //int shiftIndex = (newEndHour - newStartHour) * 2 + 1;
        int oldStartRow = (newStartHour - startHour) * 2 + 1;
        int newStartRow = 1;
        if (newStartHour >= startHour && newEndHour <= endHour) // IF New Time is 10 and old time is 8.  New End is 16 , old end is 20         && newEndHour<=endHour
        {

            for (int col = 2; col < 7; col++) {
                for (int i = 0; i < ((newEndHour - newStartHour) * 2); i++) {

                    int row = oldStartRow;
                    String cellKey = col + "_" + row;
                    StringProperty prop = officeHours.get(cellKey);
                    String newCellKey = col + "_" + newStartRow;
                    newOfficeHours.put(newCellKey, prop);
                    oldStartRow++;
                    newStartRow++;

                }
                oldStartRow = (newStartHour - startHour) * 2 + 1;
                newStartRow = 1;
            }
        } else if (newStartHour >= startHour && newEndHour > endHour) {

            for (int col = 2; col < 7; col++) {
                for (int i = 0; i < ((endHour - newStartHour) * 2); i++) {

                    int row = oldStartRow;
                    String cellKey = col + "_" + row;
                    StringProperty prop = officeHours.get(cellKey);
                    String newCellKey = col + "_" + newStartRow;
                    newOfficeHours.put(newCellKey, prop);
                    oldStartRow++;
                    newStartRow++;

                }
                oldStartRow = (newStartHour - startHour) * 2 + 1;
                newStartRow = 1;
            }
        } else if (startHour > newStartHour && endHour >= newEndHour) {
            oldStartRow = 1;
            newStartRow = (startHour - newStartHour) * 2 + 1;
            for (int col = 2; col < 7; col++) {
                for (int i = 0; i < ((newEndHour - startHour) * 2); i++) {

                    int row = oldStartRow;
                    String cellKey = col + "_" + row;
                    StringProperty prop = officeHours.get(cellKey);
                    String newCellKey = col + "_" + newStartRow;
                    newOfficeHours.put(newCellKey, prop);
                    oldStartRow++;
                    newStartRow++;

                }
                oldStartRow = 1;
                newStartRow = (startHour - newStartHour) * 2 + 1;
            }
        } else if (startHour > newStartHour && newEndHour > endHour) {
            oldStartRow = 1;
            newStartRow = (startHour - newStartHour) * 2 + 1;
            for (int col = 2; col < 7; col++) {
                for (int i = 0; i < ((endHour - startHour) * 2); i++) {

                    int row = oldStartRow;
                    String cellKey = col + "_" + row;
                    StringProperty prop = officeHours.get(cellKey);
                    String newCellKey = col + "_" + newStartRow;
                    newOfficeHours.put(newCellKey, prop);
                    oldStartRow++;
                    newStartRow++;

                }
                oldStartRow = 1;
                newStartRow = (startHour - newStartHour) * 2 + 1;
            }
        }


        workspaceComponent.resetWorkspace();
        initOfficeHours(newStartHour, newEndHour);

        for (HashMap.Entry<String, StringProperty> entry : newOfficeHours.entrySet()) {
            String key = entry.getKey();
            StringProperty prop = newOfficeHours.get(key);
            toggleTAOfficeHoursUpdate(key, prop);
        }
    }

    
    private void initOfficeHoursTest(int initStartHour, int initEndHour) {
        // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
        startHour = initStartHour;
        endHour = initEndHour;
    }
    
    private void initOfficeHours(int initStartHour, int initEndHour) {
        // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
        startHour = initStartHour;
        endHour = initEndHour;
        
        // EMPTY THE CURRENT OFFICE HOURS VALUES
        //officeHours.clear();
            
        // WE'LL BUILD THE USER INTERFACE COMPONENT FOR THE
        // OFFICE HOURS GRID AND FEED THEM TO OUR DATA
        // STRUCTURE AS WE GO
        TAWorkspace workspaceComponent = (TAWorkspace)app.getTAWorkspaceComponent();
        workspaceComponent.reloadOfficeHoursGrid(this);
    }
    
    
    /*private void initOfficeHoursUpdate(int initStartHour, int initEndHour) {
        // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
        startHour = initStartHour;
        endHour = initEndHour;

        // EMPTY THE CURRENT OFFICE HOURS VALUES
        //officeHours.clear();

        // WE'LL BUILD THE USER INTERFACE COMPONENT FOR THE
        // OFFICE HOURS GRID AND FEED THEM TO OUR DATA
        // STRUCTURE AS WE GO
        TAWorkspace workspaceComponent = (TAWorkspace) app.getTAWorkspaceComponent();
        workspaceComponent.reloadOfficeHoursGrid(this);
    }*/

    public void initHoursTest(String startHourText, String endHourText) {
        int initStartHour = Integer.parseInt(startHourText);
        int initEndHour = Integer.parseInt(endHourText);
        if ((initStartHour >= MIN_START_HOUR)
                && (initEndHour <= MAX_END_HOUR)
                && (initStartHour <= initEndHour)) {
            // THESE ARE VALID HOURS SO KEEP THEM
            initOfficeHoursTest(initStartHour, initEndHour);
        }
    }
    public void initHours(String startHourText, String endHourText) {
        int initStartHour = Integer.parseInt(startHourText);
        int initEndHour = Integer.parseInt(endHourText);
        if ((initStartHour >= MIN_START_HOUR)
                && (initEndHour <= MAX_END_HOUR)
                && (initStartHour <= initEndHour)) {
            // THESE ARE VALID HOURS SO KEEP THEM
            initOfficeHours(initStartHour, initEndHour);
        }
    }

    public boolean containsTA(TeachingAssistant testTA) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.equals(testTA) ) {
                return true;
            }
        }
        return false;
    }    
    
    public boolean containsTA(String testName) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().toLowerCase().equals(testName.toLowerCase()) ) {
                return true;
            }
        }
        return false;
    }    
    
    public boolean containsTA(String testName, String testEmail) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().toLowerCase().equals(testName.toLowerCase()) || ta.getEmail().toLowerCase().equals(testEmail.toLowerCase()) ) {
                return true;
            }
        }
        return false;
    }     
    
    public boolean containsTAEmail(String testEmail) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getEmail().equals(testEmail)) {
                return true;
            }
        }
        return false;
    }

    public void addTA(String initName) {
        // MAKE THE TA
        TeachingAssistant ta = new TeachingAssistant(initName);

        // ADD THE TA
        if (!containsTA(initName)) {
            teachingAssistants.add(ta);
        }

        // SORT THE TAS
        Collections.sort(teachingAssistants);
    }
    public void addTA(String initName, String initEmail) {
        // MAKE THE TA along with email
        TeachingAssistant ta = new TeachingAssistant(initName.trim(), initEmail.trim());

        // ADD THE TA along with email
        if (!containsTA(initName)) {
            teachingAssistants.add(ta);
        }
        // SORT THE TAS
        Collections.sort(teachingAssistants);
    }
    public void addTA(String initName, String initEmail, boolean undergrad) {
        // MAKE THE TA along with email
        TeachingAssistant ta = new TeachingAssistant(initName.trim(), initEmail.trim(), undergrad);

        // ADD THE TA along with email
        if (!containsTA(initName)) {
            teachingAssistants.add(ta);
        }
        // SORT THE TAS
        Collections.sort(teachingAssistants);
        refreshTables();
    }
    
    public void updateTA(String initName, String initEmail, String newName, String newEmail) {
        // Delete THE TA along with email
        TeachingAssistant ta = new TeachingAssistant(initName.trim(), initEmail.trim());
        TeachingAssistant newTA = new TeachingAssistant(newName.trim(), newEmail.trim());
        if (containsTA(ta)) {
            teachingAssistants.remove(ta);
            teachingAssistants.add(newTA);
            replaceFromEverywhere(initName, newName);
        }
        // SORT THE TAS
        Collections.sort(teachingAssistants);
            System.out.println(teachingAssistants);
        refreshTables();
    }
    public void tempRemoveTA(String initName, String initEmail){
        // Delete THE TA along with email
        TeachingAssistant ta = new TeachingAssistant(initName.trim(), initEmail.trim());
        if (containsTA(ta)) {
            teachingAssistants.remove(ta);
        }
        // SORT THE TAS
        Collections.sort(teachingAssistants);
            System.out.println(teachingAssistants);
    }
    
    public void deleteTA(String initName, String initEmail) {
        // Delete THE TA along with email
        TeachingAssistant ta = new TeachingAssistant(initName.trim(), initEmail.trim());
        if (containsTA(ta)) {
            teachingAssistants.remove(ta);
        }
        removeFromEverywhere(initName);
        // SORT THE TAS
        Collections.sort(teachingAssistants);
            System.out.println(teachingAssistants);
        refreshTables();
    }

    public void addOfficeHoursReservation(String day, String time, String taName) {
        String cellKey = getCellKey(day, time);
        toggleTAOfficeHours(cellKey, taName);
    }
    
    /**
     * This function toggles the taName in the cell represented
     * by cellKey. Toggle means if it's there it removes it, if
     * it's not there it adds it.
     */
    public void toggleTAOfficeHours(String cellKey, String taName) {
        StringProperty cellProp = officeHours.get(cellKey);
        String cellText = cellProp.getValue();
        if (isThereTAInCell(cellProp, taName) == true)
        {
            removeTAFromCell(cellProp, taName);
        }
        else
        {
            cellProp.setValue(cellText + "\n" + taName);
        }
    }
    
    public void toggleTAOfficeHoursUpdate(String cellKey, StringProperty prop) {
        String cellText2 = prop.getValue();
        StringProperty cellProp = officeHours.get(cellKey);
        //String cellText = cellProp.getValue();
        cellProp.set(cellText2);
    }
    /**
     * This method removes all instances of TA
     * from the Grid Cell
     * @param taName 
     */
    public void removeFromEverywhere(String taName)
    {
        // COLUMNS from 2 to 6 // ROWS from 1 to 22
        int row =1; int col = 2;
        for (row=1; row<=22;row++ )
        {
            for (col=2;col<=6;col++)
            {
                String cellKey = col + "_" + row;
                StringProperty cellProp = officeHours.get(cellKey);
                if (isThereTAInCell(cellProp, taName) == true)
                {
                    removeTAFromCell(cellProp, taName);
                }
            }
        }
        
        for (int i = 0; i< recitations.size();i++)
        {
            if (recitations.get(i).getSupervisingTA1().equals(taName)){
                recitations.get(i).setSupervisingTA1("");
                if (recitations.get(i).isEmpty()){
                    recitations.remove(recitations.get(i));
                }
                Collections.sort(recitations);
            }
            else if (recitations.get(i).getSupervisingTA2().equals(taName)){
                recitations.get(i).setSupervisingTA2("");
                if (recitations.get(i).isEmpty()){
                    recitations.remove(recitations.get(i));
                }
                Collections.sort(recitations);
            }
        }
    }
    /**
     * This method removes all instances of TA
     * from the Grid Cell
     * @param taName 
     * @param newTAName 
     */
    public void replaceFromEverywhere(String taName, String newTAName)
    {
        // COLUMNS from 2 to 6 // ROWS from 1 to 22
        int row =1; int col = 2;
        for (row=1; row<=22;row++ )
        {
            for (col=2;col<=6;col++)
            {
                String cellKey = col + "_" + row;
                StringProperty cellProp = officeHours.get(cellKey);
                if (isThereTAInCell(cellProp, taName) == true)
                {
                    replaceTAFromCell(cellProp, taName, newTAName);
                }
            }
        }
        
        for (int i = 0; i< recitations.size();i++)
        {
            
                System.out.println(recitations.get(i));
            if (recitations.get(i).getSupervisingTA1().equals(taName)){
                recitations.get(i).setSupervisingTA1(newTAName);
                Collections.sort(recitations);
            }
            else if (recitations.get(i).getSupervisingTA2().equals(taName)){
                recitations.get(i).setSupervisingTA2(newTAName);
                Collections.sort(recitations);
            }
        }
        refreshTables();
    }
    
    public void refreshTables()
    {
        TAWorkspace taWorkspaceComponent = (TAWorkspace) app.getTAWorkspaceComponent();
        recitationWorkspace recitationWorkspaceComponent = (recitationWorkspace) app.getRecitationWorkspaceComponent();
        scheduleWorkspace scheduleWorkspaceComponent = (scheduleWorkspace) app.getScheduleWorkspaceComponent();
        courseWorkspace courseWorkspaceComponent = (courseWorkspace) app.getCourseWorkspaceComponent();
        projectWorkspace projectWorkspaceComponent = (projectWorkspace) app.getProjectWorkspaceComponent();
        
        
        courseWorkspaceComponent.getPageTable().refresh();
        taWorkspaceComponent.getTATable().refresh();
        recitationWorkspaceComponent.getRecitationTable().refresh();
        scheduleWorkspaceComponent.getScheduleTable().refresh();
        projectWorkspaceComponent.getTeamTable().refresh();
        projectWorkspaceComponent.getStudentTable().refresh();
        
        Collections.sort(teachingAssistants);
        Collections.sort(recitations);
        Collections.sort(pages);
        Collections.sort(schedules);
        Collections.sort(teams);
        Collections.sort(students);
    }
    /**
     * This method checks whether the TA exists in the name office
     * grid cell already
     */
    public boolean isThereTAInCell(StringProperty cellProp, String taName){
         // GET THE CELL TEXT
        String cellText = cellProp.getValue();
        if (cellText.contains(taName))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * This method removes taName from the office grid cell
     * represented by cellProp.
     */
    public void removeTAFromCell(StringProperty cellProp, String taName) {
        // GET THE CELL TEXT
        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(taName)) {
            cellProp.setValue("");
        }
        // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(taName) == 0) {
            int startIndex = cellText.indexOf("\n") + 1;
            cellText = cellText.substring(startIndex);
            cellProp.setValue(cellText);
        }
        // IT MUST BE ANOTHER TA IN THE CELL
        else {
            int startIndex = cellText.indexOf("\n" + taName);
            String initial = cellText.substring(0, startIndex); // Saves the data before the index of TA
            String remaining = cellText.substring(startIndex+taName.length()+1); //Save the data after the fullname of the TA
            cellText = initial + remaining; //Removes the TA
            cellProp.setValue(cellText);
        }
    }
    
     /**
     * This method replaces taName from the office grid cell
     * represented by cellProp.
     * @param cellProp
     * @param taName
     * @param newName
     */
    public void replaceTAFromCell(StringProperty cellProp, String taName, String newName) {
        // GET THE CELL TEXT
        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(taName)) {
            cellProp.setValue(newName);
        }
        // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(taName) == 0) {
            int startIndex = cellText.indexOf("\n") + 1;
            cellText = cellText.substring(startIndex);
            cellProp.setValue(newName + "\n" + cellText);
        }
        // IT MUST BE ANOTHER TA IN THE CELL
        else {
            int startIndex = cellText.indexOf("\n" + taName);
            String initial = cellText.substring(0, startIndex); // Saves the data before the index of TA
            String remaining = cellText.substring(startIndex+taName.length()+1); //Save the data after the fullname of the TA
            cellText = initial + "\n" + newName +  remaining; //Replaces the TA
            cellProp.setValue(cellText);
        }
    }
    
    /**
     * This method highlights all the left and above cells when a cell is hovered
     * @param cellKey
     */
    public void highlightDuringHover(String cellKey, boolean flag)
    {
        TAWorkspace workspaceComponent = (TAWorkspace)app.getTAWorkspaceComponent();
        //TAStyle styleComponent = (TAStyle)app.getStyleComponent();
        // COLUMNS from 2 to 6 // ROWS from 1 to 22
        
        int row =0; int col = 2;
        String arr[] = cellKey.split("_");
        int col1 = Integer.parseInt(arr[0]);
        int row1 = Integer.parseInt(arr[1]);
        if (flag ==  true)
        {
            
            //workspaceComponent.getOfficeHoursGridPane(getCellKey(col1,0)).getStyleClass().add("-fx-border-color: #fcffc4;");
            for (row=1; row< row1; row++ )
            {
                workspaceComponent.getTACellPane(getCellKey(col1,row)).getStyleClass().add(Hover(flag));
            }

            //workspaceComponent.getTACellPane(getCellKey(0,row1)).getStyleClass().add("-fx-border-color: #fcffc4;");
            //workspaceComponent.getTACellPane(getCellKey(1,row1)).getStyleClass().add("-fx-border-color: #fcffc4;");
            for (col=2;col< col1 ; col++)
            {
                workspaceComponent.getTACellPane(getCellKey(col,row1)).getStyleClass().add(Hover(flag));
            }
        }
        else
        {
            //workspaceComponent.getTACellPane(getCellKey(col1,0)).getStyleClass().remove(Hover(flag));
            for (row=1; row< row1; row++ )
            {
                workspaceComponent.getTACellPane(getCellKey(col1,row)).getStyleClass().remove(Hover(true));
            }

            //workspaceComponent.getTACellPane(getCellKey(0,row1)).getStyleClass().remove("-fx-border-color: #fcffc4;");
            //workspaceComponent.getTACellPane(getCellKey(1,row1)).getStyleClass().remove("-fx-border-color: #fcffc4;");
            for (col=2;col< col1 ; col++)
            {
                workspaceComponent.getTACellPane(getCellKey(col,row1)).getStyleClass().remove(Hover(true));
            }
        }
    }
    

    /**
     * @return the recitations
     */
    public ObservableList<Recitation> getRecitations() {
        return recitations;
    }
    
    public void addRecitation(String initSection, String initInstructor, String initDayTime, String initLocation, String initTA1, String initTA2){
        // MAKE THE RECITATION
        Recitation rec = new Recitation(initSection.trim(), initInstructor.trim(), initDayTime.trim(), initLocation.trim(), initTA1.trim(), initTA2.trim());
        // ADD THE RECITATION]
        recitations.add(rec);
        // SORT THE RECITATIONS
        Collections.sort(recitations);
        refreshTables();
    }

    /**
     * @return the schedules
     */
    public ObservableList<Schedule> getSchedules() {
        return schedules;
    }
    
 
    public void addSchedule(String initType, String initDate, String initTime, String initTitle, String initTopic, String initLink, String initCriteria){
        // MAKE THE Schedule
        Schedule sche = new Schedule(initType.trim(), initDate.trim(), initTime.trim(), initTitle.trim(), initTopic.trim(), initLink.trim(), initCriteria.trim());
        // ADD THE Schedule]
        schedules.add(sche);
        // SORT THE schedules
        Collections.sort(schedules);
        refreshTables();
    }

    /**
     * @return the teams
     */
    public ObservableList<Team> getTeams() {
        return teams;
    }
    
 
    public void addTeam(String initName, String initBGcolor, String initTextColor, String initLink){
        // MAKE THE Team
        Team team = new Team(initName.trim(), initBGcolor.trim(), initTextColor.trim(), initLink.trim());
        // ADD THE Team
        teams.add(team);
        // SORT THE teams
        Collections.sort(teams);
        refreshTables();
    }
    
    public void addTeam(Team team){
        // ADD THE Team
        teams.add(team);
        // SORT THE teams
        Collections.sort(teams);
        refreshTables();
    }

    
    /**
     * @return the students
     */
    public ObservableList<Student> getStudents() {
        return students;
    }
    
    public void addStudent(String initFirstName, String initLastName, String initTeam, String initRole){
        // MAKE THE Student
        Student student = new Student(initFirstName.trim(), initLastName.trim(), initTeam.trim(), initRole.trim());
        // ADD THE Student
        students.add(student);
        // SORT THE students
        Collections.sort(students);
        refreshTables();
    }
    
    public void addStudent(Student student){
        // ADD THE Student
        students.add(student);
        // SORT THE students
        Collections.sort(students);
        refreshTables();
    }
     
    /**
     * @return the students
     */
    public ObservableList<Page> getPages() {
        return pages;
    }
    
    public void addPage(String initNav, String initFileName, String initScript, boolean initUse){
        // MAKE THE Page
        Page page = new Page(initNav.trim(), initFileName.trim(), initScript.trim(), initUse);
        // ADD THE Page
        pages.add(page);
        // SORT THE pages
        Collections.sort(pages);
        refreshTables();
    }
    
    
    int startYear;
    int startMonth;
    int startDay;
    
    int endYear;
    int endMonth;
    int endDay;
    
    public void initDates(int startYear, int startMonth, int startDay, int endYear, int endMonth, int endDay){
        this.startYear = startYear;
        this.startMonth  = startMonth;
        this.startDay  = startDay;
         this.endYear = endYear;
        this.endMonth  = endMonth;
        this.endDay  = endDay;
    }
    
    String subject;
    String number;
    String semester;
    String year;
    String courseTitle ;
    String instructorName;
    String instructorHome;
    String exportDir;
    String site;
    String stylesheet;
    String banner;
    String leftFooter;
    String rightFooter;
    
    public void initClearCourseDetails()
    {
        this.subject = "";
        this.number = "";
        this.semester = "";
        this.year = "";
        this.courseTitle = "" ;
        this.instructorName = "";
        this.instructorHome = "";
        this.setExportDir("");
        this.setSite("");
        this.setStylesheet("");
        this.banner = PATH_DEFAULT_BANNER;       
        this.leftFooter = PATH_DEFAULT_LEFT_FOOTER;       
        this.rightFooter = PATH_DEFAULT_RIGHT_FOOTER;        
    }
    public void initCourseDetails(String subject,  String number,  String semester,  String year,  String courseTitle ,  String instructorName,  String instructorHome,  String exportDir,  String site,  String stylesheet)
    {
        this.subject = subject;
        this.number = number;
        this.semester = semester;
        this.year = year;
        this.courseTitle = courseTitle ;
        this.instructorName = instructorName;
        this.instructorHome = instructorHome;
        this.setExportDir(exportDir);
        this.setSite(site);
        this.setStylesheet(stylesheet);
        this.banner = PATH_DEFAULT_BANNER;       
        this.leftFooter = PATH_DEFAULT_LEFT_FOOTER;       
        this.rightFooter = PATH_DEFAULT_RIGHT_FOOTER; 
    }
    
    public void initCourseDetailsOnly(String subject,  String number,  String semester,  String year,  String courseTitle ,  String instructorName,  String instructorHome,  String exportDir,  String site,  String stylesheet)
    {
        this.subject = subject;
        this.number = number;
        this.semester = semester;
        this.year = year;
        this.courseTitle = courseTitle ;
        this.instructorName = instructorName;
        this.instructorHome = instructorHome;
        this.setExportDir(exportDir);
        this.setSite(site);
        this.setStylesheet(stylesheet);
    }
    
    public void initCourseDetails(String subject,  String number,  String semester,  String year,  String courseTitle ,  String instructorName,  String instructorHome,  String exportDir,  String site,  String stylesheet, String banner, String leftFooter, String rightFooter)
    {
        this.subject = subject;
        this.number = number;
        this.semester = semester;
        this.year = year;
        this.courseTitle = courseTitle ;
        this.instructorName = instructorName;
        this.instructorHome = instructorHome;
        this.setExportDir(exportDir);
        this.setSite(site);
        this.setStylesheet(stylesheet);
        this.banner = banner;       
        this.leftFooter = leftFooter;       
        this.rightFooter = rightFooter;  
    }

    /**
     * @return the endMonth
     */
    public int getEndMonth() {
        return endMonth;
    }

    /**
     * @return the endDay
     */
    public int getEndDay() {
        return endDay;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @return the semester
     */
    public String getSemester() {
        return semester;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @return the courseTitle
     */
    public String getCourseTitle() {
        return courseTitle;
    }

    /**
     * @return the instructorName
     */
    public String getInstructorName() {
        return instructorName;
    }

    /**
     * @return the instructorHome
     */
    public String getInstructorHome() {
        return instructorHome;
    }

    /**
     * @return the exportDir
     */
    public String getExportDir() {
        return exportDir;
    }

    /**
     * @return the site
     */
    public String getSite() {
        return site;
    }

    /**
     * @return the stylesheet
     */
    public String getStylesheet() {
        return stylesheet;
    }
     /**
     * @return the banner
     */
    public String getBanner() {
        return banner;
    }

    /**
     * @param banner the banner to set
     */
    public void setBanner(String banner) {
        this.banner = banner;
    }

    /**
     * @return the leftFooter
     */
    public String getLeftFooter() {
        return leftFooter;
    }

    /**
     * @param leftFooter the leftFooter to set
     */
    public void setLeftFooter(String leftFooter) {
        this.leftFooter = leftFooter;
    }

    /**
     * @return the rightFooter
     */
    public String getRightFooter() {
        return rightFooter;
    }

    /**
     * @param rightFooter the rightFooter to set
     */
    public void setRightFooter(String rightFooter) {
        this.rightFooter = rightFooter;
    }

    /**
     * @return the startYear
     */
    public int getStartYear() {
        return startYear;
    }

    /**
     * @return the startMonth
     */
    public int getStartMonth() {
        return startMonth;
    }

    /**
     * @return the startDay
     */
    public int getStartDay() {
        return startDay;
    }

    /**
     * @return the endYear
     */
    public int getEndYear() {
        return endYear;
    }
    
    /**
     * @param exportDir the exportDir to set
     */
    public void setExportDir(String exportDir) {
        this.exportDir = exportDir;
    }

    /**
     * @param site the site to set
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     * @param stylesheet the stylesheet to set
     */
    public void setStylesheet(String stylesheet) {
        this.stylesheet = stylesheet;
    }
    
    public ObservableList<String> getSubjectOptions(){
        ObservableList<String> options = FXCollections.observableArrayList();
        options.add("AMS");
        options.add("CHE");
        options.add("CSE");
        options.add("MAT");
        options.add("MEC");
        options.add("PHY");
        return options;
    }
    
    public ObservableList<Integer> getNumberOptions(){
        ObservableList<Integer> options = FXCollections.observableArrayList();
        int i = 100;
        for (i=100; i<400; i++)
        {
            options.add(i);
        }
        return options;
    }
    
    public ObservableList<String> getSemesterOptions(){
        ObservableList<String> options = FXCollections.observableArrayList();
        options.add("Fall");
        options.add("Winter");
        options.add("Spring");
        options.add("Summer I");
        options.add("Summer I Extended");
        options.add("Summer I");
        options.add("Summer II Extended");
        return options;
    }
    
    public ObservableList<String> getStylesheetOptions(){
        ObservableList<String> options = FXCollections.observableArrayList();
        String path = PATH_TO_HTML;
        if (getSite()!= null){
            path = getSite().substring(5);
            System.out.print(getSite().substring(5));
        }
        //System.out.print(PATH_TO_HTML);
            
            System.out.print(path + PATH_CSS);
            File[] files = new File(path + PATH_CSS).listFiles();
            //File[] files = new File(getSite().substring(5)).listFiles();
            //If this pathname does not denote a directory, then listFiles() returns null. 

            for (File file : files) {
                if (file.isFile()) {
                    options.add(file.getName());
                }
            }
        return options;
    }
    
    public ObservableList<String> getTypeOptions(){
        ObservableList<String> options = FXCollections.observableArrayList();
        options.add("holidays");
        options.add("lectures");
        options.add("references");
        options.add("recitations");
        options.add("hws");
        return options;
    }
    
    public ObservableList<Integer> getYearOptions(){
        ObservableList<Integer> options = FXCollections.observableArrayList();
        int i = 2000;
        for (i=2000; i<2030; i++)
        {
            options.add(i);
        }
        return options;
    }
    
    // RECITATIONS
    public void updateRecitation(Recitation rec1, Recitation rec2) {
        // DELETE THE RECITATION
        recitations.remove(rec1);
        recitations.add(rec2);      
        // SORT THE RECITATIONS
        Collections.sort(recitations);
        System.out.println(recitations);
        refreshTables();
    }
    public void deleteRecitation(Recitation rec) {
        recitations.remove(rec);
        // SORT THE RECITATIONS
        Collections.sort(recitations);
        refreshTables();
    }
    
    // SCHEDULES
    public void updateSchedule(Schedule rec1, Schedule rec2) {
        // DELETE THE RECITATION
        schedules.remove(rec1);
        schedules.add(rec2);      
        // SORT THE RECITATIONS
        Collections.sort(schedules);
        System.out.println(schedules);
        refreshTables();
    }
    public void deleteSchedule(Schedule rec) {
        schedules.remove(rec);
        // SORT THE RECITATIONS
        Collections.sort(schedules);
        refreshTables();
    }
    
    public int getTypeIndex(String type){
        return getTypeOptions().indexOf(type);
    }
    
    //TEAMS
    public void updateTeam(Team rec1, Team rec2) {
        // DELETE THE RECITATION
        teams.remove(rec1);
        teams.add(rec2);      
        replaceTeamFromEverywhere(rec1.getName(), rec2.getName());
        // SORT THE RECITATIONS
        Collections.sort(teams);
        System.out.println(teams);
        refreshTables();
    }
    public void tempDeleteTeam(Team rec) {
        teams.remove(rec);
        // SORT THE RECITATIONS
        Collections.sort(teams);
        refreshTables();
    }
    
    public void deleteTeam(Team rec) {
        teams.remove(rec);
        // SORT THE RECITATIONS
        removeTeamFromEverywhere(rec.getName());
        Collections.sort(teams);
        refreshTables();
    }
    
    
    /**
     * This method removes all instances of TEAM
     * from the Grid Cell
     * @param teamName 
     */
    public void removeTeamFromEverywhere(String teamName)
    {        
        for (int i = 0; i< students.size();i++)
        {
            if (students.get(i).getTeam().equals(teamName)){
                students.get(i).setTeam("");
                Collections.sort(students);
            }
        }
    }
    /**
     * This method removes all instances of TEAM
     * from the Grid Cell
     * @param teamName 
     * @param newTeamName 
     */
    public void replaceTeamFromEverywhere(String teamName, String newTeamName)
    {        
        System.out.print(teamName + " ---- "    + newTeamName);
        for (int i = 0; i< students.size();i++)
        {
            System.out.println( students.get(i));
            
            System.out.print("kata ho yp "  +   students.get(i).getTeam());
            if (students.get(i).getTeam().equals(teamName)){
                System.out.print("bhitra " + students.get(i).getTeam());
                students.get(i).setTeam(newTeamName);
                Collections.sort(students);
            }
        }
        refreshTables();
    }
    
    
    public boolean containsTeam(String testName) {
        for (Team std : teams) {
            if (std.getName().toLowerCase().equals(testName.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsBgColor(String color) {
        for (Team team : teams) {
            if (team.getBGcolor().toLowerCase().equals(color.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
    
    public Team getTeam(String testName) {
        for (Team team : teams) {
            if (team.getName().equals(testName)) {
                return team;
            }
        }
        return null;
    }
       
    public int getTeamIndex(String type){
        return teams.indexOf(getTeam(type));
    }
    
    
    //STUDENTS
    public void updateStudent(Student std1, Student std2) {
        // DELETE THE RECITATION
        students.remove(std1);
        students.add(std2);      
        // SORT THE RECITATIONS
        Collections.sort(students);
        System.out.println(students);
        refreshTables();
    }
    public void deleteStudent(Student rec) {
        students.remove(rec);
        // SORT THE RECITATIONS
        Collections.sort(students);
        refreshTables();
    }
    
    public boolean containsStudent(String testFirstName, String testLastName) {
        for (Student std : students) {
            if (std.getFirstName().toLowerCase().equals(testFirstName.toLowerCase()) && std.getLastName().toLowerCase().equals(testLastName.toLowerCase()) ) {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsStudent(Student testStd) {
        for (Student std : students) {
            if (std.getFirstName().toLowerCase().equals(testStd.getFirstName().toLowerCase()) && std.getLastName().toLowerCase().equals(testStd.getLastName().toLowerCase()) ) {
                return true;
            }
        }
        return false;
    }  
    
   
    public ObservableList<String> getStudentsInTeam(String testTeam)
    {
        ObservableList<String> options = FXCollections.observableArrayList();
        for (Student std : students) {
            if (std.getTeam().toLowerCase().equals(testTeam.toLowerCase()) )                
            {
                options.add(std.getFirstName() + " "+ std.getLastName());
            }
        }
        return options;
        /*
        for (Team team : teams) {
            if (team.getName().equals(testName)) {
                return team;
            }
        }
        return null;
    */
    }
    /*
    public ObservableList<String> getStudentsInTeam(String testTeam)
    {
        ObservableList<String> options = FXCollections.observableArrayList();
        for (Student std : students) {
            if (std.getTeam().toLowerCase().equals(testTeam.toLowerCase()) )                
            {
                options.add(std.getFirstName() + " "+ std.getLastName());
            }
        }
        return options;
        /*
        for (Team team : teams) {
            if (team.getName().equals(testName)) {
                return team;
            }
        }
        return null;
    
    }*/
    
}