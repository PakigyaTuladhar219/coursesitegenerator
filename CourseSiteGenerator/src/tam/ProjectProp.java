/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam;

/**
 * This enum provides a list of all the user interface
 * text that needs to be loaded from the XML properties
 * file. By simply changing the XML file we could initialize
 * this application such that all UI controls are provided
 * in another language.
 * 
 * This only contains for the tab pane.
 * 
 * @author PLT
 * @version 1.0
 */
public enum ProjectProp {
    PROJECTS_LABEL_TEXT,

    TEAMS_LABEL_TEXT,
    NAME_HEADER_TEXT,
    COLOR_HEADER_TEXT,
    TEXT_COLOR_HEADER_TEXT,
    LINK_HEADER_TEXT,

    NAME_LABEL_TEXT,
    COLOR_LABEL_TEXT,
    TEXT_COLOR_LABEL_TEXT,
    LINK_LABEL_TEXT,

    STUDENTS_HEADER_TEXT,
    FIRST_NAME_HEADER_TEXT,
    LAST_NAME_HEADER_TEXT,
    TEAM_HEADER_TEXT,
    ROLE_HEADER_TEXT,

    ADD_EDIT_LABEL_TEXT,
    FIRST_NAME_LABEL_TEXT,
    LAST_NAME_LABEL_TEXT,
    TEAM_LABEL_TEXT,
    ROLE_LABEL_TEXT,
    ADD_UPDATE_BUTTON_TEXT,
    ADD_BUTTON_TEXT,
    CLEAR_TEXT,
    
    START_TIME_LABEL_TEXT,
    END_TIME_LABEL_TEXT,

    SCHEDULE_ITEM_LABEL_TEXT,
    TYPE_HEADER_TEXT,
    DATE_HEADER_TEXT,
    TITLE_HEADER_TEXT,
    TOPIC_HEADER_TEXT,

    TYPE_LABEL_TEXT,
    DATE_LABEL_TEXT,
    TIME_LABEL_TEXT,
    TITLE_LABEL_TEXT,
    TOPIC_LABEL_TEXT,
    CRITERIA_LABEL_TEXT
}
