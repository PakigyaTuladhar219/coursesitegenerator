/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam;

/**
 * This enum provides a list of all the user interface
 * text that needs to be loaded from the XML properties
 * file. By simply changing the XML file we could initialize
 * this application such that all UI controls are provided
 * in another language.
 * 
 * This only contains for the tab pane.
 * 
 * @author PLT
 * @version 1.0
 */
public enum RecitationProp {
    
    RECITATIONS_LABEL_TEXT,

    SECTION_HEADER_TEXT,
    INSTRUCTOR_HEADER_TEXT,
    DAY_TIME_HEADER_TEXT,
    LOCATION_HEADER_TEXT,
    TA_HEADER_TEXT,

    ADD_EDIT_LABEL_TEXT,
    SECTION_LABEL_TEXT,
    INSTRUCTOR_LABEL_TEXT,
    DAY_TIME_LABEL_TEXT,
    LOCATION_LABEL_TEXT,
    SUPERVISING_TA__LABEL_TEXT,
    ADD_UPDATE_BUTTON_TEXT,
    ADD_BUTTON_TEXT,
    CLEAR_TEXT
}
