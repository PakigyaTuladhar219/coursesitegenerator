package tam.workspace;

import csg.transaction.*;

/**
 *
 * @author McKillaGorilla
 */
public interface jTPS_Transaction {
    public void doTransaction();
    public void undoTransaction();
}
