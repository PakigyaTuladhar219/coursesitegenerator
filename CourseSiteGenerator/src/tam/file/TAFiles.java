package tam.file;

import csg.data.Page;
import csg.data.Recitation;
import csg.data.Schedule;
import csg.data.Student;
import csg.data.Team;
import csg.workspace.courseWorkspace;
import csg.workspace.scheduleWorkspace;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import static djf.settings.AppPropertyType.CLOSE_BUTTON_TEXT;
import static djf.settings.AppPropertyType.EXPORT_COMPLETED_MESSAGE;
import static djf.settings.AppPropertyType.EXPORT_COMPLETED_TITLE;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_BRANDS;
import static djf.settings.AppStartupConstants.PATH_TO_HTML;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.shape.Path;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import org.apache.commons.io.FileUtils;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.workspace.TAController;

/**
 * This class serves as the file component for the TA
 * manager app. It provides all saving and loading 
 * services for the application.
 * 
 * @author Richard McKenna
 * @coauthor Pakigya Tuladhar
 */
public class TAFiles implements AppFileComponent {
    // THIS IS THE APP ITSELF
    TAManagerApp app;
    
    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_EMAIL = "email";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_GRAD_TAS = "grad_tas";
    
    public TAFiles() {
        
    }
    
    public TAFiles(TAManagerApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
	// CLEAR THE OLD DATA OUT
	TAData dataManager = (TAData)data;

	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);

	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);

          // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            //dataManager.addTA(name);
            //dataManager.addTA(name, email);
            dataManager.addTA(name, email, true);
        }
        // NOW LOAD ALL THE GRAD TAs
        jsonTAArray = json.getJsonArray(JSON_GRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            //dataManager.addTA(name);
            //dataManager.addTA(name, email);
            dataManager.addTA(name, email, false);
        }

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.addOfficeHoursReservation(day, time, name);
        }
        
        dataManager.isLoaded = true;
        TAController.getJTPS().clearTransaction();
    }

    /**
     * This method helps us to load the passed json data into the parent TA data
     * @param data
     * @param json
     * @throws IOException 
     */
    public void loadJsonData(AppDataComponent data, JsonObject json) throws IOException {
	// LOAD THE JSON FILE WITH ALL THE DATA
	//JsonObject json = loadJSONFile(filePath);

	// CLEAR THE OLD DATA OUT
	TAData dataManager = (TAData)data;
        dataManager.isLoaded = false;
        
	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);

        // NOW RELOAD THE WORKSPACE WITH THE LOADED DATA
        app.getTAWorkspaceComponent().reloadWorkspace(app.getDataComponent());

        // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            dataManager.addTA(name, email, true);
        }
        // NOW LOAD ALL THE GRAD TAs
        jsonTAArray = json.getJsonArray(JSON_GRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            dataManager.addTA(name, email, false);
        }

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.addOfficeHoursReservation(day, time, name);
        }
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	TAData dataManager = (TAData)data;

	// NOW BUILD THE TA JSON OBJECTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder taArrayBuilderGrad = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {
            if (ta.isUndergrad()==true){
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL, ta.getEmail()).build();
                taArrayBuilder.add(taJson);
            }
            else
            {
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL, ta.getEmail()).build();
                taArrayBuilderGrad.add(taJson);
            }
	}
	JsonArray undergradTAsArray = taArrayBuilder.build();
	JsonArray gradTAsArray = taArrayBuilderGrad.build();

	// NOW BUILD THE TIME SLOT JSON OBJECTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_GRAD_TAS, gradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    
    static final String JSON_PAGES = "pages";
    static final String JSON_UNUSED = "Unused";
    static final String JSON_USE = "use";
    static final String JSON_NAV= "nav";
    static final String JSON_FILENAME = "fileName";
    static final String JSON_SCRIPT = "script";
    
    static final String JSON_SUBJECT = "subject";
    static final String JSON_NUMBER = "number";
    static final String JSON_SEMESTER = "semester";
    static final String JSON_YEAR = "year";
    static final String JSON_TITLE = "title";
    static final String JSON_INSTRUCTORNAME = "instructorName";
    static final String JSON_INSTRUCTORHOME = "instructorHome";
    static final String JSON_EXPORTDIR = "exportDirectory";
    static final String JSON_SITETEMPLATE = "siteTemplate";
    static final String JSON_BANNER = "bannerSchool";
    static final String JSON_LEFTFOOTER = "leftFooter";
    static final String JSON_RIGHTFOOTER = "rightFooter";
    static final String JSON_STYLESHEET = "stylesheet";
    public void saveCourseData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	TAData dataManager = (TAData)data;
        courseWorkspace course = (courseWorkspace) app.getCourseWorkspaceComponent();

	// NOW BUILD THE TA JSON OBJECTS TO SAVE
	JsonArrayBuilder pageUsedArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder pageUnusedArrayBuilder = Json.createArrayBuilder();
	ObservableList<Page> pages = dataManager.getPages();
	for (Page pg : pages) {
            if (pg.isUse()==true){
                JsonObject pageJson = Json.createObjectBuilder()
                        .add(JSON_NAV, pg.getNav())
                        .add(JSON_FILENAME, pg.getFileName())
                        .add(JSON_SCRIPT, pg.getScript())
                        .build();
                pageUsedArrayBuilder.add(pageJson);
            }
            else
            {
                JsonObject pageJson = Json.createObjectBuilder()
                        .add(JSON_NAV, pg.getNav())
                        .add(JSON_FILENAME, pg.getFileName())
                        .add(JSON_SCRIPT, pg.getScript())
                        .build();
                pageUnusedArrayBuilder.add(pageJson);
            }
	}
	JsonArray UsedArray = pageUsedArrayBuilder.build();
	JsonArray UnusedArray = pageUnusedArrayBuilder.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
                /*
		.add(JSON_SUBJECT, "" + course.getSubjectComboBox().getSelectionModel().getSelectedItem().toString())
		.add(JSON_NUMBER, "" + course.getNumberComboBox().getSelectionModel().getSelectedItem().toString())
		.add(JSON_SEMESTER,"" + course.getSemesterComboBox().getSelectionModel().getSelectedItem().toString())
		.add(JSON_YEAR, "" + "" + course.getYearComboBox().getSelectionModel().getSelectedItem().toString())
		.add(JSON_TITLE, "" + course.getTitleTextField().getText())
		.add(JSON_INSTRUCTORNAME, "" + course.getInstructorNameTextField().getText())
		.add(JSON_INSTRUCTORHOME, "" + course.getInstructorHomeTextField().getText())
		.add(JSON_EXPORTDIR, "" + course.getPathLabel().getText())
		.add(JSON_SITETEMPLATE, "" + course.getTemplateChooseLabel().getText())
                // ADd the banners here
		.add(JSON_STYLESHEET, "" + course.getStylesheetComboBox().getSelectionModel().getSelectedItem().toString())
                */
                .add(JSON_SUBJECT, "" + dataManager.getSubject())
                .add(JSON_NUMBER, "" + dataManager.getNumber())
                .add(JSON_SEMESTER,"" + dataManager.getSemester())
                .add(JSON_YEAR, "" + "" + dataManager.getYear())
                .add(JSON_TITLE, "" + dataManager.getCourseTitle())
                .add(JSON_INSTRUCTORNAME, "" + dataManager.getInstructorName())
                .add(JSON_INSTRUCTORHOME, "" + dataManager.getInstructorHome())
                .add(JSON_EXPORTDIR, "" + dataManager.getExportDir())
                .add(JSON_SITETEMPLATE, "" + dataManager.getSite())
                .add(JSON_BANNER, "" + dataManager.getBanner().substring(getLastIndexOf(dataManager.getBanner())))
                .add(JSON_LEFTFOOTER, "" + dataManager.getLeftFooter().substring(getLastIndexOf(dataManager.getLeftFooter())))
                .add(JSON_RIGHTFOOTER, "" + dataManager.getRightFooter().substring(getLastIndexOf(dataManager.getRightFooter())))
                // ADd the banners here
                .add(JSON_STYLESHEET, "" + dataManager.getStylesheet())
                .add(JSON_USE, UsedArray)
                .add(JSON_UNUSED, UnusedArray)

                
                
                .add(JSON_USE, UsedArray)
                .add(JSON_UNUSED, UnusedArray)
                /*.add(JSON_USED, UsedArray)
                .add(JSON_USED, UnusedArray)*/
		//.add(JSON_END_HOUR, "" + dataManager.getEndHour())
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    int getLastIndexOf(String str)
    {
        if (str.lastIndexOf("/") > str.lastIndexOf("\\"))
        {
            return str.lastIndexOf("/")+1;
        }
        else
        {
            return str.lastIndexOf("\\")+1;
        }
    }
    
    static final String JSON_RECITATIONS = "recitations";
    static final String JSON_SECTION = "section";
    static final String JSON_INSTRUCTOR = "instructor";
    static final String JSON_DAYTIME = "day_time";
    static final String JSON_LOCATION = "location";
    static final String JSON_TA1 = "ta_1";
    static final String JSON_TA2 = "ta_2";
    public void saveRecitationData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	TAData dataManager = (TAData)data;

	// NOW BUILD THE TA JSON OBJECTS TO SAVE
	JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
	ObservableList<Recitation> recs = dataManager.getRecitations();
	for (Recitation rec : recs) {
                JsonObject recitationJson = Json.createObjectBuilder()
                        .add(JSON_SECTION, "<strong>" + rec.getSection() + "</strong> (" + rec.getInstructor() +")")
                        .add(JSON_INSTRUCTOR, rec.getInstructor())
                        .add(JSON_DAYTIME, rec.getDayTime())
                        .add(JSON_LOCATION, rec.getLocation())
                        .add(JSON_TA1, rec.getSupervisingTA1())
                        .add(JSON_TA2, rec.getSupervisingTA2())
                        .build();
                recitationArrayBuilder.add(recitationJson);
	}
	JsonArray recitationArray = recitationArrayBuilder.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_RECITATIONS, recitationArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    
    
    static final String JSON_SCHEDULE = "schedules";
    static final String JSON_STARTDATEMONTH = "startDateMonth";
    static final String JSON_STARTDATEDAY = "startDateDay";
    static final String JSON_STARTDATEYEAR = "startDateYear";
    static final String JSON_ENDDATEMONTH = "endDateMonth";
    static final String JSON_ENDDATEDAY = "endDateDay";
    static final String JSON_ENDDATEYEAR = "endDateYear";
    
    
    static final String JSON_STARTMONDAYMONTH = "startingMondayMonth";
    static final String JSON_STARTMONDAYDAY = "startingMondayDay";
    static final String JSON_ENDFRIDAYMONTH = "endingFridayMonth";
    static final String JSON_ENDFRIDAYDAY = "endingFridayDay";
    
    static final String JSON_TYPE = "type";
    static final String JSON_DATE = "date";
    static final String JSON_MONTH = "month"; 
    static final String JSON_TOPIC = "topic";
    static final String JSON_LINK = "link";
    static final String JSON_CRITERIA = "criteria";
    static final String JSON_HOLIDAYS = "holidays";
    static final String JSON_LECTURES = "lectures";
    static final String JSON_REFERENCES = "references";
    static final String JSON_HWS = "hws";
    
    public void saveScheduleData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	TAData dataManager = (TAData)data;
        
	// NOW BUILD THE TA JSON OBJECTS TO SAVE
	JsonArrayBuilder holidaysArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder lecturesArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder referencesArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder recitationsArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder hwsArrayBuilder = Json.createArrayBuilder();
	ObservableList<Schedule> sches = dataManager.getSchedules();
	for (Schedule sche : sches) {
                JsonObject recitationJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, "" + sche.getDateFormat().getMonthValue())
                        .add(JSON_DAY, "" + sche.getDateFormat().getDayOfMonth())
                        .add(JSON_TIME, sche.getTime())
                        .add(JSON_TITLE, sche.getTitle() )
                        .add(JSON_TOPIC, sche.getTopic() )
                        .add(JSON_LINK, sche.getLink())
                        .add(JSON_CRITERIA, sche.getCriteria() )
                        .build();
                
                if (sche.getType().equals(JSON_HOLIDAYS))
                {
                    holidaysArrayBuilder.add(recitationJson);                
                }
                else if (sche.getType().equals(JSON_LECTURES))
                {
                    lecturesArrayBuilder.add(recitationJson);                
                }
                else if (sche.getType().equals(JSON_REFERENCES))
                {
                    referencesArrayBuilder.add(recitationJson);                
                }
                else if (sche.getType().equals(JSON_RECITATIONS))
                {
                    recitationsArrayBuilder.add(recitationJson);                
                }
                else if (sche.getType().equals(JSON_HWS))
                {
                    hwsArrayBuilder.add(recitationJson);                
                }
	}
	//JsonArray scheduleArray = schedulArrayBuilder.build();
        JsonArray holidaysArray = holidaysArrayBuilder.build();
	JsonArray lecturesArray = lecturesArrayBuilder.build();
	JsonArray referencesArray = referencesArrayBuilder.build();
	JsonArray recitationsArray = recitationsArrayBuilder.build();
	JsonArray hwsArray = hwsArrayBuilder.build();
	
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_STARTMONDAYMONTH, "" + dataManager.getStartMonth() )
                .add(JSON_STARTMONDAYDAY, "" + dataManager.getStartDay())
                .add(JSON_STARTDATEYEAR, "" + dataManager.getStartYear())
                
                .add(JSON_ENDFRIDAYMONTH, "" + dataManager.getEndMonth())
                .add(JSON_ENDFRIDAYDAY, "" + dataManager.getEndDay())
                .add(JSON_ENDDATEYEAR, "" + dataManager.getStartYear())
                
                .add(JSON_HOLIDAYS, holidaysArray)
		.add(JSON_LECTURES, lecturesArray)
                .add(JSON_REFERENCES, referencesArray)
                .add(JSON_RECITATIONS, recitationsArray)
                .add(JSON_HWS, hwsArray)
                
                .build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    
    static final String JSON_TEAMS = "teams";
    //static final String JSON_NAME = "name";
    static final String JSON_BGCOLOR = "color";
    static final String JSON_TEXTCOLOR = "textColor";
    static final String JSON_TEXT_COLOR = "text_color";
    //static final String JSON_LINK = "link";
    static final String JSON_STUDENTS = "students";
    static final String JSON_FIRSTNAME = "firstName";
    static final String JSON_LASTNAME = "lastName";
    static final String JSON_TEAM = "team";
    static final String JSON_ROLE = "role";
    static final String JSON_WORK = "work";
    static final String JSON_PROJECTS = "projects";
    
    public void saveProjectData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	TAData dataManager = (TAData)data;

	// NOW BUILD THE TA JSON OBJECTS TO SAVE
	JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder projectsArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder workArrayBuilder = Json.createArrayBuilder();
	ObservableList<Team> teams = dataManager.getTeams();
	for (Team team : teams) {
            for (String student : dataManager.getStudentsInTeam(team.getName())) {
                //
                        studentArrayBuilder.add(student);
                        //.build();
                //studentArrayBuilder.add(studentJson);
            }
            JsonArray studentArray = studentArrayBuilder.build();
            JsonObject teamJson = Json.createObjectBuilder()
                        .add(JSON_NAME, team.getName())
                        .add(JSON_STUDENTS, studentArray)
                        .add(JSON_LINK, team.getLink() )
                        .build();
            projectsArrayBuilder.add(teamJson);
	}
	JsonArray projectArray = projectsArrayBuilder.build();
       
        JsonObject workJson = Json.createObjectBuilder()
                        .add(JSON_SEMESTER, dataManager.getSemester() + " " + dataManager.getYear())
                        .add(JSON_PROJECTS, projectArray)
                        .build();
        
        workArrayBuilder.add(workJson);
	
	JsonArray workArray = workArrayBuilder.build();
        //*/
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_WORK, workArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    /**
     *
     * @param data
     * @param filePath
     * @throws IOException
     */
    @Override
    public void saveTeamData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	TAData dataManager = (TAData)data;

	// NOW BUILD THE TA JSON OBJECTS TO SAVE
	JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
	ObservableList<Team> teams = dataManager.getTeams();
	for (Team team : teams) {
                JsonObject teamJson = Json.createObjectBuilder()
                        .add(JSON_NAME, team.getName())
                        .add(JSON_RED , "" + team.getRed())
                        .add(JSON_GREEN , "" + team.getGreen())
                        .add(JSON_BLUE , "" + team.getBlue())
                        .add(JSON_TEXT_COLOR, "" + team.getTextColor())
                        .build();
                teamArrayBuilder.add(teamJson);
	}
	JsonArray teamArray = teamArrayBuilder.build();
        
	JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
	ObservableList<Student> students = dataManager.getStudents();
	for (Student student : students) {
                JsonObject studentJson = Json.createObjectBuilder()
                        .add(JSON_LASTNAME, student.getLastName())
                        .add(JSON_FIRSTNAME, student.getFirstName())
                        .add(JSON_TEAM,  student.getTeam())
                        .add(JSON_ROLE,  student.getRole() )
                        .build();
                studentArrayBuilder.add(studentJson);
	}
	JsonArray studentArray = studentArrayBuilder.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
                //.add(JSON_)
                .add(JSON_TEAMS, teamArray)
                .add(JSON_STUDENTS, studentArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    /**
     * This method helps us to change the data on the UI to a json object so that we can save it
     * @param data
     * @return
     * @throws IOException 
     */
    public JsonObject saveJsonData(AppDataComponent data) throws IOException {
	// GET THE DATA
	TAData dataManager = (TAData)data;

	// NOW BUILD THE TA JSON OBJECTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder taArrayBuilderGrad = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {
            if (ta.isUndergrad()==true){
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL, ta.getEmail()).build();
                taArrayBuilder.add(taJson);
            }
            else
            {
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL, ta.getEmail()).build();
                taArrayBuilderGrad.add(taJson);
            }
	}
	JsonArray undergradTAsArray = taArrayBuilder.build();
	JsonArray gradTAsArray = taArrayBuilderGrad.build();

	// NOW BUILD THE TIME SLOT JSON OBJECTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_GRAD_TAS, gradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
		.build();
	
        return dataManagerJSO;
    }

    
    /**
     * This method helps us to change the data on the UI to a json object so that we can save it
     * @param data
     * @return
     * @throws IOException 
     */
    public JsonObject saveJsonData(AppDataComponent data, int start, int end) throws IOException {
	// GET THE DATA
	TAData dataManager = (TAData)data;

// NOW BUILD THE TA JSON OBJECTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder taArrayBuilderGrad = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {
            if (ta.isUndergrad()==true){
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL, ta.getEmail()).build();
                taArrayBuilder.add(taJson);
            }
            else
            {
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL, ta.getEmail()).build();
                taArrayBuilderGrad.add(taJson);
            }
	}
	JsonArray undergradTAsArray = taArrayBuilder.build();
	JsonArray gradTAsArray = taArrayBuilderGrad.build();

	// NOW BUILD THE TIME SLOT JSON OBJECTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_GRAD_TAS, gradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
		.build();
	
        return dataManagerJSO;
    }

    
    
    
    @Override
    public void saveAllData(AppDataComponent data, String filePath) throws IOException {
    	// GET THE DATA
	TAData dataManager = (TAData)data;
        courseWorkspace course = (courseWorkspace) app.getCourseWorkspaceComponent();
        scheduleWorkspace sws = (scheduleWorkspace) app.getScheduleWorkspaceComponent();




	// TEACHING ASSISTANTS
	// NOW BUILD THE TA JSON OBJECTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder taArrayBuilderGrad = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {
            if (ta.isUndergrad()==true){
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL, ta.getEmail()).build();
                taArrayBuilder.add(taJson);
            }
            else
            {
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL, ta.getEmail()).build();
                taArrayBuilderGrad.add(taJson);
            }
	}
	JsonArray undergradTAsArray = taArrayBuilder.build();
	JsonArray gradTAsArray = taArrayBuilderGrad.build();

	// NOW BUILD THE TIME SLOT JSON OBJECTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
    







	// Course Details
        // NOW BUILD THE TA JSON OBJECTS TO SAVE
	JsonArrayBuilder pageUsedArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder pageUnusedArrayBuilder = Json.createArrayBuilder();
	ObservableList<Page> pages = dataManager.getPages();
	for (Page pg : pages) {
            if (pg.isUse()==true){
                JsonObject pageJson = Json.createObjectBuilder()
                        .add(JSON_NAV, pg.getNav())
                        .add(JSON_FILENAME, pg.getFileName())
                        .add(JSON_SCRIPT, pg.getScript())
                        .build();
                pageUsedArrayBuilder.add(pageJson);
            }
            else
            {
                JsonObject pageJson = Json.createObjectBuilder()
                        .add(JSON_NAV, pg.getNav())
                        .add(JSON_FILENAME, pg.getFileName())
                        .add(JSON_SCRIPT, pg.getScript())
                        .build();
                pageUnusedArrayBuilder.add(pageJson);
            }
	}
	JsonArray UsedArray = pageUsedArrayBuilder.build();
	JsonArray UnusedArray = pageUnusedArrayBuilder.build();
        





		/// RECITATIONS

	JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
	ObservableList<Recitation> recs = dataManager.getRecitations();
	for (Recitation rec : recs) {
                JsonObject recitationJson = Json.createObjectBuilder()
                        .add(JSON_SECTION, rec.getSection())
                        .add(JSON_INSTRUCTOR, rec.getInstructor())
                        .add(JSON_DAYTIME, rec.getDayTime())
                        .add(JSON_LOCATION, rec.getLocation())
                        .add(JSON_TA1, rec.getSupervisingTA1())
                        .add(JSON_TA2, rec.getSupervisingTA2())
                        .build();
                recitationArrayBuilder.add(recitationJson);
	}
	JsonArray recitationArray = recitationArrayBuilder.build();
        




        /// SCHEDULE
	JsonArrayBuilder scheduleArrayBuilder = Json.createArrayBuilder();
	ObservableList<Schedule> sches = dataManager.getSchedules();
	for (Schedule sche : sches) {
                JsonObject recitationJson = Json.createObjectBuilder()
                        .add(JSON_TYPE, sche.getType())
                        .add(JSON_DATE, sche.getDate())
                        .add(JSON_TIME, sche.getTime())
                        .add(JSON_TITLE, sche.getTitle() )
                        .add(JSON_TOPIC, sche.getTopic() )
                        .add(JSON_LINK, sche.getLink())
                        .add(JSON_CRITERIA, sche.getCriteria() )
                        .build();
                scheduleArrayBuilder.add(recitationJson);
	}
	JsonArray scheduleArray = scheduleArrayBuilder.build();
        




			/// PROJECT

			// NOW BUILD THE TA JSON OBJECTS TO SAVE
			JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
			ObservableList<Team> teams = dataManager.getTeams();
			for (Team team : teams) {
		                JsonObject teamJson = Json.createObjectBuilder()
		                        .add(JSON_NAME, team.getName())
		                        .add(JSON_BGCOLOR, team.getBGcolor())
		                        .add(JSON_TEXTCOLOR, team.getTextColor())
		                        .add(JSON_LINK, team.getLink() )
		                        .build();
		                teamArrayBuilder.add(teamJson);
			}
			JsonArray teamArray = teamArrayBuilder.build();
		        
			JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
			ObservableList<Student> students = dataManager.getStudents();
			for (Student student : students) {
		                JsonObject studentJson = Json.createObjectBuilder()
		                        .add(JSON_FIRSTNAME, student.getFirstName())
		                        .add(JSON_LASTNAME, student.getLastName())
		                        .add(JSON_TEAM,  student.getTeam())
		                        .add(JSON_ROLE,  student.getRole() )
		                        .build();
		                studentArrayBuilder.add(studentJson);
			}
			JsonArray studentArray = studentArrayBuilder.build();
		        




	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_GRAD_TAS, gradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)


                .add(JSON_SUBJECT, "" + dataManager.getSubject())
                .add(JSON_NUMBER, "" + dataManager.getNumber())
                .add(JSON_SEMESTER,"" + dataManager.getSemester())
                .add(JSON_YEAR, "" + "" + dataManager.getYear())
                .add(JSON_TITLE, "" + dataManager.getCourseTitle())
                .add(JSON_INSTRUCTORNAME, "" + dataManager.getInstructorName())
                .add(JSON_INSTRUCTORHOME, "" + dataManager.getInstructorHome())
                .add(JSON_EXPORTDIR, "" + dataManager.getExportDir())
                .add(JSON_SITETEMPLATE, "" + dataManager.getSite())
                .add(JSON_BANNER, "" + dataManager.getBanner())
                .add(JSON_LEFTFOOTER, "" + dataManager.getLeftFooter())
                .add(JSON_RIGHTFOOTER, "" + dataManager.getRightFooter())
                // ADd the banners here
                .add(JSON_STYLESHEET, "" + dataManager.getStylesheet())
                
                
                .add(JSON_USE, UsedArray)
                .add(JSON_UNUSED, UnusedArray)
                .add(JSON_RECITATIONS, recitationArray)
                
                .add(JSON_STARTDATEMONTH, "" + dataManager.getStartMonth() )
                .add(JSON_STARTDATEDAY, "" + dataManager.getStartDay())
                .add(JSON_STARTDATEYEAR, "" + dataManager.getStartYear())
                
                
                
                .add(JSON_ENDDATEMONTH, "" + dataManager.getEndMonth() )
                .add(JSON_ENDDATEDAY, "" + dataManager.getEndDay())
                .add(JSON_ENDDATEYEAR, "" + dataManager.getEndYear())
                
                .add(JSON_SCHEDULE, scheduleArray)


                .add(JSON_TEAMS, teamArray)
                .add(JSON_STUDENTS, studentArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();

    }
    
    
    
    
        
    
    @Override
    public void loadAllData(AppDataComponent data, String filePath) throws IOException {
	// CLEAR THE OLD DATA OUT
	TAData dataManager = (TAData)data;

	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);

	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);

          // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            //dataManager.addTA(name);
            //dataManager.addTA(name, email);
            dataManager.addTA(name, email, true);
        }
        // NOW LOAD ALL THE GRAD TAs
        jsonTAArray = json.getJsonArray(JSON_GRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            //dataManager.addTA(name);
            //dataManager.addTA(name, email);
            dataManager.addTA(name, email, false);
        }

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.addOfficeHoursReservation(day, time, name);
        }
        
        
        
        //Course Details
        
	// LOAD THE START AND END HOURS
        
	String subject = json.getString(JSON_SUBJECT);
        String number = json.getString(JSON_NUMBER);
        String semester = json.getString(JSON_SEMESTER);
        String year = json.getString(JSON_YEAR);
        String courseTitle = json.getString(JSON_TITLE);
        String instructorName = json.getString(JSON_INSTRUCTORNAME);
        String instructorHome = json.getString(JSON_INSTRUCTORHOME);
        String exportDir = json.getString(JSON_EXPORTDIR);
        String site = json.getString(JSON_SITETEMPLATE);
        String stylesheet = json.getString(JSON_STYLESHEET);
        String banner = json.getString(JSON_BANNER);
        String leftFooter = json.getString(JSON_LEFTFOOTER);
        String rightFooter = json.getString(JSON_RIGHTFOOTER);
        
        //String number = json.getString(JSON_)
        /*String number = json.getString(JSON_)
        String number = json.getString(JSON_)
        String number = json.getString(JSON_)
        */
        dataManager.initCourseDetails(subject,  number,  semester,  year,  courseTitle ,  instructorName,  instructorHome,  exportDir,  site,  stylesheet, banner, leftFooter, rightFooter);

        //scheduleWorkspace sws = (scheduleWorkspace) app.getScheduleWorkspaceComponent();

        /*        
        cws.getSubjectComboBox().setValue(subject);
        cws.getNumberComboBox().setValue(number);
        cws.getSemesterComboBox().setValue(semester);
        cws.getYearComboBox().setValue(year);
        
        cws.getTitleTextField().setText(courseTitle);
        cws.getInstructorNameTextField().setText(instructorName);
        cws.getInstructorHomeTextField().setText(instructorHome);
        cws.getPathLabel().setText(exportDir);
        cws.getTemplateChooseLabel().setText(site);
        cws.getStylesheetComboBox().setValue(stylesheet);
  */      
        
        
        
        JsonArray jsonPageArray = json.getJsonArray(JSON_USE);
        for (int i = 0; i < jsonPageArray.size(); i++) {
            JsonObject jsonPage = jsonPageArray.getJsonObject(i);
            String nav = jsonPage.getString(JSON_NAV);
            String fileName = jsonPage.getString(JSON_FILENAME);
            String script = jsonPage.getString(JSON_SCRIPT);
            dataManager.addPage(nav, fileName, script, true);
        }
        
        
        // NOW LOAD ALL THE unused Pages
        jsonPageArray = json.getJsonArray(JSON_UNUSED);
        for (int i = 0; i < jsonPageArray.size(); i++) {
            JsonObject jsonPage = jsonPageArray.getJsonObject(i);
            String nav = jsonPage.getString(JSON_NAV);
            String fileName = jsonPage.getString(JSON_FILENAME);
            String script = jsonPage.getString(JSON_SCRIPT);
            dataManager.addPage(nav, fileName, script, false);
        }
        
        
        
	int startYear = Integer.parseInt(json.getString(JSON_STARTDATEYEAR));
        int startMonth = Integer.parseInt(json.getString(JSON_STARTDATEMONTH));
        int startDay = Integer.parseInt(json.getString(JSON_STARTDATEDAY));
        
	int endYear = Integer.parseInt(json.getString(JSON_ENDDATEYEAR));
        int endMonth = Integer.parseInt(json.getString(JSON_ENDDATEMONTH));
        int endDay = Integer.parseInt(json.getString(JSON_ENDDATEDAY));
        
        //sws.getStartDatePicker().setValue(LocalDate.of(startYear, startMonth,startDay));
        //sws.getEndDatePicker().setValue(LocalDate.of(endYear, endMonth, endDay));
        dataManager.initDates(startYear, startMonth,startDay,endYear, endMonth, endDay);
        
        //sws.getStartDatePicker().setValue(LocalDate.of(2017,1, 1));
        //sws.getEndDatePicker().setValue(LocalDate.of(2017, 5,19));
        
        // Recitations
         JsonArray jsonRecitationArray = json.getJsonArray(JSON_RECITATIONS);
        for (int i = 0; i < jsonRecitationArray.size(); i++) {
            JsonObject jsonRecitation = jsonRecitationArray.getJsonObject(i);
            String section = jsonRecitation.getString(JSON_SECTION);
            String instructor = jsonRecitation.getString(JSON_INSTRUCTOR);
            String dayTime = jsonRecitation.getString(JSON_DAYTIME);
            String location = jsonRecitation.getString(JSON_LOCATION);
            String TA1 = jsonRecitation.getString(JSON_TA1);
            String TA2 = jsonRecitation.getString(JSON_TA2);
            dataManager.addRecitation(section,instructor, dayTime, location, TA1, TA2);
        }
        
        
        
        
        // SCHEDULES
         JsonArray jsonScheduleArray = json.getJsonArray(JSON_SCHEDULE);
        for (int i = 0; i < jsonScheduleArray.size(); i++) {
            JsonObject jsonSchedule = jsonScheduleArray.getJsonObject(i);
            String type = jsonSchedule.getString(JSON_TYPE);
            String date = jsonSchedule.getString(JSON_DATE);
            String time = jsonSchedule.getString(JSON_TIME);
            String title = jsonSchedule.getString(JSON_TITLE);
            String topic = jsonSchedule.getString(JSON_TOPIC);
            String link = jsonSchedule.getString(JSON_LINK);
            String criteria = jsonSchedule.getString(JSON_CRITERIA);
            dataManager.addSchedule(type, date, time, title, topic, link, criteria);
        }
        
        
        
        // TeamS
         JsonArray jsonTeamArray = json.getJsonArray(JSON_TEAMS);
        for (int i = 0; i < jsonTeamArray.size(); i++) {
            JsonObject jsonTeam = jsonTeamArray.getJsonObject(i);
            String name = jsonTeam.getString(JSON_NAME);
            String color = jsonTeam.getString(JSON_BGCOLOR);
            String textColor = jsonTeam.getString(JSON_TEXTCOLOR);
            String link = jsonTeam.getString(JSON_LINK);
            dataManager.addTeam(name, color, textColor, link);
        }
        
        JsonArray jsonStudentArray = json.getJsonArray(JSON_STUDENTS);
        for (int i = 0; i < jsonStudentArray.size(); i++) {
            JsonObject jsonStudent = jsonStudentArray.getJsonObject(i);
            String firstName = jsonStudent.getString(JSON_FIRSTNAME);
            String lastName = jsonStudent.getString(JSON_LASTNAME);
            String team = jsonStudent.getString(JSON_TEAM);
            String role = jsonStudent.getString(JSON_ROLE);
            dataManager.addStudent(firstName, lastName, team, role);
        }
        
        dataManager.isLoaded = true;
        TAController.getJTPS().clearTransaction();
    }
    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    //public void exportData(AppDataComponent data, String filePath) throws IOException {
    public void exportData(AppDataComponent data) throws IOException {
        
	// SAVE IT TO A DIRECTORY
        
	TAData dataManager = (TAData)data;
        String filePath = dataManager.getExportDir().substring(5);
        String filePath2 = dataManager.getSite().substring(5);
        
        //File prevFile = new File(PATH_TO_HTML);
        File prevFile = new File(filePath2);
        File selectedFile = new File(filePath);
        
        
        FileUtils.copyDirectory(prevFile, selectedFile);
        System.out.println(prevFile.toString());
        System.out.println(selectedFile.toString());
        
        System.out.println(PATH_BRANDS);
        System.out.println(selectedFile.toString()+"images");
        FileUtils.copyDirectory(new File(PATH_BRANDS), new File(selectedFile.toString()+"/images"));
	app.getFileComponent().exportData(app.getDataComponent(), selectedFile.toString());
        /*
        app.getFileComponent().saveData(app.getDataComponent(), selectedFile.toString()+"/js/OfficeHoursGridData.json");
        app.getFileComponent().saveCourseData(app.getDataComponent(), selectedFile.toString()+"/js/HomeData.json");
	app.getFileComponent().saveRecitationData(app.getDataComponent(), selectedFile.toString()+"/js/RecitationsData.json");
	app.getFileComponent().saveScheduleData(app.getDataComponent(), selectedFile.toString()+"/js/ScheduleData.json");
	app.getFileComponent().saveProjectData(app.getDataComponent(), selectedFile.toString()+"/js/ProjectsData.json");
	app.getFileComponent().saveTeamData(app.getDataComponent(), selectedFile.toString()+"/js/TeamsAndStudents.json");
        */
        System.out.print("DID IT COPY?");
	// TELL THE USER THE FILE HAS BEEN SAVED
	AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
        dialog.show(props.getProperty(EXPORT_COMPLETED_TITLE),props.getProperty(EXPORT_COMPLETED_MESSAGE), props.getProperty(CLOSE_BUTTON_TEXT));
        
        /*System.out.println("yo haina ra" + dataManager.getExportDir().substring(5));
        System.out.println("yo haina ra" + filePath);
        //Path p = new Path(filePath);
        //File
        File file = new File(filePath);
        //File f = new File();
        //System.out.println(file.toString());
        app.getFileController().exportWork(file);*/
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        
	// CLEAR THE OLD DATA OUT
	TAData dataManager = (TAData)data;
        //filePath = dataManager.getExportDir();
        
	saveData(app.getDataComponent(), filePath.toString()+"/js/OfficeHoursGridData.json");
        saveCourseData(app.getDataComponent(), filePath.toString()+"/js/HomeData.json");
        System.out.print("new " + filePath.toString()+"/js/HomeData.json");
	saveRecitationData(app.getDataComponent(), filePath.toString()+"/js/RecitationsData.json");
	saveScheduleData(app.getDataComponent(), filePath.toString()+"/js/ScheduleData.json");
	saveProjectData(app.getDataComponent(), filePath.toString()+"/js/ProjectsData.json");
	saveTeamData(app.getDataComponent(), filePath.toString()+"/js/TeamsAndStudents.json");
        
        /*
        //String newPath = dataManager.getBanner();
        
        //EXAMPLEcopyDirectory(File srcDir, File destDir) 
        //File prevFile = new File(PATH_TO_HTML);
        System.out.print("WELL IS THIS ENOUGH?");
        
        //FileUtils.copyDirectory(prevFile, selectedFile);
        
        File source = new File(FILE_PROTOCOL + newPath);
        String oldPath = filePath.toString()+ "/images/";
        
        File replacementFile = new File(FILE_PROTOCOL + oldPath);
        
        System.out.print("new " + newPath);
        System.out.print("old " + oldPath);
        //FileUtils.forceDelete(replacementFile);
	//InputStream is = new ByteArrayInputStream(app.getFileComponent().saveJsonData(app.getDataComponent()).toString().getBytes());
        
        
        FileUtils.copyFile(source, replacementFile);
        */
        //FileUtils.copyFile(source, source);
        //FileUtils.copyToFile(source, replacementFile);
        //FileUtils.copyToFile(source, prevFile);
        //FileUtils.
        //Files.move()
        /*left CSLogo.png
        right SBUWhiteShieldLogo.jpg
        banner SBUDarkRedShieldLogo.png
        */
        
    
    }
    
    // HELPER METHOD FOR EXPORTING WORK
    public void exportTestWork(String selectedFile) throws IOException{
	// SAVE IT TO A DIRECTORY
        app.getFileComponent().saveAllData(app.getDataComponent(), PATH_WORK + selectedFile + ".json");
    }
    
    
    @Override
    public void loadTestData(AppDataComponent data, String filePath) throws IOException {
	// CLEAR THE OLD DATA OUT
	TAData dataManager = (TAData)data;

	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);

	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHoursTest(startHour, endHour);

          // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            dataManager.addTA(name, email, true);
        }
        // NOW LOAD ALL THE GRAD TAs
        jsonTAArray = json.getJsonArray(JSON_GRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            dataManager.addTA(name, email, false);
        }

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.addOfficeHoursReservation(day, time, name);
        }
        
        
        
        //Course Details
        
	// LOAD THE START AND END HOURS
        
	String subject = json.getString(JSON_SUBJECT);
        String number = json.getString(JSON_NUMBER);
        String semester = json.getString(JSON_SEMESTER);
        String year = json.getString(JSON_YEAR);
        String courseTitle = json.getString(JSON_TITLE);
        String instructorName = json.getString(JSON_INSTRUCTORNAME);
        String instructorHome = json.getString(JSON_INSTRUCTORHOME);
        String exportDir = json.getString(JSON_EXPORTDIR);
        String site = json.getString(JSON_SITETEMPLATE);
        String stylesheet = json.getString(JSON_STYLESHEET);
        
        dataManager.initCourseDetails(subject,  number,  semester,  year,  courseTitle ,  instructorName,  instructorHome,  exportDir,  site,  stylesheet);
        
        JsonArray jsonPageArray = json.getJsonArray(JSON_USE);
        for (int i = 0; i < jsonPageArray.size(); i++) {
            JsonObject jsonPage = jsonPageArray.getJsonObject(i);
            String nav = jsonPage.getString(JSON_NAV);
            String fileName = jsonPage.getString(JSON_FILENAME);
            String script = jsonPage.getString(JSON_SCRIPT);
            dataManager.addPage(nav, fileName, script, true);
        }
        
        
        // NOW LOAD ALL THE unused Pages
        jsonPageArray = json.getJsonArray(JSON_UNUSED);
        for (int i = 0; i < jsonPageArray.size(); i++) {
            JsonObject jsonPage = jsonPageArray.getJsonObject(i);
            String nav = jsonPage.getString(JSON_NAV);
            String fileName = jsonPage.getString(JSON_FILENAME);
            String script = jsonPage.getString(JSON_SCRIPT);
            dataManager.addPage(nav, fileName, script, false);
        }
        
        
        
	int startYear = Integer.parseInt(json.getString(JSON_STARTDATEYEAR));
        int startMonth = Integer.parseInt(json.getString(JSON_STARTDATEMONTH));
        int startDay = Integer.parseInt(json.getString(JSON_STARTDATEDAY));
        
	int endYear = Integer.parseInt(json.getString(JSON_ENDDATEYEAR));
        int endMonth = Integer.parseInt(json.getString(JSON_ENDDATEMONTH));
        int endDay = Integer.parseInt(json.getString(JSON_ENDDATEDAY));
        
        dataManager.initDates(startYear, startMonth,startDay,endYear, endMonth, endDay);
        //sws.getStartDatePicker().setValue(LocalDate.of(startYear, startMonth,startDay));
        //sws.getEndDatePicker().setValue(LocalDate.of(endYear, endMonth, endDay));
        
        //sws.getStartDatePicker().setValue(LocalDate.of(2017,1, 1));
        //sws.getEndDatePicker().setValue(LocalDate.of(2017, 5,19));
        
        // Recitations
         JsonArray jsonRecitationArray = json.getJsonArray(JSON_RECITATIONS);
        for (int i = 0; i < jsonRecitationArray.size(); i++) {
            JsonObject jsonRecitation = jsonRecitationArray.getJsonObject(i);
            String section = jsonRecitation.getString(JSON_SECTION);
            String instructor = jsonRecitation.getString(JSON_INSTRUCTOR);
            String dayTime = jsonRecitation.getString(JSON_DAYTIME);
            String location = jsonRecitation.getString(JSON_LOCATION);
            String TA1 = jsonRecitation.getString(JSON_TA1);
            String TA2 = jsonRecitation.getString(JSON_TA2);
            dataManager.addRecitation(section,instructor, dayTime, location, TA1, TA2);
        }
        
        
        
        
        // SCHEDULES
         JsonArray jsonScheduleArray = json.getJsonArray(JSON_SCHEDULE);
        for (int i = 0; i < jsonScheduleArray.size(); i++) {
            JsonObject jsonSchedule = jsonScheduleArray.getJsonObject(i);
            String type = jsonSchedule.getString(JSON_TYPE);
            String date = jsonSchedule.getString(JSON_DATE);
            String time = jsonSchedule.getString(JSON_TIME);
            String title = jsonSchedule.getString(JSON_TITLE);
            String topic = jsonSchedule.getString(JSON_TOPIC);
            String link = jsonSchedule.getString(JSON_LINK);
            String criteria = jsonSchedule.getString(JSON_CRITERIA);
            dataManager.addSchedule(type, date, time, title, topic, link, criteria);
        }
        
        
        
        // TeamS
         JsonArray jsonTeamArray = json.getJsonArray(JSON_TEAMS);
        for (int i = 0; i < jsonTeamArray.size(); i++) {
            JsonObject jsonTeam = jsonTeamArray.getJsonObject(i);
            String name = jsonTeam.getString(JSON_NAME);
            String color = jsonTeam.getString(JSON_BGCOLOR);
            String textColor = jsonTeam.getString(JSON_TEXTCOLOR);
            String link = jsonTeam.getString(JSON_LINK);
            dataManager.addTeam(name, color, textColor, link);
        }
        
        JsonArray jsonStudentArray = json.getJsonArray(JSON_STUDENTS);
        for (int i = 0; i < jsonStudentArray.size(); i++) {
            JsonObject jsonStudent = jsonStudentArray.getJsonObject(i);
            String firstName = jsonStudent.getString(JSON_FIRSTNAME);
            String lastName = jsonStudent.getString(JSON_LASTNAME);
            String team = jsonStudent.getString(JSON_TEAM);
            String role = jsonStudent.getString(JSON_ROLE);
            dataManager.addStudent(firstName, lastName, team, role);
        }
        
        dataManager.isLoaded = true;
        TAController.getJTPS().clearTransaction();
    }
    
    
    
        
    @Override
    public void saveTestData(AppDataComponent data, String filePath) throws IOException {
    	// GET THE DATA
	TAData dataManager = (TAData)data;
        //courseWorkspace course = (courseWorkspace) app.getCourseWorkspaceComponent();
        //scheduleWorkspace sws = (scheduleWorkspace) app.getScheduleWorkspaceComponent();




	// TEACHING ASSISTANTS
	// NOW BUILD THE TA JSON OBJECTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder taArrayBuilderGrad = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {
            if (ta.isUndergrad()==true){
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL, ta.getEmail()).build();
                taArrayBuilder.add(taJson);
            }
            else
            {
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL, ta.getEmail()).build();
                taArrayBuilderGrad.add(taJson);
            }
	}
	JsonArray undergradTAsArray = taArrayBuilder.build();
	JsonArray gradTAsArray = taArrayBuilderGrad.build();

        
	// NOW BUILD THE TIME SLOT JSON OBJECTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
        /*
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}*/
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
    







	// Course Details
        // NOW BUILD THE TA JSON OBJECTS TO SAVE
	JsonArrayBuilder pageUsedArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder pageUnusedArrayBuilder = Json.createArrayBuilder();
	ObservableList<Page> pages = dataManager.getPages();
	for (Page pg : pages) {
            if (pg.isUse()==true){
                JsonObject pageJson = Json.createObjectBuilder()
                        .add(JSON_NAV, pg.getNav())
                        .add(JSON_FILENAME, pg.getFileName())
                        .add(JSON_SCRIPT, pg.getScript())
                        .build();
                pageUsedArrayBuilder.add(pageJson);
            }
            else
            {
                JsonObject pageJson = Json.createObjectBuilder()
                        .add(JSON_NAV, pg.getNav())
                        .add(JSON_FILENAME, pg.getFileName())
                        .add(JSON_SCRIPT, pg.getScript())
                        .build();
                pageUnusedArrayBuilder.add(pageJson);
            }
	}
	JsonArray UsedArray = pageUsedArrayBuilder.build();
	JsonArray UnusedArray = pageUnusedArrayBuilder.build();
        





		/// RECITATIONS

	JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
	ObservableList<Recitation> recs = dataManager.getRecitations();
	for (Recitation rec : recs) {
                JsonObject recitationJson = Json.createObjectBuilder()
                        .add(JSON_SECTION, rec.getSection())
                        .add(JSON_INSTRUCTOR, rec.getInstructor())
                        .add(JSON_DAYTIME, rec.getDayTime())
                        .add(JSON_LOCATION, rec.getLocation())
                        .add(JSON_TA1, rec.getSupervisingTA1())
                        .add(JSON_TA2, rec.getSupervisingTA2())
                        .build();
                recitationArrayBuilder.add(recitationJson);
	}
	JsonArray recitationArray = recitationArrayBuilder.build();
        




        /// SCHEDULE
	JsonArrayBuilder scheduleArrayBuilder = Json.createArrayBuilder();
	ObservableList<Schedule> sches = dataManager.getSchedules();
	for (Schedule sche : sches) {
                JsonObject recitationJson = Json.createObjectBuilder()
                        .add(JSON_TYPE, sche.getType())
                        .add(JSON_DATE, sche.getDate())
                        .add(JSON_TIME, sche.getTime())
                        .add(JSON_TITLE, sche.getTitle() )
                        .add(JSON_TOPIC, sche.getTopic() )
                        .add(JSON_LINK, sche.getLink())
                        .add(JSON_CRITERIA, sche.getCriteria() )
                        .build();
                scheduleArrayBuilder.add(recitationJson);
	}
	JsonArray scheduleArray = scheduleArrayBuilder.build();
        




			/// PROJECT

			// NOW BUILD THE TA JSON OBJECTS TO SAVE
			JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
			ObservableList<Team> teams = dataManager.getTeams();
			for (Team team : teams) {
		                JsonObject teamJson = Json.createObjectBuilder()
		                        .add(JSON_NAME, team.getName())
		                        .add(JSON_BGCOLOR, team.getBGcolor())
		                        .add(JSON_TEXTCOLOR, team.getTextColor())
		                        .add(JSON_LINK, team.getLink() )
		                        .build();
		                teamArrayBuilder.add(teamJson);
			}
			JsonArray teamArray = teamArrayBuilder.build();
		        
			JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
			ObservableList<Student> students = dataManager.getStudents();
			for (Student student : students) {
		                JsonObject studentJson = Json.createObjectBuilder()
		                        .add(JSON_FIRSTNAME, student.getFirstName())
		                        .add(JSON_LASTNAME, student.getLastName())
		                        .add(JSON_TEAM,  student.getTeam())
		                        .add(JSON_ROLE,  student.getRole() )
		                        .build();
		                studentArrayBuilder.add(studentJson);
			}
			JsonArray studentArray = studentArrayBuilder.build();
		        




	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_GRAD_TAS, gradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)


                
                .add(JSON_SUBJECT, "" + dataManager.getSubject())
                .add(JSON_NUMBER, "" + dataManager.getNumber())
                .add(JSON_SEMESTER,"" + dataManager.getSemester())
                .add(JSON_YEAR, "" + "" + dataManager.getYear())
                .add(JSON_TITLE, "" + dataManager.getCourseTitle())
                .add(JSON_INSTRUCTORNAME, "" + dataManager.getInstructorName())
                .add(JSON_INSTRUCTORHOME, "" + dataManager.getInstructorHome())
                .add(JSON_EXPORTDIR, "" + dataManager.getExportDir())
                .add(JSON_SITETEMPLATE, "" + dataManager.getSite())
                // ADd the banners here
                .add(JSON_STYLESHEET, "" + dataManager.getStylesheet())
                
                /*
                .add(JSON_SUBJECT, "" + course.getSubjectComboBox().getSelectionModel().getSelectedItem().toString())
                .add(JSON_NUMBER, "" + course.getNumberComboBox().getSelectionModel().getSelectedItem().toString())
                .add(JSON_SEMESTER,"" + course.getSemesterComboBox().getSelectionModel().getSelectedItem().toString())
                .add(JSON_YEAR, "" + "" + course.getYearComboBox().getSelectionModel().getSelectedItem().toString())
                .add(JSON_TITLE, "" + course.getTitleTextField().getText())
                .add(JSON_INSTRUCTORNAME, "" + course.getInstructorNameTextField().getText())
                .add(JSON_INSTRUCTORHOME, "" + course.getInstructorHomeTextField().getText())
                .add(JSON_EXPORTDIR, "" + course.getPathLabel().getText())
                .add(JSON_SITETEMPLATE, "" + course.getTemplateChooseLabel().getText())
                // ADd the banners here
                .add(JSON_STYLESHEET, "" + course.getStylesheetComboBox().getSelectionModel().getSelectedItem().toString())
                */
                
                .add(JSON_USE, UsedArray)
                .add(JSON_UNUSED, UnusedArray)




                .add(JSON_RECITATIONS, recitationArray)

               
                
                .add(JSON_STARTDATEMONTH, "" + dataManager.getStartMonth() )
                .add(JSON_STARTDATEDAY, "" + dataManager.getStartDay())
                .add(JSON_STARTDATEYEAR, "" + dataManager.getStartYear())
                
                
                
                .add(JSON_ENDDATEMONTH, "" + dataManager.getEndMonth() )
                .add(JSON_ENDDATEDAY, "" + dataManager.getEndDay())
                .add(JSON_ENDDATEYEAR, "" + dataManager.getEndYear())
                /*
                .add(JSON_ENDDATEMONTH, "" + sws.getEndDatePicker().getValue().getMonthValue() )
                .add(JSON_ENDDATEDAY, "" + sws.getEndDatePicker().getValue().getDayOfMonth() )
                .add(JSON_ENDDATEYEAR, "" + sws.getEndDatePicker().getValue().getYear())
                */
                
                .add(JSON_SCHEDULE, scheduleArray)


                .add(JSON_TEAMS, teamArray)
                .add(JSON_STUDENTS, studentArray)



		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();

    }
}