/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam;

/**
 * This enum provides a list of all the user interface
 * text that needs to be loaded from the XML properties
 * file. By simply changing the XML file we could initialize
 * this application such that all UI controls are provided
 * in another language.
 * 
 * This only contains for the tab pane.
 * 
 * @author PLT
 * @version 1.0
 */
public enum csgProp {
    // FOR THE TAB TITLES
    
    COURSE_DETAILS_TAB_TEXT,
    TA_DATA_TAB_TEXT,
    RECITATION_TAB_TEXT,
    SCHEDULE_TAB_TEXT,
    PROJECT_DATA_TAB_TEXT
}
