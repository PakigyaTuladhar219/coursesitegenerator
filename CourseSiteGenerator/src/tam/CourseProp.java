/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam;

/**
 * This enum provides a list of all the user interface
 * text that needs to be loaded from the XML properties
 * file. By simply changing the XML file we could initialize
 * this application such that all UI controls are provided
 * in another language.
 * 
 * This only contains for the tab pane.
 * 
 * @author PLT
 * @version 1.0
 */
public enum CourseProp {
 //To easily load data
        // COURSE DETAILS WORKSPACE CONTROLS STUFF
        COURSE_INFO_LABEL_TEXT,
        SUBJECT_LABEL_TEXT,
        NUMBER_LABEL_TEXT,
        SEMESTER_LABEL_TEXT,
        YEAR_LABEL_TEXT,
        TITLE_LABEL_TEXT,
        INSTRUCTOR_NAME_LABEL_TEXT,
        INSTRUCTOR_HOME_LABEL_TEXT,
        EXPORT_DIR_LABEL_TEXT,
        CHANGE_BUTTON_TEXT,
        
        // SITE TEMPLATE
        SITE_TEMPLATE_LABEL_TEXT,
        INFO_LABEL_TEXT,
        SELECT_DIR_BUTTON_TEXT,
        SITE_PAGES_LABEL_TEXT,

        //HEADER FOR TABLE
        USE_HEADER_TEXT,
        NAVBAR_HEADER_TEXT,
        FILE_NAME_HEADER_TEXT,
        SCRIPT_HEADER_TEXT,

        //PAGE STYLE
        PAGE_STYLE_LABEL_TEXT,
        BANNER_SCHOOL_LABEL_TEXT,
        LEFT_FOOTER_LABEL_TEXT,
        RIGHT_FOOTER_LABEL_TEXT,
        STYLESHEET_LABEL_TEXT,
        NOTE_STYLESHEET_LABEL_TEXT
}
