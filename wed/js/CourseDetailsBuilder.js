// dataFile TO LOAD
var subject;
var number;
var semester;
var year;
var title;
var instructorName;
var instructorHome;
var title;
var banner;
var leftFooter;
var rightFooter;
var stylesheet;
var page= "";

function buildCourseDetails(pageInfo) {
    var dataFile = "./js/HomeData.json";
    loadDataFile(dataFile, loadCourseDetails);
    page = pageInfo;
}


function loadDataFile(jsonFile, callback) {
    $.getJSON(jsonFile, function(json) {
        callback(json);
    });
}

function buildNavBar(json){

}

function buildCourseInfo(dataFile){

    var nav = $("#navbar");
    var textToAppend = "";

    var home = "";
    var syllabus = "";
    var schedule = "";
    var hws = "";
    var projects = "";
    var open = "";
    for (var i = 0; i < dataFile.use.length; i++) {
        if (dataFile.use[i].nav == "Home")
        {
            if (page=="Home"){open="open_"}
            home = '<a class="' + open +'nav" href="index.html" id="home_link">Home</a>';
        }
        if (dataFile.use[i].nav == "Syllabus")
        {
            if (page=="Syllabus"){open="open_"}
            syllabus= '<a class="' + open +'nav" href="syllabus.html" id="syllabus_link">Syllabus</a>';
        }
        if (dataFile.use[i].nav  == "Schedule")
        {
            if (page=="Schedule"){open="open_"}
            schedule= '<a class="' + open +'nav" href="schedule.html" id="schedule_link">Schedule</a>';
        }
        if (dataFile.use[i].nav == "HWs")
        {
            if (page=="HWs"){open="open_"}
            hws = '<a class="' + open +'nav" href="hws.html" id="hws_link">HWs</a>';
        }
        if (dataFile.use[i].nav  == "Projects")
        {
            if (page=="Projects"){open="open_"}
            projects = '<a id="projects_link" class="' + open +'nav" href="projects.html">Projects</a>';
        }
        open ="";
    }
    textToAppend = home + syllabus + schedule + hws + projects; 
    nav.append(textToAppend);

    var pageTitle = $("#title");
    textToAppend = subject + " " + number;

    var pageTitle = $("#classtitle");
    textToAppend = subject + " " + number;
    
    pageTitle.append(textToAppend);



    var prof = $("#banner");
    var textToAppend = subject + " " + number  + " - " + semester + " " + year  + " <br>" +  title;
    
    prof.append(textToAppend);

    var createdBy = $("#instructor_link");
    createdBy.append('<a href="' + instructorHome + '">' + instructorName + "</a>")

    //var x = document.getElementById("sbu_navbar");   // Get the element with id="demo"
        //x.src = "./images/" + banner;
    var ban = $("#bannerImg");
    ban.append('<img class="sbu_navbar" alt="Stony Brook University"  height="50" src="./images/' + banner + '"/>');
    
    var left = $("#left");
    left.append('<img class="sunysb" style="float:left" src="./images/'+ leftFooter +'" alt="SBU" />');

    var right = $("#right");
    left.append('<img style="float:right" src="./images/'+ rightFooter +'" alt="CS" />');
}

function loadCourseDetails(json) {
    initDetails(json);
    professordataFile(json);
    buildCourseInfo(json);
}

function initDetails(dataFile) {
    // GET THE START AND END HOURS
    subject = dataFile.subject;
    number = parseInt(dataFile.number);
    semester = dataFile.semester;
    year = parseInt(dataFile.year);
    title = dataFile.title;
    instructorName = dataFile.instructorName;
    instructorHome = dataFile.instructorHome;
    banner = dataFile.bannerSchool;
    leftFooter  = dataFile.leftFooter ;
    rightFooter  = dataFile.rightFooter;
    stylesheet = dataFile.stylesheet;
}

function professordataFile(dataFile){
    var prof = $("#professor")
    var textToAppend = '<a href="' + instructorHome + '">' + instructorName + "</a><br/>";
    /*textToAppend += "<strong>" + instructorHome+" </strong><br />";
*/
    prof.append(textToAppend);

    var profImg = $("#profImg");
    profImg.append(professorPicture())


}


function professorPicture() {
    var name = instructorName;
    var abbrName = name.replace(/\s/g, '');
    var text = "<img "
                + " src='./images/" + abbrName + ".JPG' "
                + " alt='" + name + "' style='margin-left:30px;'' /> <br />"
                + "<br /><br />";
    return text;
}
