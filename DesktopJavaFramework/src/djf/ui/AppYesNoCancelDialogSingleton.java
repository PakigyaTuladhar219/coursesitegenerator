package djf.ui;

import djf.settings.AppPropertyType;
import static djf.settings.AppPropertyType.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import properties_manager.PropertiesManager;

/**
 * This class serves to present a dialog with three options to
 * the user: Yes, No, or Cancel and lets one access which was
 * selected.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class AppYesNoCancelDialogSingleton extends Stage {
    // HERE'S THE SINGLETON
    static AppYesNoCancelDialogSingleton singleton;
    
    
    // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
    static PropertiesManager props = PropertiesManager.getPropertiesManager();
    
    // GUI CONTROLS FOR OUR DIALOG
    VBox messagePane;
    Scene messageScene;
    Label messageLabel;
    Button yesButton;
    Button noButton;
    Button cancelButton;
    String selection;
    
    // CONSTANT CHOICES

    public static String YES = props.getProperty(AppPropertyType.YES_MESSAGE.toString());
    public static String NO = props.getProperty(AppPropertyType.NO_MESSAGE.toString());
    public static String CANCEL = props.getProperty(AppPropertyType.CANCEL_MESSAGE.toString());
   
    
    /*public static String YES = "yes";*/
    //public static String NO ="no";
    //public static String CANCEL = "cancel";
    
    //public static final String NO = props.getProperty(AppPropertyType.NO_MESSAGE.toString());
    //public static final String CANCEL = props.getProperty(AppPropertyType.CANCEL_MESSAGE.toString());
    
    /**
     * Note that the constructor is private since it follows
     * the singleton design pattern.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    private AppYesNoCancelDialogSingleton(
    ) {
        /*
        YES = props.getProperty(AppPropertyType.YES_MESSAGE.toString());
        NO = props.getProperty(AppPropertyType.NO_MESSAGE.toString());
        CANCEL = props.getProperty(AppPropertyType.CANCEL_MESSAGE.toString());
        */
     }
    
    /**
     * The static accessor method for this singleton.
     * 
     * @return The singleton object for this type.
     */
    public static AppYesNoCancelDialogSingleton getSingleton() {
	if (singleton == null)
	    singleton = new AppYesNoCancelDialogSingleton();
	return singleton;
    }
	
    /**
     * This method initializes the singleton for use.
     * 
     * @param primaryStage The window above which this
     * dialog will be centered.
     */
    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        messageLabel = new Label();        
        
/*
        String YES = props.getProperty(AppPropertyType.YES_MESSAGE.toString());
        String NO = props.getProperty(AppPropertyType.NO_MESSAGE.toString());
        String CANCEL = props.getProperty(AppPropertyType.CANCEL_MESSAGE.toString());
*/
        // YES, NO, AND CANCEL BUTTONS
        yesButton = new Button(YES);
        noButton = new Button(NO);
        cancelButton = new Button(CANCEL);
	
	// MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler yesNoCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            AppYesNoCancelDialogSingleton.this.selection = sourceButton.getText();
            AppYesNoCancelDialogSingleton.this.hide();
        };
        
	// AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        yesButton.setOnAction(yesNoCancelHandler);
        noButton.setOnAction(yesNoCancelHandler);
        cancelButton.setOnAction(yesNoCancelHandler);

        // NOW ORGANIZE OUR BUTTONS
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(yesButton);
        buttonBox.getChildren().add(noButton);
        buttonBox.getChildren().add(cancelButton);
        
        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(buttonBox);
        
        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }

    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
 
    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     * 
     * @param title The title to appear in the dialog window bar.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(String title, String message) {
	// SET THE DIALOG TITLE BAR TITLE
	setTitle(title);
	
	// SET THE MESSAGE TO DISPLAY TO THE USER
        messageLabel.setText(message);
        /*
        yesButton.setText(AppPropertyType.YES_MESSAGE.toString());
        noButton.setText(AppPropertyType.NO_MESSAGE.toString());
        cancelButton.setText(AppPropertyType.CANCEL_MESSAGE.toString());
        */
        /*
        String YES = props.getProperty(AppPropertyType.YES_MESSAGE.toString());
        String NO = props.getProperty(AppPropertyType.NO_MESSAGE.toString());
        String CANCEL = props.getProperty(AppPropertyType.CANCEL_MESSAGE.toString());
	*/
        
	// AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
	// WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
	// DO MORE WORK.
        showAndWait();
    }
}